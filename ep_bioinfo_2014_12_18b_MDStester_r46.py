# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 14:21:40 2014

@author: root
"""

from ep_bioinfo_2014_12_16a_src_reader3_r50 import *

D = LQs_D

seed = np.random.RandomState()

from sklearn import manifold

from sklearn.decomposition import PCA

corrs = corrcoef(D.transpose())
similarities = 1-corrs

print('doing MDS')

mds = manifold.MDS(n_components=2, metric=False, max_iter=300000, eps=1e-12,
                    dissimilarity="precomputed", random_state=seed, n_jobs=6,
                    n_init=3000)
                    
   
cities = LQs_engCities
                 
R=mds.fit(similarities)           
C = R.fit_transform(similarities)
print('done MDS')

if(1):
  figure()
  
  for xj in range(len(C)):
    x=C[xj]
    plot(x[0],x[1],'.')    
    plt.text(   x[0],x[1], unicode(cities[xj]) )

Fitdists = [ [ sqrt(sum((C[j]-C[i])**2)) for i in range(len(C))]for j in range(len(C))]

if(0):
  figure()
  plt.subplot(1,2,1)
  plt.imshow(corrs,interpolation='nearest')
  plt.xticks(arange(10),cities,rotation=-90)
  plt.yticks(arange(10),cities,rotation=0)
  plt.subplot(1,2,2)
  plt.imshow(Fitdists,interpolation='nearest')
  plt.xticks(arange(10),cities,rotation=-90)
""" """
#