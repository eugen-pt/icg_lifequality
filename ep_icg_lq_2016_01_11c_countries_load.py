# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 16:22:28 2016

@author: ep
"""

"""      Imports      """

import xlrd

"""    Local defs     """
def v(r):
    return [c.value for c in r]


""" Full Table """

xlspath = u'src/countries.xls'

"""      Consts       """

wb = xlrd.open_workbook(xlspath)
s = wb.sheet_by_index(0)

countries_nums = v(s.col(0))
countries = v(s.col(1))

""" Local Definitions """



"""   Data Loading    """



"""    Processing     """


"""      Saving       """



""" """
#
