# -*- coding: utf-8 -*-
"""
Created on Thu May 28 11:33:24 2015

@author: ep
"""
from copy import copy


def loadPolys(fname):
    with open(fname) as f:
        LS    = f.readlines()
    lj=1
    PS = {}
    PS['nps'] = []
    PS['ruName'] = LS[0][:-1]
    while(lj<len(LS)-1):
        try:
            np = int(LS[lj][:-1])
        except:
            print(fname)
            print(lj)
            print(LS[lj])
            raise ValueError()
        lj=lj+1
        P = []
        while((LS[lj][0]=='\t')or(LS[lj][0]==' ')):
            P.append(map(float,LS[lj].split()))
            if(P[-1][0]<0):
                P[-1][0]=P[-1][0]+360
            lj=lj+1
        PS[np] = copy(P) 
        PS['nps'].append(np)
        lj=lj+1    
    return PS


#def

import glob

try:
    PSs
except:    
    PSs =  {}  
    
    files = glob.glob('polys/RU*')
    
    for fname in files:
        codename = fname[len('polys/RU'):fname.index('.')].replace('-','')
        PSs[codename] = loadPolys(fname)

    codenames= PSs.keys()
    codenames.remove('')
    
    import matplotlib.pyplot as plt
    import shapely.geometry as geo

    RuPoly = geo.Polygon(PSs[''][1])    
    for cn in codenames:
        newPs = []
        for n in PSs[cn]['nps']:
            hpoly = geo.Polygon(PSs[cn][n])
            try:
                x = hpoly.intersection(RuPoly)
            except:
                print(cn)
                print(n)
            try:
                for gp in x.geoms:            
                    newPs.append(array(gp.boundary.coords.xy).transpose())
            except:
                newPs.append(array(x.boundary.coords.xy).transpose())
        PSs[cn]['nps'] = range(1,len(newPs)+1)
        for j in PSs[cn]['nps']:        
            PSs[cn][j] = newPs[j-1]

codenames= PSs.keys()
codenames.remove('')

def RuMap_plot_codenames_colors(codenames_colors,figname = ''):
    patches = []   
    colors  = []
    for cn in codenames:#['']:
        for n in [1]:#PSs[cn]['nps']:
            patches.append(plt.Polygon(PSs[cn][n]))
            if(cn in codenames_colors):
                colors.append(codenames_colors[cn])
            else:
                colors.append(0)
#            ax.add_patch(plt.Polygon(PSs[cn][n]))
    from matplotlib.collections import PatchCollection
    figure(figname)
    ax = subplot(1,1,1)
#    p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=1)
    p = PatchCollection(patches, cmap=matplotlib.cm.gray_r, alpha=1)
#    for n in [1]:#PSs[cn]['nps']:
#        ax.add_patch(plt.Polygon(PSs[''][n]))
    p.set_array(np.array(colors))
    ax.add_collection(p)
    plt.colorbar(p)
    
    plt.show()
    ax.autoscale_view()
    

def RuMap_plotCodenames(regions):
    if(type(regions)==type('')) or(type(regions)==type(u'')):
        regions=[regions]
        
    regions_iD = {regions[j]:j for j in range(len(regions))}
    codenames_colors = {s:regions_iD[s]+1 if s in regions_iD else 0 for s in codenames}    
    RuMap_plot_codenames_colors(codenames_colors,figname='|'.join(regions))    
    
#if __name__ == "__main__":
#    codenames_colors = {s:0 if s=='AMU' else 1 for s in codenames}    
#    
##    colors = 100*np.random.rand(len(patches))
##   
##    colors = [100-mean(array(PSs[cn][1])[:,1]) for cn in codenames]
#    patches = []   
#    colors  = []
#    for cn in codenames:#['']:
#        for n in [1]:#PSs[cn]['nps']:
#            patches.append(plt.Polygon(PSs[cn][n]))
#            colors.append(codenames_colors[cn])
##            ax.add_patch(plt.Polygon(PSs[cn][n]))
#    from matplotlib.collections import PatchCollection
#    figure()
#    ax = subplot(1,1,1)
#    p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=1)
##    for n in [1]:#PSs[cn]['nps']:
##        ax.add_patch(plt.Polygon(PSs[''][n]))
#    p.set_array(np.array(colors))
#    ax.add_collection(p)
#    plt.colorbar(p)
#    
#    plt.show()
#    ax.autoscale_view()


codenames_regions = {s:'' for s in codenames}


""" """
codenames_regions[u'AD']=u'Республика Адыгея'
codenames_regions[u'AL']=u'Республика Алтай'
codenames_regions[u'ALT']=u'Алтайский край'
codenames_regions[u'AMU']=u'Амурская область'
codenames_regions[u'ARK']=u'Архангельская область'
codenames_regions[u'AST']=u'Астраханская область'
codenames_regions[u'BA']=u'Республика Башкортостан'
codenames_regions[u'BEL']=u'Белгородская область'
codenames_regions[u'BRY']=u'Брянская область'
codenames_regions[u'BU']=u'Республика Бурятия'
codenames_regions[u'CE']=u'Чеченская Республика'
codenames_regions[u'CHE']=u'Челябинская область'
codenames_regions[u'CHU']=u'Чукотский автономный округ'
codenames_regions[u'CU']=u'Чувашская республика'
codenames_regions[u'DA']=u'Республика Дагестан'
codenames_regions[u'IN']=u'Республика Ингушетия'
codenames_regions[u'IRK']=u'Иркутская область'
codenames_regions[u'IVA']=u'Ивановская область'
codenames_regions[u'KAM']=u'Камчатский край'
codenames_regions[u'KB']=u'Республика Кабардино-Балкария'
codenames_regions[u'KC']=u'Карачаево-Черкесская Республика'
codenames_regions[u'KDA']=u'Краснодарский край'
codenames_regions[u'KEM']=u'Кемеровская область'
codenames_regions[u'KGD']=u'Калининградская область'
codenames_regions[u'KGN']=u'Курганская область'
codenames_regions[u'KHA']=u'Хабаровский край'
codenames_regions[u'KHM']=u'Ханты-Мансийский АО'
codenames_regions[u'KIR']=u'Кировская область'
codenames_regions[u'KK']=u'Республика Хакасия'
codenames_regions[u'KL']=u'Республика Калмыкия'
codenames_regions[u'KLU']=u'Калужская область'
codenames_regions[u'KO']=u'Республика Коми'
codenames_regions[u'KOS']=u'Костромская область'
codenames_regions[u'KR']=u'Республика Карелия'
codenames_regions[u'KRS']=u'Курская область'
codenames_regions[u'KYA']=u'Красноярский край'
codenames_regions[u'LEN']=u'Санкт-Петербург и Ленинградская область'
codenames_regions[u'LIP']=u'Липецкая область'
codenames_regions[u'MAG']=u'Магаданская область'
codenames_regions[u'ME']=u'Республика Марий Эл'
codenames_regions[u'MO']=u'Республика Мордовия'
codenames_regions[u'MOS']=u'Москва и область'
#codenames_regions[u'MOW']=u'Москва'
codenames_regions[u'MUR']=u'Мурманская область'
codenames_regions[u'NEN']=u'Ненецкий АО'
codenames_regions[u'NGR']=u'Новгородская область'
codenames_regions[u'NIZ']=u'Нижегородская область'
codenames_regions[u'NVS']=u'Новосибирская область'
codenames_regions[u'OMS']=u'Омская область'
codenames_regions[u'ORE']=u'Оренбургская область'
codenames_regions[u'ORL']=u'Орловская область'
codenames_regions[u'PER']=u'Пермский край'
codenames_regions[u'PNZ']=u'Пензенская область'
codenames_regions[u'PRI']=u'Приморский край'
codenames_regions[u'PSK']=u'Псковская область'
codenames_regions[u'ROS']=u'Ростовская область'
codenames_regions[u'RYA']=u'Рязанская область'
codenames_regions[u'SA']=u'Республика Саха (Якутия)'
codenames_regions[u'SAK']=u'Сахалинская область'
codenames_regions[u'SAM']=u'Самарская область'
codenames_regions[u'SAR']=u'Саратовская область'
codenames_regions[u'SE']=u'Республика Северная Осетия-Алания'
codenames_regions[u'SMO']=u'Смоленская область'
#codenames_regions[u'SPE']=u'Санкт-Петербургmatplotlib.cm.jet'
codenames_regions[u'STA']=u'Ставропольский край'
codenames_regions[u'SVE']=u'Свердловская область'
codenames_regions[u'TA']=u'Татарстан'
codenames_regions[u'TAM']=u'Тамбовская область'
codenames_regions[u'TOM']=u'Томская область'
codenames_regions[u'TUL']=u'Тульская область'
codenames_regions[u'TVE']=u'Тверская область'
codenames_regions[u'TY']=u'Республика Тыва'
codenames_regions[u'TYU']=u'Тюменская область'
codenames_regions[u'UD']=u'Удмуртская республика'
codenames_regions[u'ULY']=u'Ульяновская область'
codenames_regions['VGG']=u'Волгоградская область'
codenames_regions[u'VLA']=u'Владимирская область'
codenames_regions[u'VLG']=u'Вологодская область'
codenames_regions[u'VOR']=u'Воронежская область'
codenames_regions[u'YAN']=u'Ямало-Ненецкий АО'
codenames_regions[u'YAR']=u'Ярославская область'
codenames_regions[u'YEV']='Еврейская автономная область'
codenames_regions[u'ZAB']=u'Забайкальский край'

regions_codenames = {codenames_regions[s]:s for s in codenames_regions}

def RuMap_plot_region_values(regions,values,figname = ''):
    codenames_colors = {}    

    not_decoded_N = 0;    
    not_decoded = [];
    for j in   range(len(regions)):
        r = regions[j]
        if(r in PSs):
            codenames_colors[r] = values[j]
        elif(r in regions_codenames)and(regions_codenames[r] in PSs):
            codenames_colors[regions_codenames[r]] = values[j]
        else:
            not_decoded_N=not_decoded_N+1
            not_decoded.append(regions[j])
    if(not_decoded_N>0):
        print('  %i not decoded : %s'%(not_decoded_N,', '.join(not_decoded)))
    RuMap_plot_codenames_colors(codenames_colors,figname=figname)    

#