# -*- coding: utf-8 -*-
"""
Created on Thu May 28 11:26:48 2015

@author: ep
"""

import urllib2
import os

root_url = 'http://be.gis-lab.info/data/osm_dump/poly/'

r =[s.split('\t') for s in urllib2.urlopen(root_url+'index').read().split('\n')]


codenames = [a[0] for a in r]
runames = [a[1] for a in r]


try:
    os.mkdir('polys')
except:
    pass

for codename in codenames:
    text = urllib2.urlopen(root_url+codename+'.poly').read()
    with open('polys/'+codename+'.poly','w') as f:
        f.write(text)
#