# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 11:13:21 2015

@author: ep
"""

import numpy as np
from numpy import array,sum,mean

Data_fname = '2015-03-18_13-05-35__v9_Regions__pagesC_percityAgregated_OKVED_cities.xls'
Data_fname = '2015-03-18_13-05-35__v9_Regions__pagesC_percityAgregated_OKVED_regions.xls'
#outfname = fname.replace('xls','tsv')

""" data preparation """
if(1):
    print('loading..')
    import xlrd
    
    
    wb = xlrd.open_workbook(Data_fname)
    
    s=wb.sheet_by_index(0)
    
    Data = array([[c.value for c in s.row(j)[3:]] for j in range(1,len(s.col(0)))])
    Cities = [c.value for c in s.row(0)[3:]]
    sCats = [c.value for c in s.col(1)[1:]]
    Qs = [c.value for c in s.col(2)[1:]]
    Fnames = [c.value for c in s.col(0)[1:]]
    print('done')

Qs_quoted = [s[0]=='"' for s in Qs]
Qs_quoted_d = {j:1 for j in range(len(Qs_quoted)) if Qs_quoted[j]}

Qs_nq = [Qs[j] for j in range(len(Qs)) if j not in Qs_quoted_d]
""" unique cats in the same order  + numbers per cat """

uCats = [sCats[0]]
uCatNums = [[0]]
for sj in range(1,len(sCats)):
    cat = sCats[sj]
    if(cat in uCats):
        
       uCatNums[uCats.index(cat)].append(sj) 
    else:
       uCats.append(cat) 
       uCatNums.append([sj])

#not quoted nums
uCatNums_nq = [ [j for j in a if j not in Qs_quoted_d] for a in uCatNums]

# =)
shortCats = []
for c in uCats:
    tc = c.split('_')
    for j in range(len(tc)):
        if(len(tc[-1-j])>0):
            shortCats.append(tc[-1-j])
            break

""" Data per category """

# old norm - we took sum per category
#catData = [sum(Data[a,:],axis=0) for a in uCatNums]

#new norm - divided by the number of Qs, so - mean N per one Q for each category
catData = [mean(Data[a,:],axis=0) for a in uCatNums]
catData_nq = [mean(Data[a,:],axis=0) for a in uCatNums_nq]

""" Normalized """

temp_sum = sum(array(catData),axis=0)

ncatData = array([a/temp_sum for a in catData])
ncatData_nq = array([a/temp_sum for a in catData_nq])

nData = [a/temp_sum for a in Data]
nData_nq = [Data[j]/temp_sum for j in range(len(Data)) if j not in Qs_quoted_d]

"""  """

catCorrMx = np.corrcoef(ncatData)
catCorrMx_nq = np.corrcoef(ncatData_nq)


""" """
#