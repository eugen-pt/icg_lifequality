# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work


6 = src2, with all the words !

9 = new Yandex interface

10 - timeout for loading page, yandex cities

"""

# Set up some global variables

do_fromTheEnd = 0

num_fetch_threads = 8

pauseK = .05;
pauseMin = .05;
pauseAfter = .05;

pauseAfterCityEntered = .3;



from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from scipy import misc

import re
#from ep_bioinfo_2014_12_10f_src_reader1 import *
from ep_bioinfo_2015_01_29d_src_reader3_3 import *

from ep_icg_lq_2015_01_29a_gradoteka_citieslist import *

import datetime


# System modules
from Queue import Queue
from threading import Thread
import time

#cities = 'НОВОСИБИРСК	ТОМСК	ОМСК	КЕМЕРОВО	ВЛАДИВОСТОК	КАЛИНИНГРАД	СОЧИ	МУРМАНСК	САНКТ-ПЕТЕРБУРГ	МОСКВА'

#cities from https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD:50_%D0%BA%D1%80%D1%83%D0%BF%D0%BD%D0%B5%D0%B9%D1%88%D0%B8%D1%85_%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%BE%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8
#cities = 'Москва	Санкт-Петербург	Новосибирск	Екатеринбург	Нижний Новгород	Казань	Самара	Челябинск	Омск	Ростов-на-Дону	Уфа	Красноярск	Пермь	Волгоград	Воронеж	Саратов	Краснодар	Тольятти	Тюмень	Ижевск	Барнаул	Ульяновск	Иркутск	Владивосток	Ярославль	Хабаровск	Махачкала	Оренбург	Томск	Новокузнецк	Кемерово	Астрахань	Рязань	Набережные	Челны	Пенза	Липецк	Тула	Киров	Чебоксары	Калининград	Курск	Улан-Удэ	Ставрополь	Магнитогорск	Брянск	Иваново	Тверь	Сочи	Белгород	Симферополь'
#cities = 'Москва	Санкт-Петербург	Новосибирск	Екатеринбург	Нижний Новгород'#	Казань	Самара	Челябинск	Омск	Ростов-на-Дону	Уфа	Красноярск	Пермь	Волгоград	Воронеж	Саратов	Краснодар	Тольятти	Тюмень	Ижевск	Барнаул	Ульяновск	Иркутск	Владивосток	Ярославль	Хабаровск	Махачкала	Оренбург	Томск	Новокузнецк	Кемерово	Астрахань	Рязань	Набережные	Челны	Пенза	Липецк	Тула	Киров	Чебоксары	Калининград	Курск	Улан-Удэ	Ставрополь	Магнитогорск	Брянск	Иваново	Тверь	Сочи	Белгород	Симферополь'
#cities = cities.split('\t')

#cities = RuCities


cities = RuCities_all

cities = YaRuRegions

cities = YaRuCities_all

cities.extend(YaRuRegions) 


#YaRegions_NumsDict


#LQj_done={}

from_date_full='17.03.2014'
to_date_full='17.03.2015'

PreStr_done = {}
def savingPreStr(LQj,qstr,cityj):
  #print(cities[cityj].decode('utf8')) 
  return ','.join([s.replace(',','&') for s in [LQs_files[LQj] ,LQs_cats[LQj],qstr,cities[cityj].decode('utf8')]]) ### .decode('utf8')


try:
#if(1):
#  aaa
  print('Checking if file already exists..')
  outfname = '2015-03-18_13-05-35__v9_Regions__pagesC_percity_OKVED.csv'
  if(1):
    """ Deals badly with commas in qs"""
    with open(outfname) as f:
      LS = f.readlines()
    print(len(LS))
#    LS = [[s.replace('&',',').replace(u'&',',') for s in L.split(',')[0:4]] for L in LS[1:]]
    LS = [[s for s in L.split(',')[0:4]] for L in LS[1:]]
  else:
    import csv
    
    with open(outfname,'rb') as f:
      r = csv.reader(f)
      print(len(LS))
      LS= [row for row in r]
      LS = [L[0:4] for L in LS[1:]]
      
  for L in LS:
    PreStr = ','.join(L)
#    print(PreStr)
    PreStr_done[unicode(PreStr)]=1
      
#  pass
except:
  outfname = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__v10_YaRuCities__'+'pagesC_percity_OKVED.csv'

  with open(outfname,'w') as f:
    f.write('original file,category,q,city,Res\r\n')

#
#aaa






def downloadEnclosures(i, q,outq,wipq,doneq):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print '%s: Logining..' % i
    driver = webdriver.Firefox()
    driver.set_page_load_timeout(30)
    #driver.get("http://www.python.org")
#    driver.get('https://passport.yandex.ru/passport?mode=auth')
#
#    driver.find_element_by_xpath('//input[@id="login"]').clear()
#    driver.find_element_by_xpath('//input[@id="login"]').send_keys('ya.kolchanov2015')
#
#    driver.find_element_by_xpath('//input[@id="passwd"]').clear()
#    driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
#
#    driver.find_element_by_xpath('//span[@class="button-content"]').click()
#
    print '%s: log in OK' % i
    qstr = ''
    LQj = -153
    while True:
        try:
            print '%s: Reading next Q' % i
            LQj,qstr,cityj = q.get()
            
            print '%s: q=%s' % (i,qstr)
            
            
            wip_queue.put((LQj,qstr,cityj,))
            
            url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(qstr,YaRegions_NumsDict[cities[cityj]],from_date_full,to_date_full)
            
            print(url)
            while(1):
              try:
                driver.get(url)
                break
              except:
                pass
            pause(pauseMin+rand()*pauseK)
              
            
            while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
              print('%s: "Yandex" not in title! sth wrong')
              try:
                driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                pause(2)
              except:
                pause(20)
                while(1):
                  try:
                    driver.get(url)
                    break
                  except:
                    pass
                pause(pauseMin+rand()*pauseK)
            
            print '%s: url opened' % i
    
            temp = driver.title
            
    
    
            prevtitle = ''
    
      
            city = cities[cityj]
            Ns = -153
    
            prevtitle = driver.title
            
    
    
            for j in range(15):
              ttitle = driver.title
              temp=ttitle.replace(u'млн','000 000')
              temp=temp.replace(u'тыс.','000')
    
              try:
                tempN = int(''.join(re.findall('[0-9]',temp)))
              except:
                if(u'ничего не найдено' in temp):
                  tempN=0
                else:
                  raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
    
    #            print(ttitle)
    #            print(tempN)
              if(prevtitle!=ttitle):
                break
              pause(pauseAfter+rand()*pauseK)
            Ns = tempN
    
            print '%s: Done' % i
            outq.put((LQj,qstr,cityj,Ns))
            doneq.put((LQj,qstr,cityj,))
            # instead of really downloading the URL,
            # we just pretend and sleep
    #        time.sleep(i + 2)
    
            q.task_done()
        except:
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1);


#aaa
# Filling the Queue
print("Filling the Queue")


NAlreadyDone = 0;
PreStr_done

enclosure_queue = Queue()

for LQjt in range(len(LQs_qs)):
  if(do_fromTheEnd):
    LQj = len(LQs_qs)-1-LQjt
  else:
    LQj = LQjt
  qstr = LQs_qs[LQj]
  try:
    print(qstr)
  except:
    qstr=qstr[0:-1]
    print(qstr)

  """ adding unquoted stuff """
  for cityj in range(len(cities)):
    
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    
    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
#      print('done already')
      NAlreadyDone=NAlreadyDone+1
      continue
  
    enclosure_queue.put((LQj,qstr,cityj))

  """ adding quoted stuff """
  if((' ' in qstr) or (u' ' in qstr) or ('-' in qstr) or (u'-' in qstr)):
    if((qstr[0]!='"') or (qstr[-1]!='"')):
      qstr='"'+qstr+'"'
#    print(qstr)
    
  for cityj in range(len(cities)):
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    
    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
#      print(PreStr+' done already')
      NAlreadyDone=NAlreadyDone+1
      continue
  
    enclosure_queue.put((LQj,qstr,cityj))

print('Queue filled.')
#aaa

print("**************************************************")

print('NAlreadyDone = %i'%NAlreadyDone)
print('Left to do = %i'%enclosure_queue.qsize())

import time
time.sleep(4)

output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()

def CheckWIPnDone():
  print 'CheckWIPnDone:starting'
  wips = {}
  wipN=0
  while(not wip_queue.empty()):
    LQj,qstr,cityj = wip_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    wips[PreStr]=(LQj,qstr,cityj)
    wipN=wipN+1
    
  print('CheckWIPnDone:' +' found %i wipStuff'%wipN)  
  while(not doneq_queue.empty()):
    LQj,qstr,cityj = doneq_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    try:
      wips[PreStr]=0
      wipN=wipN-1
    except:
      print('somehow done but not wip : %s'%PreStr)
#      raise ValueError('somehow wip but not done : %s'%PreStr)
  print('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)  

  for s in  wips.keys():
    t = wips[s]
    if(t==0):
      pass
    else:
      (LQj,qstr,cityj) = t
      print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(t)
      enclosure_queue.put((LQj,qstr,cityj))

  
  print 'CheckWIPnDone:done'

if(1):

  print('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
  print('Workers started.')
  
  
  import psutil
  
  def savingThread(q,q_in):
    print('savingThread: Sarted.')
    while(True):
      in_empty_n = 0
      
      ntdn = 0
      while(q.empty()):
        if(q_in.empty()):
          in_empty_n=in_empty_n+1
          print('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
        else:
          print('savingThread: nothing to do..')
          ntdn=ntdn+1
        
        print('~ %i left undone, ntd=%i'%(q_in.qsize(),ntdn))      
        
        if(ntdn>20):
          print('='*10)
          print('Restarting')
          print('='*10)
          
          print('Killing firefoxes..')
          # Restarting because sth went wrong!
          for proc in psutil.process_iter():
            if proc.name == 'firefox':
              proc.kill()
          print('done.')    
          CheckWIPnDone()    
          print('restarting workers..')
          for i in range(num_fetch_threads):
              worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
              worker.setDaemon(True)
              worker.start()
          ntdn=0    
          print('done.')
          print('='*10)
              
        
        if(in_empty_n>100):
          print('savingThread: nothing to DO! returning.')
          return
        pause(1)
      print('savingThread: something to do !)')
      with open(outfname,'a') as f:
        while(q.empty()==0):
          LQj,qstr,cityj,Ns=q.get()
          f.write(savingPreStr(LQj,qstr,cityj)+','+str(Ns)+'\r\n')
  
  
  #print('waiting for first output..')
  #while(output_queue.empty()):
  #  pause(.1)
  #print('starting saver..')
  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,))
  worker.setDaemon(True)
  worker.start()
  
  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
  print '*** Main thread waiting'
  enclosure_queue.join()
  print '*** Main thread Done waiting'

""" """
#

