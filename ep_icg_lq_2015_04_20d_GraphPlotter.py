# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 12:36:03 2015

@author: ep
"""

try:
    Data
    catCorrMx_nq
    shortCats
except:    

    from ep_icg_lq_2015_04_20c_data_loader import *
    

    
catDistMx_nq = 1-catCorrMx_nq

import networkx as nx


#pshortCats = [s.replace(' ','_') for s in shortCats]
pshortCats=shortCats
for corrT in [0.5,0.65,0.8,0.9]:
    with open('temp_%.2f.tsv'%corrT,'w') as f:
        for cj in range(len(uCats)):
            for ci in range(cj+1,len(uCats)):
                if(catCorrMx_nq[cj,ci]>corrT):
                    f.write('%s\t%s\t%f\n'%(pshortCats[cj],pshortCats[ci],catCorrMx_nq[cj,ci]))

for corrT in [0.5,0.65,0.8,0.9]:
    with open('temp_corr_lt_%.2f.tsv'%corrT,'w') as f:
        for cj in range(len(uCats)):
            for ci in range(cj+1,len(uCats)):
                if(catCorrMx_nq[cj,ci]<-corrT):
                    f.write('%s\t%s\t%f\n'%(pshortCats[cj],pshortCats[ci],catCorrMx_nq[cj,ci]))

edges = []
edges_labels = {}
have_Links = {}
for cj in range(len(uCats)):
    for ci in range(cj+1,len(uCats)):
        if(catCorrMx_nq[cj,ci]>0.65):
            have_Links[shortCats[cj]]=1
            have_Links[shortCats[ci]]=1
            edges.append((shortCats[cj],shortCats[ci],{'weight':catCorrMx_nq[cj,ci]}))
            edges_labels[(shortCats[cj],shortCats[ci])] = '%.2f'%catCorrMx_nq[cj,ci]
            
G = nx.Graph()
for c in have_Links:
    G.add_node(c)
G.add_edges_from(edges)

#pos = nx.random_layout(G)
pos = nx.spring_layout(G)

#nx.draw_networkx_nodes(G,pos)
nx.draw_networkx_edges(G,pos,edge_cmap='jet',edge_vmin=0,edge_vmax=2)
nx.draw_networkx_edge_labels(G,pos,edges_labels)
nx.draw_networkx_labels(G,pos,font_size=7)


""" """
#