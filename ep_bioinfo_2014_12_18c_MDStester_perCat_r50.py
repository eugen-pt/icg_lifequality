# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 14:21:40 2014

@author: root
"""

from ep_bioinfo_2014_12_16a_src_reader3_r50 import *
import os

#D = LQs_D_CityScoreNorm
D = LQs_D_CityPopulationNorm
#D = LQs_CM_D_RegionPopulationNorm

D = (D.transpose()/sum(D,axis=1).transpose()).transpose()

outdir='MDS_figures_r50_CityScoreNorm2'
outdir='MDS_figures_r50_CityPopulationNorm3_eachN'
#

try:
    os.mkdir(outdir)
except:
    pass

Cats = array(LQs_cats)
uc = unique(Cats)

#uc_Eng = ['Architecture','Dekor-Prikl Art','Design','Drawing Art?','Innovation','Cinematography','Crime','Small business','Automobiles','Music','Education','General','Consumer basket','Mass media','Sport','Theatr','Farmer','Imaginative literature']
uc_Eng = ['Architecture','Arts and Crafts','Design','Diseases','Fine Art','Innovation','Cinematography','Criminal','Small Business','Car makers','Music','Education','General','Consumer basket','Mass media','Sport','Theatre','Farmer','Imagin Lit','Info staffing']

seed = np.random.RandomState(1)

from sklearn import manifold

from sklearn.decomposition import PCA

#corrs = corrcoef(D.transpose())
#similarities = 1-corrs
#
## have been used for picture generation:
##mds = manifold.MDS(n_components=2, metric=False, max_iter=3000, eps=1e-12,
##                    dissimilarity="precomputed", random_state=seed, n_jobs=1,
##                    n_init=300)
#
#mds = manifold.MDS(n_components=2, metric=False, max_iter=3000, eps=1e-12,
#                    dissimilarity="precomputed", random_state=seed, n_jobs=1,
#                    n_init=300)
#                    
cities = LQs_engCities
#                    
#R=mds.fit(similarities)           
#C = R.fit_transform(similarities)



if(1):
  figure()
  for ucj in range(len(uc)):
#    subplot(4,5,ucj+1)
    print('%i/%i'%(ucj+1,len(uc)))
    corrs = corrcoef(D[Cats==uc[ucj],:].transpose())
    similarities = 1-corrs
    mds = manifold.MDS(n_components=2, metric=False, max_iter=30000, eps=1e-12,
                        dissimilarity="precomputed", random_state=seed, n_jobs=4,
                        n_init=1000)
#    R=mds.fit(similarities)           
    C = mds.fit_transform(similarities)
    
    
    for xj in range(len(C)):
      x=C[xj]
      plot(x[0],x[1],'.')    
      plt.text(   x[0],x[1], unicode(cities[xj]) )
    title(uc_Eng[ucj])
    show()
    savefig(outdir+u'/fig_'+uc_Eng[ucj]+'.png', bbox_inches='tight')
    clf()

# Can be used to assess the quality of MDS: matrice should be similar
#  (except for colors : high correlations = low istances)
if(0):

  Fitdists = [ [ sqrt(sum((C[j]-C[i])**2)) for i in range(len(C))]for j in range(len(C))]

  figure()
  plt.subplot(1,2,1)
  plt.imshow(corrs,interpolation='nearest')
  plt.xticks(arange(10),cities,rotation=-90)
  plt.yticks(arange(10),cities,rotation=0)
  plt.title('Correlations')
  plt.subplot(1,2,2)
  plt.imshow(Fitdists,interpolation='nearest')
  plt.xticks(arange(10),cities,rotation=-90)
  plt.title('Distances')
""" """
#