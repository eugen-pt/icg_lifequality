# -*- coding: utf-8 -*-
"""
Created on Thu Feb  5 10:56:13 2015

@author: ep
"""

import xlwt

#infname = '2015-01-29_16-51-21__v7_Gradoteka__pagesCount_percity.csv'
infname = '2015-01-29_17-11-26__v8_Gradoteka__pagesCount_percity_OKVED.csv'
infname = '2015-03-18_13-05-35__v9_Regions__pagesC_percity_OKVED.csv'

outfname = infname.replace('percity','percityAgregated').replace('csv','xls')


Data = {}

if(0):
  # deals with ,"aa,vv", badly =(
  with open(infname) as f:
    LS = f.readlines()
    
  LS = [a.split(',') for a in LS[1:]]
else:
  import csv
  with open(infname,'rb') as f:
    reader= csv.reader(f)
    LS = [row for row in reader]
    LS=LS[1:]

allCities = {}

Qstowrite = {}


for A in LS:
  fname = A[0]
  cat = A[1]
  qtowrite = unicode(A[2])
  city = A[3]
  
  allCities[city]=1
    
  q = qtowrite.replace('"','')
  q = unicode(q)
  
  Qstowrite[q] = qtowrite
    
  n = A[4]
  try:
    Data[fname][cat][q][city]=n
  except:
    try:
      Data[fname][cat][q] = {}
    except:
      try:
        Data[fname][cat] = {}
      except:
        try:
          Data[fname]={}
        except:
          pass
        Data[fname][cat] = {}
      Data[fname][cat][q] = {}    
    Data[fname][cat][q][city]=n

import xlwt
wb = xlwt.Workbook()
ws = wb.add_sheet('A Test Sheet')

ws.write(0,0,'filename')
ws.write(0,1,'category')
ws.write(0,2,'q')

allCities = allCities.keys()
allCities.sort()

cn=0
for city in allCities:
  ws.write(0,3+cn,unicode(city))
  cn=cn+1

rn=0
Afname=Data.keys()
Afname.sort()
for fname in Afname:
  Acat = Data[fname].keys()
  Acat.sort()
  for cat in Acat:
    Aq = Data[fname][cat].keys()
    Aq.sort()
    for q in Aq:
      rn=rn+1
      ws.write(rn,0,unicode(fname))
      ws.write(rn,1,unicode(cat))
      ws.write(rn,2,unicode(Qstowrite[q]))
      cn=0
      for city in allCities:
        try:
          ws.write(rn,3+cn,int(Data[fname][cat][q][city]))
        except:
          ws.write(rn,3+cn,-153)
        cn=cn+1

wb.save(unicode(outfname))

""" """
#