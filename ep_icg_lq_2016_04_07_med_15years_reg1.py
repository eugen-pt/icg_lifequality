# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work


6 = src2, with all the words !

9 = new Yandex interface

10 - timeout for loading page, yandex cities

"""

# Set up some global variables

do_fromTheEnd = 0

num_fetch_threads = 1

pauseK = .05;
pauseMin = .05;
pauseAfter = .05;
pauseAfterCityEntered = .3;

#pauseK = 2;
#pauseMin = 1
#pauseAfter = 1
#pauseAfterCityEntered = 1

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#import random.random as rand
def rand():
    return 0.2

import re
#from ep_bioinfo_2014_12_10f_src_reader1 import *
#from ep_bioinfo_2015_01_29d_src_reader3_3 import *

from ep_icg_lq_2015_01_29a_gradoteka_citieslist import *

import datetime


# System modules
from Queue import Queue
from threading import Thread
import time

from time import sleep as pause

#cities = 'НОВОСИБИРСК	ТОМСК	ОМСК	КЕМЕРОВО	ВЛАДИВОСТОК	КАЛИНИНГРАД	СОЧИ	МУРМАНСК	САНКТ-ПЕТЕРБУРГ	МОСКВА'

#cities from https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD:50_%D0%BA%D1%80%D1%83%D0%BF%D0%BD%D0%B5%D0%B9%D1%88%D0%B8%D1%85_%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%BE%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8
#cities = 'Москва	Санкт-Петербург	Новосибирск	Екатеринбург	Нижний Новгород	Казань	Самара	Челябинск	Омск	Ростов-на-Дону	Уфа	Красноярск	Пермь	Волгоград	Воронеж	Саратов	Краснодар	Тольятти	Тюмень	Ижевск	Барнаул	Ульяновск	Иркутск	Владивосток	Ярославль	Хабаровск	Махачкала	Оренбург	Томск	Новокузнецк	Кемерово	Астрахань	Рязань	Набережные	Челны	Пенза	Липецк	Тула	Киров	Чебоксары	Калининград	Курск	Улан-Удэ	Ставрополь	Магнитогорск	Брянск	Иваново	Тверь	Сочи	Белгород	Симферополь'
#cities = 'Москва	Санкт-Петербург	Новосибирск	Екатеринбург	Нижний Новгород'#	Казань	Самара	Челябинск	Омск	Ростов-на-Дону	Уфа	Красноярск	Пермь	Волгоград	Воронеж	Саратов	Краснодар	Тольятти	Тюмень	Ижевск	Барнаул	Ульяновск	Иркутск	Владивосток	Ярославль	Хабаровск	Махачкала	Оренбург	Томск	Новокузнецк	Кемерово	Астрахань	Рязань	Набережные	Челны	Пенза	Липецк	Тула	Киров	Чебоксары	Калининград	Курск	Улан-Удэ	Ставрополь	Магнитогорск	Брянск	Иваново	Тверь	Сочи	Белгород	Симферополь'
#cities = cities.split('\t')
#cities = RuCities
#cities = RuCities_all
#cities = YaRuRegions
#cities = YaRuCities_all
#cities.extend(YaRuRegions) 

#aaa
""" """
""" Setting starting working parameters """
""" """


cities = YaRuRegions


print('len(YaRuRegions)=%i'%len(YaRuRegions))

# o_O ?

if(len(cities)!=86):
    for c in cities:
        print(c)
    raise ValueError('Length of YaRuRegions should be 86')



#delete_this_if_len_of_cities_is_86

#cities = [u'Новосибирская область',u'Кемеровская область',u'Алтайский край',u'Томская область',u'Омская область',u'Рязанская область',u'Архангельская область',u'Хабаровский край',u'Астраханская область',u'Магаданская область']
#NSO_city = u'Новосибирская область'
#Novosibirsk_city = u'Новосибирск'
#for s in cities+[NSO_city,Novosibirsk_city]:
#    print(s),
#    print(YaRegions_NumsDict[s])

#aaa

#TODO
#outfname = '2015-12-16_10-58-44__v13__KNA_wtf_wtF__pagesC_percity.csv'
outfname = '2016-04-08_12-56-48__v13b__KNA_wtf_wtF__pagesC_percity.csv'

#NSO_subregions = ['Татарский','Карасукский','Барабинский','Тогучинский','Ордынский','Коченевский','Кыштовский']
#Novosibirsk_subregions = ['Дзержинский','Железнодорожный','Заельцовский','Калининский','Кировский','Ленинский','Октябрьский','Первомайский','Советский','Центральный']
#
#NSO_allSubregions = """► Баганский район‎ (6: 3 кат., 3 с.)► Барабинский район‎ (9: 4 кат., 5 с.)► Болотнинский район‎ (8: 4 кат., 4 с.)► Венгеровский район‎ (9: 6 кат., 3 с.)► Доволенский район‎ (8: 4 кат., 4 с.)З► Здвинский район‎ (9: 3 кат., 6 с.)И► Искитимский район‎ (11: 6 кат., 5 с.)К► Карасукский район‎ (7: 4 кат., 3 с.)► Каргатский район‎ (8: 3 кат., 5 с.)► Колыванский район‎ (9: 4 кат., 5 с.)► Коченёвский район‎ (9: 6 кат., 3 с.)► Кочковский район‎ (8: 5 кат., 3 с.)► Краснозёрский район‎ (6: 3 кат., 3 с.)► Куйбышевский район Новосибирской области‎ (8: 4 кат., 4 с.)► Купинский район‎ (7: 3 кат., 4 с.)► Кыштовский район‎ (26: 5 кат., 21 с.)М► Маслянинский район‎ (9: 5 кат., 4 с.)► Мошковский район‎ (9: 6 кат., 3 с.)Н► Новосибирский район‎ (9: 5 кат., 4 с.)О► Ордынский район‎ (11: 4 кат., 7 с.)С► Северный район Новосибирской области‎ (10: 5 кат., 5 с.)► Сузунский район‎ (7: 4 кат., 3 с.)Т► Татарский район‎ (6: 3 кат., 3 с.)► Тогучинский район‎ (11: 6 кат., 5 с.)У► Убинский район‎ (9: 4 кат., 5 с.)[×] Упразднённые районы Новосибирской области‎ (7: 7 с.)► Усть-Таркский район‎ (9: 4 кат., 5 с.)Ч► Чановский район‎ (8: 4 кат., 4 с.)► Черепановский район‎ (11: 5 кат., 6 с.)► Чистоозёрный район‎ (6: 3 кат., 3 с.)► Чулымский район‎ (8: 5 кат., 3 с.) """
#NSO_allSubregions=re.findall('([^ ]+) район',NSO_allSubregions)
#NSO_allSubregions.remove('Новосибирский')
#
#NSO_subregions = NSO_allSubregions
""" """


source_qs= [u'свадьба',u'субсидии',u'"рост цен"',u'ипотека',u'кризис',u'кредит',u'инфляция']
#source_qs= [u'"рост цен"']
source_cats = ['00' for q in source_qs]
#source_qs = ['нефть','ямы на дорогах']
#aaa

 
	

# Термины
	
source_qs= [u'"Гепатит E"',u'Бериллиоз',u'"Нервная булимия"',u'Отморожение ',u'"Натуральная оспа"',u'"Нервная анорексия"',u'"Почечнокаменная болезнь"',u'"Гепатит D"',u'"Рак гортани"',u'"Геморрагическая лихорадка Эбола"',u'"Атипичная пневмония"',u'"Дискинезия желчевыводящих путей"',u'"Язва двенадцатиперстной кишки"',u'"Гепатит C"',u'Описторхоз',u'Дифтерия ',u'"Рак мочевого пузыря"',u'"Рак щитовидной железы"',u'"Гепатит B"',u'"Ветряная оспа"',u'"Рак поджелудочной железы"',u'Ревматизм']

#city = u'Россия'
#city = Novosibirsk_city

"""
#Объективные показатели

Доллар
Цена на хлеб
Число новорожденных (или количество населения)
"""	
	
"""
Негативная окраска
"""

neg_adds = [u'плохо',u'ужасно',u'кошмар',u'невыносимо']
	

"""
Позитивная окраска
"""

pos_adds = [u'хорошо',u'замечательно',u'отлично',u'чудесно']

posneg_adds = {1:pos_adds,-1:neg_adds,0:[neg_adds,pos_adds]}
	
"""

Статистика по динамике (суммарно по всему Яндексу)
Статистика по регионам за последний год
"""

""" """

#01.11.2013 - 30.11.2013	2385	0,000000408568

#01.12.2013 - 31.12.2013	1787	0,000000297656
#01.01.2014 - 31.01.2014	2171	0,000000314953
#01.02.2014 - 28.02.2014	2612	0,000000429862
#01.03.2014 - 31.03.2014	4698	0,000000667481
#01.04.2014 - 30.04.2014	4793	0,000000751422
#01.05.2014 - 31.05.2014	4138	0,000000682031
#01.06.2014 - 30.06.2014	2758	0,000000499594
#01.07.2014 - 31.07.2014	2563	0,000000511113
#01.08.2014 - 31.08.2014	2240	0,000000401344
#01.09.2014 - 30.09.2014	3156	0,000000511335
#01.10.2014 - 31.10.2014	2886	0,000000421512
#01.11.2014 - 30.11.2014	1987	0,000000305978

source_dates = """
01.01.2000 - 31.03.2000
01.04.2000 - 30.06.2000
01.07.2000 - 30.09.2000
01.10.2000 - 31.12.2000
01.01.2001 - 31.03.2001
01.04.2001 - 30.06.2001
01.07.2001 - 30.09.2001
01.10.2001 - 31.12.2001
01.01.2002 - 31.03.2002
01.04.2002 - 30.06.2002
01.07.2002 - 30.09.2002
01.10.2002 - 31.12.2002
01.01.2003 - 31.03.2003
01.04.2003 - 30.06.2003
01.07.2003 - 30.09.2003
01.10.2003 - 31.12.2003
01.01.2004 - 31.03.2004
01.04.2004 - 30.06.2004
01.07.2004 - 30.09.2004
01.10.2004 - 31.12.2004
01.01.2005 - 31.03.2005
01.04.2005 - 30.06.2005
01.07.2005 - 30.09.2005
01.10.2005 - 31.12.2005
01.01.2006 - 31.03.2006
01.04.2006 - 30.06.2006
01.07.2006 - 30.09.2006
01.10.2006 - 31.12.2006
01.01.2007 - 31.03.2007
01.04.2007 - 30.06.2007
01.07.2007 - 30.09.2007
01.10.2007 - 31.12.2007
01.01.2008 - 31.03.2008
01.04.2008 - 30.06.2008
01.07.2008 - 30.09.2008
01.10.2008 - 31.12.2008
01.01.2009 - 31.03.2009
01.04.2009 - 30.06.2009
01.07.2009 - 30.09.2009
01.10.2009 - 31.12.2009
01.01.2010 - 31.03.2010
01.04.2010 - 30.06.2010
01.07.2010 - 30.09.2010
01.10.2010 - 31.12.2010
01.01.2011 - 31.03.2011
01.04.2011 - 30.06.2011
01.07.2011 - 30.09.2011
01.10.2011 - 31.12.2011
01.01.2012 - 31.03.2012
01.04.2012 - 30.06.2012
01.07.2012 - 30.09.2012
01.10.2012 - 31.12.2012
01.01.2013 - 31.03.2013
01.04.2013 - 30.06.2013
01.07.2013 - 30.09.2013
01.10.2013 - 31.12.2013
01.01.2014 - 31.03.2014
01.04.2014 - 30.06.2014
01.07.2014 - 30.09.2014
01.10.2014 - 31.12.2014
01.01.2015 - 31.03.2015
01.04.2015 - 30.06.2015
01.07.2015 - 30.09.2015
01.10.2015 - 31.12.2015
01.01.2016 - 31.03.2016
"""
source_dates = re.findall('([0-9\.]+) \- ([0-9\.]+)',source_dates)
#cities = [u'Новосибирская область',u'Кемеровская область']
#source_dates=source_dates[8:]
""" """
""" Preparing arrays """
""" """

LQs_files = []
LQs_cats = []
LQs_qs = []
LQs_dates = []
LQs_cities = []
LQs_posneg = []
LQs_adds = []
pos_neg = 0

if pos_neg == 1:
    for qj in range(len(source_qs)):
        a = source_qs[qj]
        cat = '0cat'
        for dates in source_dates:
            for posneg in [-1,1,0]:
        #            LQs_files.append('__')
        #            LQs_cats.append('_')
        #            LQs_qs.append(a)
        #            LQs_cities.append(cities)
        #            LQs_dates.append(dates)
        #            LQs_posneg.append(posneg)
                temp = ''
                if posneg == -1:
                    for jj in posneg_adds[1]:
                        temp = temp + ' -' + jj
                    LQs_adds.append('('+' | '.join(posneg_adds[posneg])+')' + temp)
                    LQs_posneg.append(-1)
                    
                if posneg == 1:
                    for jj in posneg_adds[-1]:
                        temp = temp + ' -' + jj
                    
    
                    LQs_adds.append('('+' | '.join(posneg_adds[posneg])+')' + temp)
                    LQs_posneg.append(1)
                if posneg == 0:
                    for jj in posneg_adds[-1]:
                        temp = temp + ' -' + jj
                    
                    LQs_adds.append('')
                    LQs_posneg.append(0)
                    
                LQs_files.append('__')
                LQs_cats.append('_')
                LQs_qs.append(a)
                LQs_cities.append(cities)
                LQs_dates.append(dates)
else:
    for qj in range(len(source_qs)):
        a = source_qs[qj]
        cat = '0cat'
        for dates in source_dates:
                   
                LQs_files.append('__')
                LQs_cats.append('_')
                LQs_qs.append(a)
                LQs_cities.append(cities)
                LQs_dates.append(dates)            
            


#            for adds in posneg_adds[posneg]:
#                LQs_files.append('Novosibirsk')
#                LQs_cats.append(cat)
#                LQs_qs.append(a)
#                LQs_cities.append([u'Россия'])
#                LQs_dates.append(dates)
#                LQs_posneg.append(posneg)
#                LQs_adds.append(adds)
""" """

#LQj_done={}


PreStr_done = {}
def savingPreStr(LQj,qstr,cityj):
  #todo = [LQs_files[LQj] ,LQs_cats[LQj],qstr,LQs_cities[LQj][cityj],str(LQs_posneg[LQj]),LQs_adds[LQj],LQs_dates[LQj][0],LQs_dates[LQj][1]]
  todo = [LQs_files[LQj] ,LQs_cats[LQj],qstr,LQs_cities[LQj][cityj],LQs_dates[LQj][0],LQs_dates[LQj][1]]
#  todo = [LQs_files[LQj] ,LQs_cats[LQj],qstr,LQs_cities[LQj][cityj],str(LQs_posneg[LQj]),LQs_adds[LQj],LQs_dates[LQj][0],LQs_dates[LQj][1]]

#  print(LQj)  
#  for s in todo:
#      print(s),
  return ','.join([s.replace(',','&') for s in todo])


try:
#if(1):
#  aaa
  print('Checking if file already exists..')
  if(1):
    """ Deals badly with commas in qs"""
    with open(outfname) as f:
      LS = f.readlines()[1:]
    print(len(LS))
    LS=LS[1:]
    try:
        LS = [s.decode('utf-8') for s in LS]
    except:
        pass
#    LS = [[s.replace('&',',').replace(u'&',',') for s in L.split(',')[0:4]] for L in LS[1:]]
    LS = [[s for s in L.replace('\r\n','').split(',')[0:-1]] for L in LS]
  else:
    import csv
    
    with open(outfname,'rb') as f:
      r = csv.reader(f)
      print(len(LS))
      LS= [row for row in r]
      LS = [L[0:4] for L in LS[1:]]
      
  for L in LS:
    PreStr = ','.join(L)
#    print(PreStr)
    PreStr_done[unicode(PreStr)]=1
    
    
#  pass
except:
  outfname = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__v13b__KNA_wtf_wtF__'+'pagesC_percity.csv'

  with open(outfname,'w') as f:
    f.write('original file,category,q,city,posneg,adds,date_from,date_to,Res\r\n')

#
#aaa





def downloadEnclosures(i, q,outq,wipq,doneq):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print '%s: Logining..' % i
    driver = webdriver.Firefox()
    driver.set_page_load_timeout(30)
    #driver.get("http://www.python.org")
#    driver.get('https://passport.yandex.ru/passport?mode=auth')
#
#    driver.find_element_by_xpath('//input[@id="login"]').clear()
#    driver.find_element_by_xpath('//input[@id="login"]').send_keys('iwan.vepreff')#ya.kolchanov2015')
#    
#
#    driver.find_element_by_xpath('//input[@id="passwd"]').clear()
#    driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
#
#    driver.find_element_by_xpath('//span[@class="_nb-button-content"]').click()
#
    print '%s: log in OK' % i
    qstr = ''
    LQj = -153
    while True:
#        try:
        if(1):
            print('%s: Reading next Q' % i)
            LQj,qstr,cityj = q.get()
            
            print('%s: q=%s' % (i,qstr))
            
            
            wip_queue.put((LQj,qstr,cityj,))
            
            tqstr = qstr.replace(' ','%20')
            #url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr+'%20%26%26%20'+LQs_adds[LQj],YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
            url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr,YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
            
            print(url)
            while(1):
              try:
                driver.get(url)
                break
              except:
                pass
            pause(pauseMin+rand()*pauseK)
              
            
            while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
              print('%s: "Yandex" not in title! sth wrong' % i)
              try:
                driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                pause(2)
              except:
                pause(20)
                while(1):
                  try:
                    driver.get(url)
                    break
                  except:
                    pass
                pause(pauseMin+rand()*pauseK)
            
            print('%s: url opened' % i)
    
            temp = driver.title
            
    
    
            prevtitle = ''
    
      
            city = cities[cityj]
            Ns = -153
    
            prevtitle = driver.title
            
    
    
            for j in range(15):
                
              ttitle = driver.title
              temp=ttitle.replace(u'млн','000 000')
              temp=temp.replace(u'тыс.','000')
    
              try:
                tempN = int(''.join(re.findall('[0-9]',temp)))
              except:
                if(u'ничего не найдено' in temp):
                  tempN=0
                  break
                else:
                  raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
    
    #            print(ttitle)
    #            print(tempN)
              if(prevtitle!=ttitle):
                break
              pause(pauseAfter+rand()*pauseK)
            Ns = tempN
    
            print '%s: Done' % i
            outq.put((LQj,qstr,cityj,Ns))
            doneq.put((LQj,qstr,cityj,))
            # instead of really downloading the URL,
            # we just pretend and sleep
    #        time.sleep(i + 2)
    
            q.task_done()
#        except Exception, e:
        else:
            raise e
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1);
        


#aaa
# Filling the Queue
print("Filling the Queue")

#aaa


NAlreadyDone = 0;
PreStr_done
enclosure_queue = Queue()

for LQjt in range(len(LQs_qs)):
  if(do_fromTheEnd):
    LQj = len(LQs_qs)-1-LQjt
  else:
    LQj = LQjt
  qstr = LQs_qs[LQj]
  try:
    print(qstr)
  except:
    qstr=qstr[0:-1]
    print(qstr)

  """ adding unquoted stuff """
  
  """ adding quoted stuff """
  if((' ' in qstr) or (u' ' in qstr) or ('-' in qstr) or (u'-' in qstr)):
    if((qstr[0]!='"') or (qstr[-1]!='"')):
      qstr='"'+qstr+'"'
      
  for cityj in range(len(LQs_cities[LQj])):
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    
#    raise ValueError()
    
    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
#      print('done already')
      NAlreadyDone=NAlreadyDone+1
      continue
  
    enclosure_queue.put((LQj,qstr,cityj))


#    print(qstr)
#    
#  for cityj in range(len(LQs_cities[LQj])):
#    PreStr =  savingPreStr(LQj,qstr,cityj) 
#    
#    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
##      print(PreStr+' done already')
#      NAlreadyDone=NAlreadyDone+1
#      continue
#  
#    enclosure_queue.put((LQj,qstr,cityj))

print('Queue filled.')
#aaa
#aaa

print("**************************************************")

print('NAlreadyDone = %i'%NAlreadyDone)
print('Left to do = %i'%enclosure_queue.qsize())

#aaa

import time
time.sleep(4)

output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()

def CheckWIPnDone():
  print 'CheckWIPnDone:starting'
  wips = {}
  wipN=0
  while(not wip_queue.empty()):
    LQj,qstr,cityj = wip_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    wips[PreStr]=(LQj,qstr,cityj)
    wipN=wipN+1
    
  print('CheckWIPnDone:' +' found %i wipStuff'%wipN)  
  while(not doneq_queue.empty()):
    LQj,qstr,cityj = doneq_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    try:
      wips[PreStr]=0
      wipN=wipN-1
    except:
      print('somehow done but not wip : %s'%PreStr)
#      raise ValueError('somehow wip but not done : %s'%PreStr)
  print('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)  

  for s in  wips.keys():
    t = wips[s]
    if(t==0):
      pass
    else:
      (LQj,qstr,cityj) = t
      print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(t)
      enclosure_queue.put((LQj,qstr,cityj))

  
  print 'CheckWIPnDone:done'

if(1):

  print('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
  print('Workers started.')
  
  
  import psutil
  
  def savingThread(q,q_in):
    print('savingThread: Sarted.')
    while(True):
      in_empty_n = 0
      
      ntdn = 0
      while(q.empty()):
        if(q_in.empty()):
          in_empty_n=in_empty_n+1
          print('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
        else:
          print('savingThread: nothing to do..')
          ntdn=ntdn+1
        
        print('~ %i left undone, ntd=%i'%(q_in.qsize(),ntdn))      
        
        if(ntdn>40):
          print('='*10)
          print('Restarting')
          print('='*10)
          
          print('Killing firefoxes..')
          # Restarting because sth went wrong!
          for proc in psutil.process_iter():
            if proc.name == 'firefox':
              proc.kill()
          print('done.')    
          CheckWIPnDone()    
          print('restarting workers..')
          for i in range(num_fetch_threads):
              worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
              worker.setDaemon(True)
              worker.start()
          ntdn=0    
          print('done.')
          print('='*10)
              
        
        if(in_empty_n>100):
          print('savingThread: nothing to DO! returning.')
          return
        pause(1)
      print('savingThread: something to do !)')
      with open(outfname,'a') as f:
        while(q.empty()==0):
          LQj,qstr,cityj,Ns=q.get()
          f.write((savingPreStr(LQj,qstr,cityj)+u','+str(Ns)+'\r\n').encode('utf8'))
  
  
  #print('waiting for first output..')
  #while(output_queue.empty()):
  #  pause(.1)
  #print('starting saver..')
  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,))
  worker.setDaemon(True)
  worker.start()
  
  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
  print '*** Main thread waiting'
  enclosure_queue.join()
  print '*** Main thread Done waiting'

""" """
#

