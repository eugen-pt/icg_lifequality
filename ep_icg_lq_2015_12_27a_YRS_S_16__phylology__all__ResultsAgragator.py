# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 10:49:08 2015

@author: ep
"""


import json

from ep_icg_lq_2015_12_24c__phil__words import *


""" """

outfname = '2015-12-25_13-39-05__v16__PHF__pagesC_percity.csv'


""" Local Defs """
def myjson_load(s):
    A =json.loads(s)
    return {a[0]:a[1] for a in A}



""" Data Loading """
try:
    LS_ds = [myjson_load(L.replace('\r','').replace('\n','')) for L in LS]
except:
    LS = open(outfname,'r').readlines()[1:]
    
    LS_ds = [myjson_load(L.replace('\r','').replace('\n','')) for L in LS]

""" Reorganizing """

try:
    cat1_ds
    cat2_ds
    cat12_ds
except:    
    cat1_ds = [d for d in LS_ds if ('q2' in d)and(len(d['q2'])==0)]
    cat2_ds = [d for d in LS_ds if ('q1' in d)and(len(d['q1'])==0)]

    cat12_ds = [d for d in LS_ds if((len(d['q1'])>0)and(len(d['q2'])>0))]

q1s = unique([d['q1'] for d in cat1_ds])
q1s_2 = unique([d['q1'] for d in cat12_ds])

q2s = unique([d['q2'] for d in cat2_ds])
q2s_2 = unique([d['q2'] for d in cat12_ds])

#PLF_ucats_ordered
cats2 = PLF_ucats_ordered
cats2_2= unique([d['cat2'] for d in cat2_ds])

PLF_ucats_ordered_iD = {PLF_ucats_ordered[j]:j for j in range(len(PLF_ucats_ordered))}

cat2_words = {cat2:unique([d['q2'] for d in cat2_ds if d['cat2']==cat2]) for cat2 in PLF_ucats_ordered}
words_cat2 = {}
for c in cat2_words:
    for w in cat2_words[c]:
        words_cat2[w] = c

q2s_ordered = []
for cat2 in cats2:
    q2s_ordered.extend(cat2_words[cat2])
    
q2s_ordered_iD = {q2s_ordered[j]:j for j in range(len(q2s_ordered))}

cats1 = unique([d['cat1'] for d in cat1_ds])
cat1_words = {cat1:unique([d['q1'] for d in cat1_ds if d['cat1']==cat1]) for cat1 in cats1}
words_cat1 = {}
for c in cat1_words:
    for w in cat1_words[c]:
        words_cat1[w] = c

q1s_ordered = []
for cat1 in cats1:
    q1s_ordered.extend(cat1_words[cat1])
q1s_ordered_iD = {q1s_ordered[j]:j for j in range(len(q1s_ordered))}
    
q1s_N = [0.0]*len(q1s_ordered)
for d in cat1_ds:
    q1s_N[q1s_ordered_iD[d['q1']]]=d['N']

q2s_N = [0.0]*len(q2s_ordered)
for d in cat2_ds:
    q2s_N[q2s_ordered_iD[d['q2']]]=d['N']

q12s_N = np.ones((len(q1s_ordered),len(q2s_ordered),),'float')*nan
for d in cat12_ds:
    q12s_N[q1s_ordered_iD[d['q1']],q2s_ordered_iD[d['q2']]]=d['N']

q12s_N_nogo = isnan(q12s_N)

q2s_N_go = sum(q12s_N_nogo,axis=0)==0

q2s_ordered = array(q2s_ordered)[q2s_N_go]
q2s_ordered_iD = {q2s_ordered[j]:j for j in range(len(q2s_ordered))}
q2s_N = list(array(q2s_N)[q2s_N_go])
q12s_N = q12s_N[:,q2s_N_go]
""" """

q12s_N_gt_q1_N = []
for q1j in range(len(q1s_N)):
    

aaa

import xlwt

wb = xlwt.Workbook()

s = wb.add_sheet('LQ_PHIL_all')

for q1j in range(len(q1s_ordered)):
    q1 = q1s_ordered[q1j]
    s.write(0,3+q1j,words_cat1[q1])
    s.write(1,3+q1j,q1)
    s.write(2,3+q1j,q1s_N[q1j])
    
for q2j in range(len(q2s_ordered)):
    q2 = q2s_ordered[q2j]
    s.write(3+q2j,0,words_cat2[q2])
    s.write(3+q2j,1,q2)
    s.write(3+q2j,2,q2s_N[q2j])
    
for q2j in range(len(q2s_ordered)):
    for q1j in range(len(q1s_ordered)):
        if(isnan(q12s_N[q1j,q2j])):
            s.write(3+q2j,3+q1j,q12s_N[q1j,q2j])   
        else:
            s.write(3+q2j,3+q1j,q12s_N[q1j,q2j])    
    
wb.save(outfname.replace('.csv','__res.xls'))
""" """

aaa
cat1_w1_cat2_w2_N_dd = {}
for cat1 in cats1:
    if(cat1 not in cat1_w1_cat2_w2_N_dd):
        cat1_w1_cat2_w2_N_dd[cat1] = {}
    for w1 in cat1_words[cat1]:
        if(w1 not in cat1_w1_cat2_w2_N_dd[cat1]):
            cat1_w1_cat2_w2_N_dd[cat1][w1] = {}
        for cat2 in  cats2:
            if(cat2 not in cat1_w1_cat2_w2_N_dd[cat1][w1]):
                cat1_w1_cat2_w2_N_dd[cat1][w1][cat2] = {}
            for w2 in     cat2_words[cat2]:
                cat1_w1_cat2_w2_N_dd[cat1][w1][cat2][w2] = -153

for d in cat12_ds:
    cat1_w1_cat2_w2_N_dd[d['cat1']][d['q1']][d['cat2']][d['q2']] = d['N']
""" """



""" """
#