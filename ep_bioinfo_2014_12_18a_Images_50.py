# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 16:12:34 2014

@author: ep_work
"""

from ep_bioinfo_2014_12_16a_src_reader3_r50 import *

import os

D = LQs_CM_D_CityScoreNorm
#D = LQs_CM_D_CityPopulationNorm
#D = LQs_CM_D_RegionPopulationNorm
outdir='scatter_figures_r50_CityScoreNorm'

try:
    os.mkdir(outdir)
except:
    pass


figure()

plt.tick_params(\
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',left='off',right='off',         # ticks along the top edge are off
    labelbottom='off',  labelleft='off') # labels along the bottom edge are off
NC = 20

engCats = ['Architecture','Arts and Crafts','Design','Diseases','Fine Art','Innovation','Cinematography','Criminal','Small Business','Car makers','Music','Education','General','Consumer basket','Mass media','Sport','Theatre','Farmer','Imagin Lit','Info staffing']

f=figure()
for cj in range(NC):
  for ci in range(NC): #
    print('%i/%i->%i/%i'%(cj,NC,ci,NC))
    
#    subplot(NC,NC,cj*NC+ci+1)
    plot(D[ci,:],D[cj,:],'.')
    for j in range(len(LQs_Cities)):
      text(D[ci,j],D[cj,j],LQs_engCities[j])
    xlabel(engCats[ci])
    ylabel(engCats[cj])
#    plt.tick_params(\
#        axis='both',          # changes apply to the x-axis
#        which='both',      # both major and minor ticks are affected
#        bottom='off',      # ticks along the bottom edge are off
#        top='off',left='off',right='off' ,        # ticks along the top edge are off
#        labelbottom='off',  labelleft='off') # labels along the bottom edge are off
    show()
    savefig(outdir+u'/fig_'+engCats[cj]+'_'+engCats[ci]+'.png', bbox_inches='tight')
#    break
    clf()
#    pause(.3)
#  break

""" """
#

