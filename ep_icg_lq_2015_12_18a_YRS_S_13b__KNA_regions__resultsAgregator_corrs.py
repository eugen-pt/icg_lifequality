# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 10:09:54 2015

@author: ep
"""

import numpy as np
from numpy import array,unique,sum,shape


outfname = '2015-05-28_17-00-19__v11_fursenko_1__pagesC_percity.csv'
outfname = '2015-05-28_17-00-19__v11_fursenko_1__pagesC_percity_bu.csv'
outfname = '2015-11-16_12-05-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_12-14-16__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_12-05-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_13-46-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-12-16_10-58-44__v13__KNA_wtf_wtF__pagesC_percity.csv'
outfname = '2015-12-17_12-44-37__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions'
outfname = '2015-12-18_16-29-35__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions_distinctPN'
outfname = '2015-12-21_12-38-05__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions_distinctPN_manyminuses'

""" """

def redoDate(s):
    r = s.split('.')
    if(0):
        r.reverse()
        return '-'.join(r)
    else:    
        return '%s/%s/%s'%(r[1],r[0],r[2])
#inverse dictionary - for faster search
def iD(a):
    return {a[j]:j for j in range(len(a))}
    
def hJoin(s1,s2):
    if(len(s2)>0):
        return s1+'--'+s2
    else:
        return s1
    
def sumRegion(L):
    return hJoin(L[3],L[4]    )
    
def sumQ(L):
    return hJoin(L[1],L[2]    )
    
    
def fit_exp_linear(t, y, C=0):
    y = y - C
    y = np.log(y)
    K, A_log = np.polyfit(t, y, 1)
    A = np.exp(A_log)
    return A, K

    
    
def timeLagCorrs(M,maxlag = 6,same_Npoints=0):
    if(len(shape(M))==2):
        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
        for lag in range(maxlag+1):
            if(same_Npoints):
                tM_forw = M[:,maxlag-lag:shape(M)[1]-lag]
                tM_back = M[:,maxlag:]
            else:
                tM_back = M[:,lag:]
                tM_forw = M[:,:shape(M)[1]-lag]
            
            for j in range(shape(M)[0]):
                for i in range(shape(M)[0]):
                    result[j,i,lag] = np.corrcoef(tM_back[j],tM_forw[i])[0,1]
#    elif(len(shape(M))==3):
#        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
    else:
        raise ValueError('what the hell did you give me? shape = %s'%shape(M))
    return result
       

#uses maximum amount of information it can get 
#   => different number of points for different lags 
def timeLagCorrs_allinfo(M,maxlag = 6):
    if(len(shape(M))==2):
        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
        for lag in range(maxlag+1):
            tM_back = M[:,:shape(M)[1]-lag]
            tM_forw = M[:,lag:]
            
            for j in range(shape(M)[0]):
                for i in range(shape(M)[0]):
                    result[j,i,lag] = np.corrcoef(tM_back[j],tM_forw[i])[0,1]
#    elif(len(shape(M))==3):
#        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
    else:
        raise ValueError('what the hell did you give me? shape = %s'%shape(M))
    return result            



""" 
    Loading / preprocessing
"""
 
with open(outfname,'r') as f:
    sLS = f.readlines()

sLS = sLS[0].split('\r\r')[:-1]

#LS = array([unicode(L).replace('\r\n','').split(',') for L in LS[1:]])
LS = array([L.decode('utf-8').replace('\r\n','').split(',') for L in sLS[1:]])

#LS[:,-3] = map(redoDate,LS[:,-3])
#LS[:,-2] = map(redoDate,LS[:,-2])


Regions = LS[:,3]
Qs = LS[:,2]
PosNegs = LS[:,4]
AddS = LS[:,5]
#Dates = LS[:,-3]
NPages = array(map(int,LS[:,-1]))

# filter out Kazakhsan
tix = find([(u'Атырау' not in s)and(u'Акмолинская' not in s)and(u'Актюбинская' not in s)and(u'Алматинская' not in s)and(u'Жамбылская' not in s)and(u'Карагандинская' not in s)and(u'Костанайская' not in s)and(u'Кызылординская' not in s)and(u'Мангистауская' not in s)and(u'Павлодарская' not in s) for s in Regions])

Regions=Regions[tix]
Qs = Qs[tix]
PosNegs =PosNegs[tix]
AddS = AddS[tix]
NPages = NPages[tix]

#aaa

""" """

#unique qs
uqs = unique(Qs)
uqs_iD = iD(uqs)

#unique dates- I took starting dates
udates = unique(LS[:,-3])
udates_iD = iD(udates)

uregions = unique(Regions)
uregions_iD = iD(uregions)

ups = unique(PosNegs)
ups_text = {'-1':'neg','0':'posneg','1':'pos','2':'pos_only','-2':'neg_only','3':'pos_only_mm','-3':'neg_only_mm'}
ups_iD = iD(ups)


Data_qrp = 0*np.ones((len(uqs),len(uregions),len(ups)),'float')
Data_qrp_nR = 0*np.ones((len(uqs),len(uregions),len(ups)),'float')

for Lj in range(len(NPages)):
    Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] +NPages[Lj]
    Data_qrp_nR[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp_nR[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] +1
#    Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]]+NPages[Lj]

Data_qrp = Data_qrp/Data_qrp_nR


n_uqs = []
n_ps = []
Data_n_qr = [];
npn_uqs = []
npn_ps = []
Data_npn_qr = [];
for qj in range(len(uqs)):
    for pj in range(len(ups)):
        n_uqs.append(uqs[qj]+'_'+ups_text[ups[pj]])
        n_ps.append(ups[pj])
        Data_n_qr.append(Data_qrp[qj,:,pj])
        if(int(ups[pj])!=0):
            npn_uqs.append(uqs[qj]+'_'+ups_text[ups[pj]])
            npn_ps.append(ups[pj])
            Data_npn_qr.append(Data_qrp[qj,:,pj])
          
Data_n_qr=array(Data_n_qr)
Data_npn_qr=array(Data_npn_qr)


Data_n_r = sum(Data_n_qr,axis=0)
Data_n_q = sum(Data_n_qr,axis=1)

Data_rN_n_qr = 1.0*Data_n_qr
for rj in range(shape(Data_n_qr)[1]):
  Data_rN_n_qr[:,rj] = Data_n_qr[:,rj]/Data_n_q

Data_qN_n_qr = 1.0*Data_n_qr
for qj in range(shape(Data_n_qr)[0]):
  Data_qN_n_qr[qj,:] = Data_n_qr[qj,:]/Data_n_r

Data_npn_r = sum(Data_npn_qr,axis=0)
Data_npn_q = sum(Data_npn_qr,axis=1)

Data_rN_npn_qr = 1.0*Data_npn_qr
for rj in range(shape(Data_npn_qr)[1]):
  Data_rN_npn_qr[:,rj] = Data_npn_qr[:,rj]/Data_npn_q

Data_qN_npn_qr = 1.0*Data_npn_qr
for qj in range(shape(Data_npn_qr)[0]):
  Data_qN_npn_qr[qj,:] = Data_npn_qr[qj,:]/Data_npn_r



if(0):
  plot(Data_n_qr[:,0:10].transpose());xticks(range(0,10),uregions[0:10],rotation=-20)

#aaa

import xlwt
wb = xlwt.Workbook()

""" """     

Ds = [Data_n_qr,Data_npn_qr,Data_qN_n_qr,Data_rN_n_qr,Data_qN_npn_qr,Data_rN_npn_qr]
D_uqs = [n_uqs,npn_uqs,n_uqs,n_uqs,npn_uqs,npn_uqs]
D_ps = [n_ps,npn_ps,n_ps,n_ps,npn_ps,npn_ps]
fnname_adds=['Data_n_qr','Data_npn_qr','Data_qN_n_qr','Data_rN_n_qr','Data_qN_npn_qr','Data_rN_npn_qr']

for Dj in range(len(Ds)):
    D = Ds[Dj]
    
    s = wb.add_sheet(fnname_adds[Dj]);

    s.write(0,0,'Word');
    s.write(0,1,'Pos/Neg');
    for rj in range(len(uregions)):
        s.write(0,rj+2,uregions[rj])
    rn = 0
    
    for qj in range(len(D_uqs[Dj])):
        rn=rn+1
        hq = D_uqs[Dj][qj]
        s.write(rn,0,hq)
        s.write(rn,1,D_ps[Dj][qj])
        for rj in range(len(uregions)):
            s.write(rn,rj+2,D[qj,rj])

    """ """

    s = wb.add_sheet(fnname_adds[Dj]+'_words_corrMx');
    cMx = np.corrcoef(Ds[Dj])
    
    for qj in range(len(D_uqs[Dj])):
        s.write(0,qj+1,D_uqs[Dj][qj])
        s.write(qj+1,0,D_uqs[Dj][qj])
        for qi in range(len(D_uqs[Dj])):
            s.write(qj+1,qi+1,cMx[qj,qi])

    
    """ """
    
    s = wb.add_sheet(fnname_adds[Dj]+'_Regions_corrMx');
    cMx = np.corrcoef(Ds[Dj].transpose())
    
    for rj in range(len(uregions)):
        s.write(0,rj+1,uregions[rj])
        s.write(rj+1,0,uregions[rj])
        for ri in range(len(uregions)):
          s.write(rj+1,ri+1,cMx[rj,ri])
    
wb.save('ep_icg_lq_2015_12_18a_pagescount___'+mode+'.xls')

""" """
aaa

#Data: qs/dates/pos-neg
Data_qdp = 0*np.ones((len(uqs),len(udates),len(ups)),'float')

for Lj in range(len(LS)):
    L=LS[Lj,:]
    Data_qdp[uqs_iD[Qs[Lj]],udates_iD[Dates[Lj]],ups_iD[PosNegs[Lj]]] = Data_qdp[uqs_iD[Qs[Lj]],udates_iD[Dates[Lj]],ups_iD[PosNegs[Lj]]]+int(L[-1])

Data_dp = sum(Data_qdp,axis=0)
Data_d = sum(sum(Data_qdp,axis=0),axis=1)

t = range(len(Data_d))
y = Data_d

A,K  = fit_exp_linear(t, y)

normY = A*np.exp(array(t)*K)


if(0):
    figure('time dyn')
    plot(t,Data_dp[:,0],'-or',t,Data_dp[:,1],'-og',t,Data_d,'-ob',t,normY,'--k')


Data_expN_qdp = 1.0*Data_qdp
for qj in range(shape(Data_qdp)[0]):
    for pj in range(shape(Data_qdp)[2]):
        Data_expN_qdp[qj,:,pj] = Data_qdp[qj,:,pj]/normY

Data_expN_dp = sum(Data_expN_qdp,axis=0)
Data_expN_d = sum(Data_expN_dp,axis=1)

if(0):
    figure('exp_nom dyn')
    plot(t,Data_expN_dp[:,0],'-or',t,Data_expN_dp[:,1],'-og',t,Data_expN_d,'-ob')


n_uqs = []
Data_expN_n_qd = [];
nqj=-1
for qj in range(len(uqs)):
    for pj in range(len(ups)):
        n_uqs.append(uqs[qj]+'_'+ups_text[ups[pj]])
        nqj=nqj+1
        Data_expN_n_qd.append(Data_expN_qdp[qj,:,pj])
Data_expN_n_qd=array(Data_expN_n_qd)

Ds = [Data_expN_qdp,Data_qdp]
fnname_adds = ['Data_expN_qdp','Data_qdp']

J = {'Ds':Ds,'fnname_adds':fnname_adds,'uqs':uqs,'Data_expN_qdp':Data_expN_qdp,'n_uqs':n_uqs,'Data_expN_n_qd':Data_expN_n_qd}  
import pickle
with open('ep_icg_lq_2015_12_16c_pagescount___.pickle','w') as f:
    pickle.dump(J,f)



import xlwt
wb = xlwt.Workbook()

""" """     

for Dj in range(len(Ds)):
    D = Ds[Dj]
    
    s = wb.add_sheet(fnname_adds[Dj]);

    s.write(0,0,'Word');
    s.write(0,1,'Pos/Neg');
    for datej in range(len(udates)):
        s.write(0,datej+2,udates[datej])
    rn = 0
    
    for qj in range(len(uqs)):
        for pj in range(len(ups)):
            rn=rn+1
            hq = uqs[qj]+'_'+ups_text[ups[pj]]
            s.write(rn,0,hq)
            s.write(rn,1,ups[pj])
            for datej in range(len(udates)):
                s.write(rn,datej+2,D[qj,datej,pj])
    
""" """     

maxlag = 12     
CorrMxs=timeLagCorrs(Data_expN_n_qd,maxlag,0)      

s = wb.add_sheet('D_eN_nqd_cMx_linearize')

s.write(0,0,'Lag\\q->q')
for lag in range(maxlag+1):
    s.write(lag+1,0,lag)
tn=0
rn=0
for qj in range(len(n_uqs)):
    for qi in range(len(n_uqs)):
        tn=tn+1
        if(tn>255):
            print('zeroing')
            tn=1
            rn=rn+maxlag+5
            s.write(rn,0,'Lag\\q->q')
            for lag in range(maxlag+1):
                s.write(rn+lag+1,0,lag)
        print('.'),
        s.write(rn,tn,'%s->%s'%(n_uqs[qj],n_uqs[qi]))
        for lag in range(maxlag+1):
            s.write(rn+lag+1,tn,CorrMxs[qj,qi,lag])

for lag in range(maxlag+1):
    
    s = wb.add_sheet('D_eN_nqd_cMx_lag%i'%lag)
    for qj in range(len(n_uqs)):
        s.write(qj+1,0,n_uqs[qj])
    for qj in range(len(n_uqs)):
        s.write(0,qj+1,n_uqs[qj])
    for qj in range(len(n_uqs)):
        for qi in range(len(n_uqs)):
            s.write(qj+1,qi+1,CorrMxs[qj,qi,lag])

wb.save('ep_icg_lq_2015_12_16c_pagescount___'+'Russia_timeDyn'+'.xls')





aaa
""" """

#unique dates- I took starting dates
udates = unique(LS[:,-3])
udates_iD = iD(udates)

# "summed" regions
sRegion = array([sumRegion(LS[j]) for j in range(shape(LS)[0])])
usRegion = unique(sRegion)


uqs = unique(LS[:,2])
uqs_iD = iD(uqs)

#
##no quotes - without the quoted qs
uqs_nq = unique([s for s in LS[:,2] if s[0]!='"'])
uqs_nq_iD = iD(uqs_nq)

ucats = unique(LS[:,1])
ucats_iD = iD(ucats)
#
qs_cats_D = {L[2]:L[1] for L in LS}


sqs = [sumQ(L) for L in LS]
n_uqs = unique(sqs)
n_uqs_iD = iD(n_uqs)


sqs_nq = [sumQ(L) for L in LS if L[2][0]!='"']
n_uqs_nq = unique(sqs_nq)
n_uqs_nq_iD = iD(n_uqs_nq)

sqs_cats_D = {sumQ(L):L[1] for L in LS}

qs_sqs_D = {L[2]:sumQ(L) for L in LS}
qs_sqs_D_unicode = {unicode(L[2]):unicode(sumQ(L)) for L in LS}

n_uqs_catns = array([ucats_iD[sqs_cats_D[q]] for q in n_uqs])
n_uqs_nq_catns = array([ucats_iD[sqs_cats_D[q]] for q in n_uqs_nq])

""" """

#Data: qs/regions/dates
Data_qrd = -153*np.ones((len(n_uqs),len(usRegion),len(udates)),'float')
#same without quoted qs
Data_nq_qrd = -153*np.ones((len(n_uqs_nq),len(usRegion),len(udates)),'float')

for L in LS:
#    q = L[2]
    sq = sumQ(L)
    sR = sumRegion(L)
    date = L[-3]
    
    if(sR in usRegion_iD):
        Data_qrd[n_uqs_iD[sq],usRegion_iD[sR],udates_iD[date]] = int(L[-1])
        if(sq in n_uqs_nq_iD):
            Data_nq_qrd[n_uqs_nq_iD[sq],usRegion_iD[sR],udates_iD[date]] = int(L[-1])


Data_qrd[Data_qrd<0]=0
Data_nq_qrd[Data_nq_qrd<0]=0 
 
Data_ds_qs_r = sum(sum(Data_qrd,axis=2),axis=0)
Data_nq_ds_qs_r = sum(sum(Data_nq_qrd,axis=2),axis=0)

# region*dates Normed
Data_rdN_qrd = Data_qrd*1.0
for qj in range(shape(Data_qrd)[0]):
    for dj in range(shape(Data_qrd)[2]):
        Data_rdN_qrd[qj,:,dj] = Data_qrd[qj,:,dj]/Data_ds_qs_r
        
# region*dates Normed
Data_nq_rdN_qrd = Data_nq_qrd*1.0
for qj in range(shape(Data_nq_qrd)[0]):
    for dj in range(shape(Data_nq_qrd)[2]):
        Data_nq_rdN_qrd[qj,:,dj] = Data_nq_qrd[qj,:,dj]/Data_nq_ds_qs_r
    

Data_rs_qs_d = sum(sum(Data_qrd,axis=1),axis=0)
Data_nq_rs_qs_d = sum(sum(Data_nq_qrd,axis=1),axis=0)

# region*dates Normed
Data_dN_qrd = Data_qrd*1.0
for qj in range(shape(Data_qrd)[0]):
    for rj in range(shape(Data_qrd)[1]):
        Data_dN_qrd[qj,rj,:] = Data_qrd[qj,rj,:]/Data_rs_qs_d
        
# region*dates Normed
Data_nq_dN_qrd = Data_nq_qrd*1.0
for qj in range(shape(Data_nq_qrd)[0]):
    for rj in range(shape(Data_qrd)[1]):
        Data_nq_dN_qrd[qj,rj,:] = Data_nq_qrd[qj,rj,:]/Data_nq_rs_qs_d

""" """

Data_crd = np.zeros((len(ucats),len(usRegion),len(udates)),'float')
for cj in range(len(ucats)):
    Data_crd[cj,:,:] = sum(Data_qrd[n_uqs_catns==cj,:,:],axis=0)

Data_nq_crd = np.zeros((len(ucats),len(usRegion),len(udates)),'float')
for cj in range(len(ucats)):
    Data_nq_crd[cj,:,:] = sum(Data_nq_qrd[n_uqs_nq_catns==cj,:,:],axis=0)


# region and qs summed => per date
Data_nq_rs_cs_d = sum(sum(Data_nq_crd,axis=1),axis=0)  

# normalized by summary qs per date  
Data_nq_dN_crd =   Data_nq_crd*0.0
for cj in range(shape(Data_nq_crd)[0]):
    for rj in range(shape(Data_nq_crd)[1]):
        Data_nq_dN_crd[cj,rj,:] = Data_nq_crd[cj,rj,:] / Data_nq_rs_cs_d
""" """

#Region summed - to test time lag between qs only
Data_rs_qd = sum(Data_qrd,axis=1)
Data_nq_rs_qd = sum(Data_nq_qrd,axis=1)

# qs summed - to test time lag between regions only
Data_qs_rd = sum(Data_qrd,axis=0)
Data_nq_qs_rd = sum(Data_nq_qrd,axis=0)


Data_rdN_rs_qd = sum(Data_rdN_qrd,axis=1)
Data_nq_rdN_rs_qd = sum(Data_nq_rdN_qrd,axis=1)

Data_rdN_qs_rd= sum(Data_rdN_qrd,axis=0)
Data_nq_rdN_qs_rd = sum(Data_nq_rdN_qrd,axis=0)

Data_dN_rs_qd = sum(Data_rdN_qrd,axis=1)
Data_nq_dN_rs_qd = sum(Data_nq_dN_qrd,axis=1)

Data_dN_qs_rd= sum(Data_dN_qrd,axis=0)
Data_nq_dN_qs_rd = sum(Data_nq_dN_qrd,axis=0)

""" """

Data_nq_dN_cs_rd = sum(Data_nq_dN_crd,axis=0)
Data_nq_dN_rs_cd = sum(Data_nq_dN_crd,axis=1)

#aaa

""" """
"""  Fitting exponent to total sum """
""" """

""" """


t = range(shape(Data_qrd)[2])

y = sum(Data_rs_qd,axis=0)

A,K  = fit_exp_linear(t, y)

normY = A*np.exp(array(t)*K)

if(0):
    colors = ['r','b','k','g','orange','navy']
    plt.figure(1)
    for j in range(shape(Data_rs_qd)[0]):
        plot(t,Data_rs_qd[j,:])
    
    plot(t,y,'--')
    plot(t,normY)    



""" """

Data_expN_qrd = 1.0*Data_qrd;
for j in range(shape(Data_qrd)[1]):
    for i in range(shape(Data_expN_qrd)[0]):
        Data_expN_qrd[i,j,:] = Data_expN_qrd[i,j,:]/normY

""" """

import re
udates = array([re.sub('([0-9]+)-([0-9]+)-([0-9]+)',r'\2/\3/\1',d) for d in udates])

n_uqs = array([s.replace('00--','') for s in n_uqs])
if(1):
    
#    Ds = [Data_nq_qrd,Data_nq_dN_qrd]
#    fnname_adds = ['Data_nq_qrd','Data_nq_dN_qrd']
#    
#    for Dj in range(len(Ds)):
#        wb = xlwt.Workbook()
#        D = Ds[Dj]
#        
#        for rj in range(len(usRegion)):
#            s = wb.add_sheet(usRegion[rj].replace('--','-').replace('область','обл'))
#            s.write(0,0,'')
#            for qj in range(len(n_uqs_nq)):
#                s.write(qj+1,0,n_uqs_nq[qj])
#            for dj in range(len(udates)) :
#                s.write(0,dj+1,udates[dj])
#                for qj in range(len(n_uqs_nq)):
#                    s.write(qj+1,dj+1,D[qj,rj,dj])
#        wb.save('ep_icg_lq_2015_11_16c_pagescount___'+fnname_adds[Dj]+'.xls')

    Ds = [Data_qrd,Data_dN_qrd,Data_expN_qrd]
    fnname_adds = ['Data_qrd','Data_dN_qrd','Data_expN_qrd']
    
    J = {'Ds':Ds,'fnname_adds':fnname_adds,'n_uqs':n_uqs,'Data_expN_qrd':Data_expN_qrd}  
    import pickle
    
    with open('ep_icg_lq_2015_11_16c_pagescount___.pickle','w') as f:
        pickle.dump(J,f)
    
    import xlwt
    for Dj in range(len(Ds)):
        wb = xlwt.Workbook()
        D = Ds[Dj]
        
        for rj in range(len(usRegion)):
            s = wb.add_sheet(usRegion[rj].replace('--','-').replace('область','обл').replace('Новосибирск','N'))
            s.write(0,0,'')
            for qj in range(len(n_uqs)):
                s.write(qj+1,0,n_uqs[qj])
            for dj in range(len(udates)) :
                s.write(0,dj+1,udates[dj])
                for qj in range(len(n_uqs)):
                    s.write(qj+1,dj+1,D[qj,rj,dj])
            

            maxlag = 12     
            CorrMxs=timeLagCorrs(D[:,0,:],maxlag,0)      

            s = wb.add_sheet(usRegion[rj].replace('--','-').replace('область','обл').replace('Новосибирск','N')+'_corrMx_linearize')
            
            s.write(0,0,'Lag\\q->q')
            for lag in range(maxlag+1):
                s.write(lag+1,0,lag)
            tn=0
            for qj in range(len(n_uqs)):
                for qi in range(len(n_uqs)):
                    tn=tn+1
                    s.write(0,tn,'%s->%s'%(n_uqs[qj],n_uqs[qi]))
                    for lag in range(maxlag+1):
                        s.write(lag+1,tn,CorrMxs[qj,qi,lag])

            for lag in range(maxlag+1):
                
                s = wb.add_sheet(usRegion[rj].replace('--','-').replace('область','обл').replace('Новосибирск','N')+'_corrMx_lag%i'%lag)
                for qj in range(len(n_uqs)):
                    s.write(qj+1,0,n_uqs[qj])
                for qj in range(len(n_uqs)):
                    s.write(0,qj+1,n_uqs[qj])
                for qj in range(len(n_uqs)):
                    for qi in range(len(n_uqs)):
                        s.write(qj+1,qi+1,CorrMxs[qj,qi,lag])
            

        wb.save('ep_icg_lq_2015_11_16c_pagescount___'+fnname_adds[Dj]+'.xls')
    

""" """
#