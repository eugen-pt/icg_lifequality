# -*- coding: utf-8 -*-
"""
Created on Thu Dec 18 15:02:03 2014

@author: ep
"""

from ep_bioinfo_2014_12_16a_src_reader3_r50 import *

D = LQs_CM_D

uc_Eng = ['Architecture','Arts and Crafts','Design','Diseases','Fine Art','Innovation','Cinematography','Criminal','Small Business','Car makers','Music','Education','General','Consumer basket','Mass media','Sport','Theatre','Farmer','Imagin Lit','Info staffing']
Pops = LQs_CityPopulation

figure()
for j in range(len(uc_Eng)):
    subplot(5,4,j+1)
    plot(log(Pops),log(D[j,:]),'.')
    
if(0):    
    D = LQs_D
    
    
    Scores = sum(D,axis=0)
    
    figure()
    plot(log(Pops),log(Scores),'.')
