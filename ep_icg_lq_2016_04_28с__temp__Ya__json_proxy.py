# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 14:08:42 2016

@author: ep
"""

"""      Imports      """

import json
import requests
from   selenium import webdriver

def makeDict(proxy):
    if(proxy['type']=='SOCKS4'):
        proxiesDict = {
            'http' : "socks4://%s:%s"%(proxy['ip'],proxy['port']),
            'https' : "socks4://%s:%s"%(proxy['ip'],proxy['port'])
        }
    elif(proxy['type']=='HTTP'):
        proxiesDict = {
            'http' : "http://%s:%s"%(proxy['ip'],proxy['port']),
            'https' : "http://%s:%s"%(proxy['ip'],proxy['port'])
        }
    elif(proxy['type']=='HTTPS'):
        proxiesDict = {
            'http' : "https://%s:%s"%(proxy['ip'],proxy['port']),
            'https' : "https://%s:%s"%(proxy['ip'],proxy['port'])
        }
    else:
        return {}
    return proxiesDict


"""      Consts       """

try:
    Proxies
except:
    try:
        Proxies = json.load(open('temp_proxies.json','r'))
    except:
        Proxies = []

        driver = webdriver.Firefox()
        driver.get('http://hideme.ru/proxy-list/?country=RU&maxtime=500#json')
    #    if(1):
        for d in driver.find_element_by_class_name('proxy__t').find_elements_by_tag_name('tr'):
            tds = d.find_elements_by_tag_name('td')
            if(len(tds)>0):
                Proxies.append({'ip':tds[0].text,'port':tds[1].text,'type':tds[4].text})

        driver.close()
        json.dump(Proxies,open('temp_proxies.json','w'))


for P in Proxies:
    if(P['type']=='HTTPS'):
        continue
    print('(%s) %s:%s'%(P['type'],P['ip'],P['port']))
    try:
        R=requests.get('http://icg.8ep.ru/utasks1/ip',proxies=makeDict(P), timeout=1)
    except requests.exceptions.Timeout,e:
        print('  Timeout.')
        continue
    except requests.exceptions.ProxyError, e:
        print('  No proxy')
        continue
    if(R.status_code!=200):
        continue
    print('  '+R.text)
    print(R.status_code)

aaa
url = 'http://yandex.ru/search?text=neverever&lang=ru&ajax={}'

R=requests.get(url, proxies=proxies, timeout=5)

""" Local Definitions """



"""   Data Loading    """



"""    Processing     """


"""      Saving       """



""" """
#
