# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 10:09:54 2015

@author: ep
"""

import numpy as np
from numpy import array,unique,sum,shape

from ep_icg_lq_2015_12_23c__NY__qs import *
from ep_lq_2015_12_28a_RuMap import *
#NY_words_cats_d = {k.decode('utf-8'):NY_words_cats_d[k].decode('utf-8') for k in NY_words_cats_d}

root_path = os.getcwd()
import os


corrTh = 0.2

outfname = '2015-05-28_17-00-19__v11_fursenko_1__pagesC_percity.csv'
outfname = '2015-05-28_17-00-19__v11_fursenko_1__pagesC_percity_bu.csv'
outfname = '2015-11-16_12-05-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_12-14-16__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_12-05-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-11-16_13-46-00__v12__KNA_wtf__pagesC_percity.csv'
outfname = '2015-12-16_10-58-44__v13__KNA_wtf_wtF__pagesC_percity.csv'
outfname = '2015-12-17_12-44-37__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions'
outfname = '2015-12-18_16-29-35__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions_distinctPN'
outfname = '2015-12-21_12-38-05__v13b__KNA_wtf_wtF__pagesC_percity.csv'; mode='RuRegions_distinctPN_manyminuses'
outfname = '2015-12-23_15-57-19__v14r__NY__pagesC_percity.csv'; mode = 'NY_NY';
""" """

def redoDate(s):
    r = s.split('.')
    if(0):
        r.reverse()
        return '-'.join(r)
    else:    
        return '%s/%s/%s'%(r[1],r[0],r[2])
#inverse dictionary - for faster search
def iD(a):
    return {a[j]:j for j in range(len(a))}
    
def hJoin(s1,s2):
    if(len(s2)>0):
        return s1+'--'+s2
    else:
        return s1
    
def sumRegion(L):
    return hJoin(L[3],L[4]    )
    
def sumQ(L):
    return hJoin(L[1],L[2]    )
    
    
def fit_exp_linear(t, y, C=0):
    y = y - C
    y = np.log(y)
    K, A_log = np.polyfit(t, y, 1)
    A = np.exp(A_log)
    return A, K

    
    
def timeLagCorrs(M,maxlag = 6,same_Npoints=0):
    if(len(shape(M))==2):
        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
        for lag in range(maxlag+1):
            if(same_Npoints):
                tM_forw = M[:,maxlag-lag:shape(M)[1]-lag]
                tM_back = M[:,maxlag:]
            else:
                tM_back = M[:,lag:]
                tM_forw = M[:,:shape(M)[1]-lag]
            
            for j in range(shape(M)[0]):
                for i in range(shape(M)[0]):
                    result[j,i,lag] = np.corrcoef(tM_back[j],tM_forw[i])[0,1]
#    elif(len(shape(M))==3):
#        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
    else:
        raise ValueError('what the hell did you give me? shape = %s'%shape(M))
    return result
       

#uses maximum amount of information it can get 
#   => different number of points for different lags 
def timeLagCorrs_allinfo(M,maxlag = 6):
    if(len(shape(M))==2):
        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
        for lag in range(maxlag+1):
            tM_back = M[:,:shape(M)[1]-lag]
            tM_forw = M[:,lag:]
            
            for j in range(shape(M)[0]):
                for i in range(shape(M)[0]):
                    result[j,i,lag] = np.corrcoef(tM_back[j],tM_forw[i])[0,1]
#    elif(len(shape(M))==3):
#        result = np.zeros((shape(M)[0],shape(M)[0],maxlag+1))
    else:
        raise ValueError('what the hell did you give me? shape = %s'%shape(M))
    return result            



""" 
    Loading / preprocessing
"""
 
with open(outfname,'r') as f:
    sLS = f.readlines()
#aaa
#sLS = sLS[0].split('\r\r')[:-1]

#LS = array([unicode(L).replace('\r\n','').split(',') for L in LS[1:]])
LS = array([L.decode('utf-8').replace('\r\n','').split(',') for L in sLS[1:]])

#aaa
#LS[:,-3] = map(redoDate,LS[:,-3])
#LS[:,-2] = map(redoDate,LS[:,-2])


Regions = LS[:,3]
Qs = LS[:,2]
PosNegs = array([153]*len(Regions))
AddS = array(['']*len(Regions))
#Dates = LS[:,-3]
NPages = array(map(int,LS[:,-1]))

# filter out Kazakhsan
tix = find([(u'Атырау' not in s)and(u'Акмолинская' not in s)and(u'Актюбинская' not in s)and(u'Алматинская' not in s)and(u'Жамбылская' not in s)and(u'Карагандинская' not in s)and(u'Костанайская' not in s)and(u'Кызылординская' not in s)and(u'Мангистауская' not in s)and(u'Павлодарская' not in s) for s in Regions])

Regions=Regions[tix]
Qs = Qs[tix]
PosNegs =PosNegs[tix]
AddS = AddS[tix]
NPages = NPages[tix]

tix = find([u'Нима' not in s for s in Qs])

Regions=Regions[tix]
Qs = Qs[tix]
PosNegs =PosNegs[tix]
AddS = AddS[tix]
NPages = NPages[tix]

Cats = [NY_words_cats_d[s] for s in Qs]

#aaa

""" """


#unique qs
uqs = unique(Qs)

""" resorting - per category """
NY_ucats_iD = iD([s.decode('utf-8') for s in NY_ucats])
temp_uqs_iD = iD(uqs)
tnums = [NY_ucats_iD[NY_words_cats_d[q]]*1000 + temp_uqs_iD[q]  for q in uqs]
tix = argsort(tnums)
uqs = uqs[tix]

uqs_iD = iD(uqs)


#unique dates- I took starting dates
udates = unique(LS[:,-3])
udates_iD = iD(udates)

uregions = unique(Regions)
uregions_iD = iD(uregions)

ups = unique(PosNegs)
ups_text = {'-1':'neg','0':'posneg','1':'pos','2':'pos_only','-2':'neg_only','3':'pos_only_mm','-3':'neg_only_mm',153:''}
ups_iD = iD(ups)


Data_qrp = 0*np.ones((len(uqs),len(uregions),len(ups)),'float')
Data_qrp_nR = 0*np.ones((len(uqs),len(uregions),len(ups)),'float')

for Lj in range(len(NPages)):
    Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] +NPages[Lj]
    Data_qrp_nR[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp_nR[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] +1
#    Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]] = Data_qrp[uqs_iD[Qs[Lj]],uregions_iD[Regions[Lj]],ups_iD[PosNegs[Lj]]]+NPages[Lj]

Data_qrp = Data_qrp/Data_qrp_nR


n_uqs = []
n_ps = []
Data_n_qr = [];
npn_uqs = []
npn_ps = []
Data_npn_qr = [];
for qj in range(len(uqs)):
    for pj in range(len(ups)):
        n_uqs.append(uqs[qj]+''+ups_text[ups[pj]])
        n_ps.append(ups[pj])
        Data_n_qr.append(Data_qrp[qj,:,pj])
        if(int(ups[pj])!=0):
            npn_uqs.append(uqs[qj]+''+ups_text[ups[pj]])
            npn_ps.append(ups[pj])
            Data_npn_qr.append(Data_qrp[qj,:,pj])
          
Data_n_qr=array(Data_n_qr)
Data_npn_qr=array(Data_npn_qr)


Data_n_r = sum(Data_n_qr,axis=0)
Data_n_q = sum(Data_n_qr,axis=1)

Data_rN_n_qr = 1.0*Data_n_qr
for rj in range(shape(Data_n_qr)[1]):
  Data_rN_n_qr[:,rj] = Data_n_qr[:,rj]/Data_n_q

Data_qN_n_qr = 1.0*Data_n_qr
for qj in range(shape(Data_n_qr)[0]):
  Data_qN_n_qr[qj,:] = Data_n_qr[qj,:]/Data_n_r

Data_npn_r = sum(Data_npn_qr,axis=0)
Data_npn_q = sum(Data_npn_qr,axis=1)

Data_rN_npn_qr = 1.0*Data_npn_qr
for rj in range(shape(Data_npn_qr)[1]):
  Data_rN_npn_qr[:,rj] = Data_npn_qr[:,rj]/Data_npn_q

Data_qN_npn_qr = 1.0*Data_npn_qr
for qj in range(shape(Data_npn_qr)[0]):
  Data_qN_npn_qr[qj,:] = Data_npn_qr[qj,:]/Data_npn_r



if(0):
  plot(Data_n_qr[:,0:10].transpose());xticks(range(0,10),uregions[0:10],rotation=-20)

#aaa

""" """
""" """
""" """
""" """
""" """

n_uqs_iD = iD(n_uqs)

def getData(q):
    if(type(q)!=type([])):
        q=[q]
    tix = array([n_uqs_iD[s] for s in q])    
    return Data_qN_n_qr[tix,:]

def ep_mkdir(s):
    try:
        os.mkdir(s)
    except:
        pass
    

NY_names = [NY_words[j] for j in range(len(NY_words)) if NY_cats[j]==NY_ucats[0]]

NY_catn_words = {i:[NY_words[j] for j in range(len(NY_words)) if NY_cats[j]==NY_ucats[i]] for i in range(len(NY_ucats))}

import os
from os.path import join
root_output_path = join(root_path,u'2015_NY',u'2015_NY_htmloutput')
ep_mkdir(root_output_path)
    
BAD_NY_ucats = [NY_ucats[7],NY_ucats[9],NY_ucats[13]]

remake = 0
#remake = 1
if(1):
    """ Images (maps) """
    imgs_output_path = join(root_output_path,u'imgs')
    ep_mkdir(imgs_output_path)
    
    print('making images..')
    for ci in range(len(NY_ucats)):
        name_cat = NY_ucats[ci]
        if(name_cat in BAD_NY_ucats):
            continue
        for name in NY_catn_words[ci]:
            img_path= join(imgs_output_path,name+u'.png')
            if(os.path.isfile(img_path) and remake==0):
                continue
            name_data = getData(name)[0]
            
            RuMap_plot_region_values(uregions,name_data,figname=name)
            plt.savefig(img_path)
            plt.close(name)
            
    #plt.close('all')
    print(' Done.')
#aaa

""" for every person -- """
hroot_output_path = root_output_path
ep_mkdir(hroot_output_path)


for ci in range(len(NY_ucats)):
    name_cat = NY_ucats[ci]
    if(name_cat in BAD_NY_ucats):
        continue
    for name in NY_catn_words[ci]:
        name_data = getData(name)
        
        cur_fname = 'c%i_%s_%s.html'%(ci+1,name_cat,name)
        cur_path = join(hroot_output_path,cur_fname)
        htmlf = open(cur_path,'w')
        htmlf.write('<!DOCTYPE html>\n<html><head><meta charset="utf-8" />')
        htmlf.write("""
        <style>
        .city {
            float: left;
            margin: 5px;
            padding: 15px;
            width: %ipx;
            height: %ipx;
            border: 0px solid black;
        } 
        </style>
        """)
        htmlf.write('<title>%s</title> </head><body>'%name) 
        htmlf.write('<div>')
        htmlf.write('<table><tr><td style="vertical-align: top>')
        htmlf.write('<h2>%s</h2>'%(name))
        htmlf.write('</td><td style="align:right">')
        htmlf.write('<img src="%s" style="width:400px">'%(u'imgs/'+name+u'.png'))
        htmlf.write('</td></tr></table>')
        htmlf.write('</div>')
        for cj in range(len(NY_ucats)):
            hcat = NY_ucats[cj]
            if(hcat in BAD_NY_ucats):
                continue
            if(ci==0)and(cj==0):
                continue
            hcat_words = [w for w in NY_catn_words[cj] if w!=name]
            hcat_words_corrs = np.corrcoef(name_data,getData(hcat_words))[0,1:]
            
            tix_pos = find(hcat_words_corrs>corrTh)
            twords_pos = array(hcat_words)[tix_pos]
            tcorrs_pos = hcat_words_corrs[tix_pos]
            tix_pos = list(argsort(tcorrs_pos))
            tix_pos.reverse()

            tix_neg = find(hcat_words_corrs<-corrTh)
            twords_neg = array(hcat_words)[tix_neg]
            tcorrs_neg = hcat_words_corrs[tix_neg]
            tix_neg = list(argsort(tcorrs_neg))
            
            if(len(tix_pos)+len(tix_neg) ==0):
                htmlf.write('<b>нет значимых корреляций =( </b>')
                continue

            htmlf.write('<div class="city" style="width:%ipx;height:%ipx;">'%((max([len(tw)*8.4+30+5+4 for tw in twords_pos]+[30])+max([len(tw)*8.4+35+5+4 for tw in twords_neg]+[30])),max(len(twords_neg),len(twords_pos))*23+3+22+19+21.3*2))
            """ Navigation table """
#            htmlf.write('<h4><a name="%s">%s</a></h4>'%(hcat,hcat))
            htmlf.write('<h4>%s</h4>'%(hcat))

#            htmlf.write('<hr>')
#            htmlf.write('<br />')
#            htmlf.write('<br />')
#            htmlf.write('<br />')
#            for tempcat in NY_ucats:
#                if(tempcat==hcat):
#                    htmlf.write('<h3><a name="%s">%s</a>'%(hcat,''))
#                htmlf.write('<a href="%s#%s">%s</a></h3><br/>'%(cur_fname,tempcat,tempcat))
#                if(tempcat==hcat):
#                    htmlf.write('</h3>')
#            htmlf.write('<hr>')
#            htmlf.write('<br />')

            """ Correlation tables"""

            htmlf.write('<table style="border: 1px solid black; border-collapse: collapse;">')
            htmlf.write('<tr style="border: 1px solid black">')
            htmlf.write('<th style="border: 1px solid black">')
#            htmlf.write('<h3>Положительные корреляции:</h3><br/>')
            htmlf.write('<b>&gt0&nbsp:</b>')
            htmlf.write('</th>')
            htmlf.write('<th style="border: 1px solid black">')
#            htmlf.write('<h3>Отрицательные корреляции:</h3><br/>')
            htmlf.write('<b>&lt0&nbsp:</b>')
            htmlf.write('</th>')
            htmlf.write('</tr>')
            htmlf.write('<tr style="border: 1px solid black">')
            """ pos """
            htmlf.write('<td style="vertical-align: top; border: 1px solid black">')
            
            if(len(tix_pos)>0):
                htmlf.write('<table>')
#                htmlf.write('<table><tr><th>Term</th><th>corr</th></tr>')
                for i in tix_pos:
                    htmlf.write('<tr>')
                    htmlf.write('<td><a href="%s">%s</a></td><td>%.2f</td></tr>'%('c%i_%s_%s.html'%(cj+1,hcat,twords_pos[i]),twords_pos[i],tcorrs_pos[i]))
                htmlf.write('</table>')
            else:
                htmlf.write('--')
            htmlf.write('</td>')
            
            """ neg """
            htmlf.write('<td style="vertical-align: top; border: 1px solid black">')
            if(len(tix_neg)>0):
                htmlf.write('<table>')
#                htmlf.write('<table><tr><th>Слово</th><th>Корреляция</th></tr>')
                #tix.reverse()
                
                for i in tix_neg:
                    htmlf.write('<tr>')
                    htmlf.write('<td><a href="%s">%s</a></td><td>%.2f</td></tr>'%('c%i_%s_%s.html'%(cj+1,hcat,twords_neg[i]),twords_neg[i],tcorrs_neg[i]))
                htmlf.write('</table>')
            else:
                htmlf.write('--')
#                htmlf.write('Нет отрицательных корреляций')
            htmlf.write('</td>')
            htmlf.write('</tr>')
            htmlf.write('</table>')
    
            htmlf.write('</div>')
            
        htmlf.write('</body></html>') 
        htmlf.close()    

#aaa

""" """
""" """
""" """
""" """
""" """

import xlwt
wb = xlwt.Workbook()

""" """     

Ds = [Data_n_qr,Data_qN_n_qr,Data_rN_n_qr]
D_uqs = [n_uqs,n_uqs,n_uqs]
D_ps = [n_ps,n_ps,n_ps]
fnname_adds=['Data_n_qr','Data_qN_n_qr','Data_rN_n_qr']

for Dj in range(len(Ds)):
    D = Ds[Dj]
    
    s = wb.add_sheet(fnname_adds[Dj]);

    s.write(0,0,'Cat');
    s.write(0,1,'Word');
    for rj in range(len(uregions)):
        s.write(0,rj+2,uregions[rj])
    rn = 0
    
    for qj in range(len(D_uqs[Dj])):
        rn=rn+1
        hq = D_uqs[Dj][qj]
        s.write(rn,1,hq)
        s.write(rn,0,NY_words_cats_d[hq])
        for rj in range(len(uregions)):
            s.write(rn,rj+2,D[qj,rj])

    """ """

    s = wb.add_sheet(fnname_adds[Dj]+'_words_corrMx');
    cMx = np.corrcoef(Ds[Dj])
    
    for qj in range(len(D_uqs[Dj])):
        s.write(qj+2,1,D_uqs[Dj][qj])
        s.write(qj+2,0,NY_words_cats_d[D_uqs[Dj][qj]])
        if(qj+2<255):
            s.write(0,qj+2,NY_words_cats_d[D_uqs[Dj][qj]])
            s.write(1,qj+2,D_uqs[Dj][qj])
        for qi in range(len(D_uqs[Dj])):
            if(qi+2 < 255):
                s.write(qj+2,qi+2,cMx[qj,qi])

    
    """ """
    
    s = wb.add_sheet(fnname_adds[Dj]+'_Regions_corrMx');
    cMx = np.corrcoef(Ds[Dj].transpose())
    
    for rj in range(len(uregions)):
        s.write(0,rj+1,uregions[rj])
        s.write(rj+1,0,uregions[rj])
        for ri in range(len(uregions)):
          s.write(rj+1,ri+1,cMx[rj,ri])
    
wb.save('ep_icg_lq_2015_12_18a_pagescount___'+mode+'.xls')

""" """
#aaall_finished
""" """
""" """
""" """
""" """
""" """

""" """
#