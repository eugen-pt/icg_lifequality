# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work

noemo = part of 2016-04 work that was launched on small comp  in 1320 via remote access
"""

# Set up some global variables

# 0 = from start , 1= from end, 2 = randomized--undone
do_fromTheEnd = 1

num_fetch_threads = 1

[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered] = [.05,.05,.05,.3]
#[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered] = [2,1,1,1]

pause_per_each_worker_launch = 10 #pause to enter captcha in each new window

pause_when_all_workers_fail = 60*15 #3650 # hour+ pause when Yandex gives you a ban

verbose = 1 #~amount of output
sTh_report_each_s = 40 # only if verbose==1

wait_for_first_output = 0 #wait before starting saving thread

stop_after_queue_filled = 0


#from ep_bioinfo_2014_12_10f_src_reader1 import *
#from ep_bioinfo_2015_01_29d_src_reader3_3 import *

from ep_icg_lq_2015_01_29a_gradoteka_citieslist import *

#import colorama
import datetime
import os
import psutil
import re
import requests
import time

from selenium import webdriver

from threading import Thread
from time import sleep as pause
from Queue import Queue


#aaa
""" """
""" Local definitions """
""" """

#def cls():
#    os.system('cls' if os.name=='nt' else 'clear')
def rand():
    return ord(os.urandom(1))/255.0
def getDates(start='01.01.2000',end='31.03.2016',each_m=3):
    if not (end):
        end = datetime.datetime.now().strftime('%d.%m.%Y')
    [start_date,end_date] = [datetime.date(tx[2],tx[1],tx[0]) for tx in  [map(int,re.findall('([0-9]+)',a)) for a in [start,end]] ]

    R_dates = []
    while(start_date<end_date):
        hstart_date = start_date
        start_date = datetime.date(hstart_date.year+1*(hstart_date.month+each_m>12),hstart_date.month+each_m if (hstart_date.month+each_m<=12) else hstart_date.month+each_m-12,1);
        hend_date = start_date-datetime.timedelta(1)
        R_dates.append((hstart_date.strftime('%d.%m.%Y'),hend_date.strftime('%d.%m.%Y')))
    return R_dates

#aaa
""" """
""" Setting starting working parameters """
""" """


cities = YaRuRegions


#TODO
#outfname = '2015-12-16_10-58-44__v13__KNA_wtf_wtF__pagesC_percity.csv'
#outfname = '2016-04-07_16-36-32__v13b__KNA_wtf_wtF__pagesC_percity.csv'
#outfname = '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity.csv'
outfname = '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep_Rt.csv' #3

infnames = [ \
            '2016-04-08_12-59-00__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            '2016-04-08_12-56-48__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            '2016-04-08_13-02-26__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            ]

infnames = []
""" """
"""             Preparing arrays                """
""" """

"""    Q words     
"""

# Термины
source_qs=[]	
#source_qs= source_qs+[u'Поликлиника',u'Больница',u'"Здоровый образ жизни"',u'Спорт',u'Курение',u'Алкоголь']
#source_qs= source_qs+[u'Ожирение ',u'Стресс',u'Канцерогены',u'"Загрязнение воды"']
#source_qs= source_qs+[u'Жара',u'Влажность',u'Морозы',u'Алкоголь',u'"Загрязнение воздуха"']
#source_qs= source_qs+[u'Профилактика',u'Прививка',u'Диспансеризация',u'Гигиена',u'Дезинфекция ',u'Эпидемия']


#1
#source_qs= source_qs+[u'"Гепатит E"',u'Бериллиоз',u'"Нервная булимия"',u'Отморожение ',u'"Натуральная оспа"',u'"Нервная анорексия"',u'"Почечнокаменная болезнь"',u'"Гепатит D"',u'"Рак гортани"',u'"Геморрагическая лихорадка Эбола"',u'"Атипичная пневмония"',u'"Дискинезия желчевыводящих путей"',u'"Язва двенадцатиперстной кишки"',u'"Гепатит C"',u'Описторхоз',u'Дифтерия ',u'"Рак мочевого пузыря"',u'"Рак щитовидной железы"',u'"Гепатит B"',u'"Ветряная оспа"',u'"Рак поджелудочной железы"',u'Ревматизм']
#2
#source_qs= source_qs+[u'"Рак лёгкого"',u'"Болезнь Паркинсона"',u'"Сахарный диабет 2-го типа"',u'"Рассеянный склероз"',u'Дизентерия',u'Мастопатия',u'"Болезнь Альцгеймера"',u'Эндометриоз',u'Дальнозоркость',u'"Рак молочной железы"',u'"Птичий грипп"',u'"Сотрясение мозга"',u'"Ишемическая болезнь сердца"',u'Подагра',u'"Язва желудка"',u'Аппендицит',u'"Черепно-мозговая травма"',u'Аутизм',u'Сколиоз',u'"Цирроз печени"',u'Сифилис',u'Холецистит',u'"Инфаркт миокарда"',u'Стенокардия',u'"Бронхиальная астма"']
#4
#source_qs= source_qs+[u'"Сельское население"',u'Профилактика',u'Прививка',u'Диспансеризация',u'Гигиена',u'Дезинфекция ',u'Эпидемия',u'Поликлиника',u'Больница',u'"Здоровый образ жизни"',u'Спорт',u'Курение',u'Алкоголь',u'Ожирение ',u'Стресс',u'Канцерогены',u'"Загрязнение воды"',u'"Загрязнение воздуха"',u'Жара',u'Влажность',u'Морозы',u'"Клещевой энцефалит"',u'"Свиной грипп"',u'"Рак кожи"',u'"Миома матки"',u'"Рак желудка"',u'"Синдром Дауна"',u'"Гепатит А"',u'Близорукость',u'Катаракта',u'"Варикозное расширение вен"']

#3
source_qs=source_qs+[u'Радикулит',u'Кандидоз',u'Дерматит',u'ВИЧ-инфекция',u'Бронхит',u'Кариес',u'Педикулёз',u'Пневмония',u'Гастрит',u'Киста',u'Целлюлит',u'Вывих',u'Эпилепсия',u'Псориаз',u'Бесплодие',u'Ожог',u'Ожирение',u'Геморрой',u'Депрессия',u'Диарея',u'Инсульт',u'Остеохондроз',u'Перелом',u'Простатит',u'Токсикомания',u'Туберкулёз',u'Аллергия',u'Грипп',u'Опьянение',u'Ушиб',u'Герпес',u'Дети',u'Матери',u'Пенсионеры',u'Безработные',u'Бездомные',u'Наркоманы',u'"Городское население"',u'"Жёлчнокаменная болезнь"',u'"Сахарный диабет 1-го типа"']
source_qs = source_qs[0:1]
"""    Dates 
"""

#source_dates = getDates(start='01.11.2013',end='31.03.2016',each_m=1)
source_dates = getDates(start='01.01.2000',end='31.03.2016',each_m=3)

#aaa
"""    Окраска
"""
ini_adds = [u'Инициатива',u'Назначить',u'Намереваться',u'Планировать',u'Попробовать',u'Попытаться',u'Решить']
must_adds = [u'Должно',u'Необходимо',u'Обязанность']
no_adds = [u'Несогласие',u'Отказ',u'Отказаться',u'Отклонить',u'Игнорировать',u'"Не соблюдать"',u'Пренебрегать']
pos_adds = [u'хорошо',u'замечательно',u'отлично',u'чудесно',u'Здорово',u'Правильно',u'Одобряю']
neg_adds = [u'плохо',u'ужасно',u'кошмар',u'невыносимо',u'Неудачно',u'Неправильно',u'Возмутительно']

posneg_adds = {0:ini_adds,3:must_adds,4:no_adds,5:pos_adds,6:neg_adds}

	
LQs_files = []
LQs_cats = []
LQs_qs = []
LQs_dates = []
LQs_cities = []
LQs_posneg = []
LQs_adds = []
pos_neg = 0

if pos_neg == 1:
    for qj in range(len(source_qs)):
        a = source_qs[qj]
        cat = '0cat'
        for dates in source_dates:
            for posneg in [-1,1,0]:
        #            LQs_files.append('__')
        #            LQs_cats.append('_')
        #            LQs_qs.append(a)
        #            LQs_cities.append(cities)
        #            LQs_dates.append(dates)
        #            LQs_posneg.append(posneg)
                temp = ''
                if posneg == -1:
                    for jj in posneg_adds[1]:
                        temp = temp + ' -' + jj
                    LQs_adds.append('('+' | '.join(posneg_adds[posneg])+')' + temp)
                    LQs_posneg.append(-1)
                    
                if posneg == 1:
                    for jj in posneg_adds[-1]:
                        temp = temp + ' -' + jj
                    
    
                    LQs_adds.append('('+' | '.join(posneg_adds[posneg])+')' + temp)
                    LQs_posneg.append(1)
                if posneg == 0:
                    for jj in posneg_adds[-1]:
                        temp = temp + ' -' + jj
                    
                    LQs_adds.append('')
                    LQs_posneg.append(0)
                    
                LQs_files.append('__')
                LQs_cats.append('_')
                LQs_qs.append(a)
                LQs_cities.append(cities)
                LQs_dates.append(dates)
else:
    for qj in range(len(source_qs)):
        a = source_qs[qj]
        cat = '0cat'
        for dates in source_dates:
                   
                LQs_files.append('__')
                LQs_cats.append('_')
                LQs_qs.append(a)
                LQs_cities.append(cities)
                LQs_dates.append(dates)            
            


#            for adds in posneg_adds[posneg]:
#                LQs_files.append('Novosibirsk')
#                LQs_cats.append(cat)
#                LQs_qs.append(a)
#                LQs_cities.append([u'Россия'])
#                LQs_dates.append(dates)
#                LQs_posneg.append(posneg)
#                LQs_adds.append(adds)
""" """

#LQj_done={}


PreStr_done = {}

def savingPreStr(LQj,qstr,cityj):
  todo = [LQs_files[LQj] ,LQs_cats[LQj],qstr,LQs_cities[LQj][cityj],LQs_dates[LQj][0],LQs_dates[LQj][1]]
#  todo = [LQs_files[LQj] ,LQs_cats[LQj],qstr,LQs_cities[LQj][cityj],str(LQs_posneg[LQj]),LQs_adds[LQj],LQs_dates[LQj][0],LQs_dates[LQj][1]]
  return ','.join([s.replace(',','&') for s in todo])

def loadStuff(fname):
  if(1):
    """ Deals badly with commas in qs"""
    with open(fname) as f:
      LS = f.readlines()[1:]
    #print(len(LS))
    LS=LS[1:]
    try:
        LS = [s.decode('utf-8') for s in LS]
    except:
        pass
#    LS = [[s.replace('&',',').replace(u'&',',') for s in L.split(',')[0:4]] for L in LS[1:]]
    LS = [[s for s in L.replace('\r\n','').split(',')[0:-1]] for L in LS]
  else:
    import csv
    
    with open(fname,'rb') as f:
      r = csv.reader(f)
      #print(len(LS))
      LS= [row for row in r]
      LS = [L[0:4] for L in LS[1:]]
      
  for L in LS:
    PreStr = ','.join(L)
#    print(PreStr)
    PreStr_done[unicode(PreStr)]=1
    

for tfname in infnames:
    print('loading %s..'%tfname),
    try:
        loadStuff(tfname)
        print('ok')
    except:
        print('err.')
        pass
try:
  print('Checking if file already exists..')
  loadStuff(outfname)  
except:
  outfname = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__v13b__KNA_wtf_wtF__'+'pagesC_percity.csv'

  with open(outfname,'w') as f:
    f.write('original file,category,q,city,posneg,adds,date_from,date_to,Res\r\n')

#
#aaa





def downloadEnclosures(i, q,outq,wipq,doneq):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """

    print '%s: Logining..' % i
    if(0):    
        driver = webdriver.Firefox()
        driver.set_page_load_timeout(30)
        
        if(0): # need to login ?
            driver.get('https://passport.yandex.ru/passport?mode=auth')
            driver.find_element_by_xpath('//input[@id="login"]').clear()
            driver.find_element_by_xpath('//input[@id="login"]').send_keys('iwan.vepreff')#ya.kolchanov2015')
            driver.find_element_by_xpath('//input[@id="passwd"]').clear()
            driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
            driver.find_element_by_xpath('//span[@class="_nb-button-content"]').click()
    
    print '%s: log in OK' % i
    qstr = ''
    LQj = -153
    while True:
#        try:
        if(1):
            if(verbose==2):
                print('%s: Reading next Q' % i)
            elif(verbose==1):
                print('%s-q '%i),
            LQj,qstr,cityj = q.get()
            
            if(verbose==2):
                print('%s: q=%s' % (i,qstr))
            
            
            wip_queue.put((LQj,qstr,cityj,))
            
            tqstr = qstr.replace(' ','%20')
#            url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr+'%20%26%26%20'+LQs_adds[LQj],YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
            #url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr,YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
#            url='http://yandex.ru/yandsearch?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr,YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
            url='http://yandex.ru/search?text=%s&lang=ru&rstr=-%i&from_date_full=%s&to_date_full=%s'%(tqstr,YaRegions_NumsDict[LQs_cities[LQj][cityj]],LQs_dates[LQj][0],LQs_dates[LQj][1])
            
            if(verbose==2):
                print(url)
            while(1):
              try:
                #driver.get(url)
                R = requests.get(url+'&ajax={}')
                tD = json.loads(R.text)
                temp_title = tD['serp']['params']['found']
                break
              except:
                print('.')
                pause(pauseAfter+pauseMin+rand()*pauseK)
                pass
            pause(pauseMin+rand()*pauseK)
              
            if(0):
                while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
                  print('%s: "Yandex" not in title! sth wrong' % i)
                  try:
                    driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                    pause(2)
                  except:
                    pause(20)
                    while(1):
                      try:
                        driver.get(url)
                        break
                      except:
                        pass
                      pause(pauseMin+rand()*pauseK)
            
                temp = driver.title
                prevtitle = driver.title
            else:
                pass
            if(verbose==2):
                print('%s: url opened' % i)
            elif(verbose==1):
                print('%s-u '%i),
    
            
    
    
            prevtitle = ''
    
      
            city = cities[cityj]
            Ns = -153
    
    
            
            for j in range(15):
              
              if(0):
                  ttitle = driver.title
              else:
                  ttitle = temp_title
              temp=ttitle.replace(u'млн','000 000')
              temp=temp.replace(u'тыс.','000')
    
              try:
                tempN = int(''.join(re.findall('[0-9]',temp)))
              except:
                if(u'ничего не найдено' in temp):
                  tempN=0
                  break
                else:
                  raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
    
              if(prevtitle!=ttitle):
                break
              pause(pauseAfter+rand()*pauseK)
            Ns = tempN
    
            if(verbose==2):
                print '%s: Done' % i
            elif(verbose==1):
                print('%s-d '%i),

            outq.put((LQj,qstr,cityj,Ns))
            doneq.put((LQj,qstr,cityj,))
    
            q.task_done()
#        except Exception, e:
        else:
            raise e
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1);
        


#aaa

# Filling the Queue
print("Filling the Queue")

NAlreadyDone = 0;
PreStr_done
enclosure_queue = Queue()



for LQjt in range(len(LQs_qs)):
  if(do_fromTheEnd):
    LQj = len(LQs_qs)-1-LQjt
  else:
    LQj = LQjt
  qstr = LQs_qs[LQj]
  try:
    print(qstr)
  except:
    qstr=qstr[0:-1]
    print(qstr)

  """ adding unquoted stuff """
  
  """ adding quoted stuff """
  if((' ' in qstr) or (u' ' in qstr) or ('-' in qstr) or (u'-' in qstr)):
    if((qstr[0]!=u'"') or (qstr[-1]!='"')):
      qstr=u'"'+qstr+'"'
      
  for cityj in range(len(LQs_cities[LQj])):
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    
#    raise ValueError()
    
    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
#      print('done already')
      NAlreadyDone=NAlreadyDone+1
      continue
  
    enclosure_queue.put((LQj,qstr,cityj))


#    print(qstr)
#    
#  for cityj in range(len(LQs_cities[LQj])):
#    PreStr =  savingPreStr(LQj,qstr,cityj) 
#    
#    if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
##      print(PreStr+' done already')
#      NAlreadyDone=NAlreadyDone+1
#      continue
#  
#    enclosure_queue.put((LQj,qstr,cityj))

print('Queue filled.')

print("**************************************************")

print('NAlreadyDone = %i'%NAlreadyDone)
print('Left to do = %i'%enclosure_queue.qsize())

if(stop_after_queue_filled):
    raise ValueError('You wanted to stop here - I stopped here')

pause(4) # simple pause to see the results

# subj
output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()

def CheckWIPnDone():
  print 'CheckWIPnDone:starting'
  wips = {}
  wipN=0
  while(not wip_queue.empty()):
    LQj,qstr,cityj = wip_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    wips[PreStr]=(LQj,qstr,cityj)
    wipN=wipN+1
    
  print('CheckWIPnDone:' +' found %i wipStuff'%wipN)  
  while(not doneq_queue.empty()):
    LQj,qstr,cityj = doneq_queue.get()
    PreStr =  savingPreStr(LQj,qstr,cityj) 
    try:
      wips[PreStr]=0
      wipN=wipN-1
    except:
      print('somehow done but not wip : %s'%PreStr)
      #raise ValueError('somehow wip but not done : %s'%PreStr)
  print('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)  

  for s in  wips.keys():
    if not(wips[s]==0):
      (LQj,qstr,cityj) = wips[s]
      print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(wips[s])
      enclosure_queue.put((LQj,qstr,cityj))

  
  print 'CheckWIPnDone:done'

def time_delta_str(t):
    ns = [86400,3600,60,1]
    ss = ['d','h','m','s']
    
    R = ''
    got_flag=0
    for j in range(len(ns)):
        if((t>ns[j])or got_flag):
            got_flag=1
            k = int(t/ns[j])
            R=R+'%i%s '%(k,ss[j])
            t=t-k*ns[j]
    
    return R

if(1):
  print('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
      pause(pause_per_each_worker_launch)
  print('Workers started.')
  
  
  
  def savingThread(q,q_in):
    print('savingThread: Sarted.')
    last_left=-1
    last_datetime = datetime.datetime.now()
    while(True):
      in_empty_n = 0
      
      ntdn = 0
      while(q.empty()):
        if(q_in.empty()):
          in_empty_n=in_empty_n+1
          print('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
        else:
          if(verbose==2):
              print('savingThread: nothing to do..')
              
          ntdn=ntdn+1
        
        t_now = datetime.datetime.now()
        now_left = q_in.qsize()
        if(last_left<0):
            last_left = now_left
            last_datetime = t_now
            
        if(verbose==2):
            print('~ %i left undone, ntd=%i'%(q_in.qsize(),ntdn))      
        elif(verbose==1):
            timespan = (t_now-last_datetime).total_seconds()
            if( timespan>sTh_report_each_s ):
                print('')
                print('~ %i left undone, ntd=%i'%(q_in.qsize(),ntdn))
                speed = (last_left-now_left)*1.0/timespan
                print('Average speed: %.2f q/s , approx. time left: %s \n ETA ~ %s'%(speed,time_delta_str(now_left/speed),(datetime.datetime.now()+datetime.timedelta(0,now_left/speed))))
                print('')
                last_left = now_left
                last_datetime = t_now
                
        if(ntdn>100):
          print('='*10)
          print('Restarting')
          print('='*10)
          
          print('Killing firefoxes..')
          # Restarting because sth went wrong!
          for proc in psutil.process_iter():
            if proc.name == 'firefox':
              proc.kill()
          print('done.')    
          CheckWIPnDone()    
          
          #########################################################################
          #########################################################################
          #########################################################################

          pause(pause_when_all_workers_fail) # hour+ pause before starting again.
          
          #########################################################################
          #########################################################################
          #########################################################################

          print('restarting workers..')
          for i in range(num_fetch_threads):
              worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
              worker.setDaemon(True)
              worker.start()
              pause(pause_per_each_worker_launch)
          ntdn=0    
          print('done.')
          print('='*10)
              
        
        if(in_empty_n>100):
          print('savingThread: nothing to DO! returning.')
          return
        pause(1)
        
      if(verbose==2):
          print('savingThread: something to do !)')
      elif(verbose==1):
          print('s-s '),
      with open(outfname,'a') as f:
        while(q.empty()==0):
          LQj,qstr,cityj,Ns=q.get()
          f.write((savingPreStr(LQj,qstr,cityj)+u','+str(Ns)+'\r\n').encode('utf8'))
  
  if(wait_for_first_output):
      print('waiting for first output..')
      while(output_queue.empty()):
        pause(.5)
      print('starting saver..')
  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,))
  worker.setDaemon(True)
  worker.start()
  
  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
  print '*** Main thread waiting'
  enclosure_queue.join()
  print '*** Main thread Done waiting'

""" """
#

