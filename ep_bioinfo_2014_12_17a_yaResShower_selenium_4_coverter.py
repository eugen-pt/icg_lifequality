# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 16:24:15 2014

@author: ep_work
"""

from ep_bioinfo_2014_12_16a_src_reader2 import *

LQs_qcats = {LQs_qs[j]:LQs_cats[j] for j in range(len(LQs_qs))}


outfname = '2014-12-11_17-58-24__v4_ThQ__pagesCount.csv'

with open(outfname) as f:
  LS = f.readlines()

LS = [s.split(',') for s in LS]



import xlwt

outpath = 'src/2-redone/'

wbAT = xlwt.Workbook()
sAT = wbAT.add_sheet('1')

wb=xlwt.Workbook()

s = wb.add_sheet('1')

def writeL(L,j,add_date=1):
  if(add_date):
    L.append('12.12.2014')
  for i in range(len(L)):
    try:
      s.write(j,i,int(L[i]))
    except:
      s.write(j,i,unicode(str(L[i])))

def writeLat(L,j,add_date=1):
  if(add_date):
    L.append('12.12.2014')
  for i in range(len(L)):
    try:
      sAT.write(j,i,int(L[i]))
    except:
      sAT.write(j,i,unicode(str(L[i])))

curfile = ''
curcat = ''
headerL=LS[0]

temp = headerL
temp.append('date')
writeLat(temp,0,0)

hn=0

filej=0
for j in range(1,len(LS)):
  L =LS[j]
  hcat = L[1]
  hq = L[2]

  if(len(hcat)==0):
    oc = hcat
    try:
      hcat = LQs_qcats[hq]
    except:
      tq =unicode(hq)
      try:
        hcat = LQs_qcats[tq]
      except:
        if(tq[0]=='"'):
          tq=tq[1:]
        if(tq[-1]=='"'):
          tq=tq[:-1]
        print(tq)
        hcat = LQs_qcats[tq]
    print(oc+' -> '+hcat+' ( on '+hq+')')

  L[1] = hcat

  writeLat(L,j)
  hfile=L[0]
  if(hfile==curfile):
    pass
  else:
    if(len(curfile)>0):
      print('saving')
      tfile=curfile
      if(tfile[-1]=='x'):
        tfile=tfile[:-1]
      wb.save(outpath+tfile)

    wb=xlwt.Workbook()
    s = wb.add_sheet('1')
    curfile=hfile
    filej=0
#    temp = L[0:2]
#    temp.append(temp[1])
    temp=[headerL[1]]
    temp.extend(headerL[2:])
    temp.append('date')
    writeL(temp,0,0)

  if(hcat==curcat):
    pass
  else:
    filej=filej+1
    writeL([hcat,hcat],filej,0)
    curcat = hcat
    hn=0
  filej=filej+1
  hn=hn+1
#  temp = [hfile,hcat]
#  temp.append(hn)
  temp = [hn]
  temp.extend(L[2:])
  writeL(temp,filej)
wb.save(outpath+curfile[:-1])

wbAT.save(outpath+'AT.xls')

""" """
#

