# -*- coding: utf-8 -*-
"""
Created on Wed Dec 10 16:59:38 2014

@author: ep_work
"""


import xlrd

import os

import numpy as np

path = u'src/1-converted/'


fnames = [u'frequence_спорт.xls',u'культура часть 5 криминальная.xls',u'потребительская корзина и машины.xls',u'предпринимательство_2014.xls']
add = [1,1,0,0]
numdatas=1

path = u'src/1-redone/'
add = [0,0,0,0]
numdatas=2


try:
  LQs_files
  LQs_cats
  LQs_qs
  LQs_datas
  print('lready done..')
except:
  print('doing.')
#  fnames=os.walk(path).next()[-1]
  LQs_files=[]
  LQs_cats = []
  LQs_qs = []
  LQs_datas = []


#  do_not_use = [u'',u'культура часть 1.xlsx',u'культура часть 2.xlsx',u'культура часть 4.xlsx']

  #for fname in [fnames[2]]:#
  for filej in range(len(fnames)):
    fname=fnames[filej]
#    if(fname in do_not_use):
#      continue
    i=add[filej]
    print('file %s..'%fname)
    wb = xlrd.open_workbook(path+fname)
    s=wb.sheet_by_index(0)


    qstr = ''
    curcat = ''
    for j in range(len(s.col_values(0))):
      if(s.col_values(0+i)[j]==1):
        curcat = qstr
#        if(len(qstr)==0):
#          curcat =
        print('%s \\ %s'%(fname,curcat))
      qstr = s.col_values(1+i)[j]
      if(type(s.col_values(0+i)[j])==type(1.0)):
  #      print('.')
        tvs = s.row_values(j)[2+i:]
        if(type(tvs[0])!=type(1.0)):
          continue
        while(1):
          try:
            tvs[tvs.index('')]=0
          except:
            break
        try:
          if(sum(tvs[0:-1])==0):
            continue
        except:
          print(fname)
          print(curcat)
          print(qstr)
          print(tvs)
        # temp solution to exclude one set of doubles..
        if(len(curcat)>0):
          LQs_files.append(fname)
          LQs_cats.append(curcat)
          LQs_qs.append(qstr)
          LQs_datas.append(tvs)


# Manually written
LQs_Cities = u'НОВОСИБИРСК	ТОМСК	ОМСК	КЕМЕРОВО	ВЛАДИВОСТОК	КАЛИНИНГРАД	СОЧИ	МУРМАНСК	САНКТ-ПЕТЕРБУРГ	МОСКВА'.split('\t')
LQs_engCities= ['Nsk','Tomsk','Omsk','Kemerovo','Vlad','Kalin','Sochi','Murmansk','Piter','Moscow']
LQs_CityPopulation= np.array([1548000,557179,1166092,544006,603244,448548,399673,299148,5132000,12111000])


LQs_RegionPopulation = np.array([2731176.00 ,1070128.00 ,1973876.00 ,2734075.00, 1938516.00,963128.00 ,5404273.00 ,771058.00 ,5131942.00 ,12108257.00])
LQs_D = np.array([a[:-numdatas] for a in LQs_datas])

# sum(scores) per city will be = 1
LQs_D_CityScoreNorm = LQs_D/np.sum(LQs_D,axis=0)

# Articles per person
LQs_D_CityPopulationNorm = LQs_D/LQs_CityPopulation
LQs_D_RegionPopulationNorm = LQs_D/LQs_RegionPopulation

LQs_UniqCats_Dixs = {}
for j in range(len(LQs_cats)):
  try:
    LQs_UniqCats_Dixs[LQs_cats[j]].append(j)
  except:
    LQs_UniqCats_Dixs[LQs_cats[j]] = [j]

LQs_UniqCats = np.unique(LQs_UniqCats_Dixs.keys())
LQs_UniqCats_ixs = [LQs_UniqCats_Dixs[s] for s in LQs_UniqCats]

""" category-based means """

from numpy import array
LQs_CM_D = array([np.sum(LQs_D[a,:],axis=0) for a in LQs_UniqCats_ixs])
LQs_CM_D_CityScoreNorm = array([np.sum(LQs_D_CityScoreNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])
LQs_CM_D_CityPopulationNorm = array([np.sum(LQs_D_CityPopulationNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])
LQs_CM_D_RegionPopulationNorm = array([np.sum(LQs_D_RegionPopulationNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])


""" """
#

