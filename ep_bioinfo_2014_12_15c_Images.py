# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 16:12:34 2014

@author: ep_work
"""

from ep_bioinfo_2014_12_10f_src_reader1 import *



figure()

plt.tick_params(\
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',left='off',right='off',         # ticks along the top edge are off
    labelbottom='off',  labelleft='off') # labels along the bottom edge are off
NC = 18

engCats = ['Architecture','Dek-Prikl Art','Design','Art','Innovation','Cinematography','Criminal','Small Business','Auto','Music','Education','General','Consumer basket','Mass media','Sport','Theatre','Farmer','Imagin Lit']

D = LQs_CM_D_CityScoreNorm
#D = LQs_CM_D_RegionPopulationNorm

for cj in range(NC):
  for ci in range(cj+1,NC):
    f=figure()
#    subplot(NC,NC,cj*NC+ci+1)
    plot(D[ci,:],D[cj,:],'.')
    for j in range(len(LQs_Cities)):
      text(D[ci,j],D[cj,j],LQs_engCities[j])
    xlabel(engCats[ci])
    ylabel(engCats[cj])
#    plt.tick_params(\
#        axis='both',          # changes apply to the x-axis
#        which='both',      # both major and minor ticks are affected
#        bottom='off',      # ticks along the bottom edge are off
#        top='off',left='off',right='off' ,        # ticks along the top edge are off
#        labelbottom='off',  labelleft='off') # labels along the bottom edge are off
    show()
    savefig(u'fig_'+engCats[cj]+'_'+engCats[ci]+'.png', bbox_inches='tight')
#    break
    close(f)
#  break

""" """
#

