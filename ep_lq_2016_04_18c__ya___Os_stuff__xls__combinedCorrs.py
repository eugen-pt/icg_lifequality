# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 17:54:15 2016

@author: ep
"""

"""      Imports      """

import xlrd

"""      Consts       """

xlsfname = '2016_04_18b__2__.xls'


def v(a):
    return [c.value for c in a]
def unique(a):
    return list(sort(list(set(a))))


"""   Data Loading    """

def hfilter(a,th=5):
    for j in range(1,len(a)-1):
        if(a[j]>a[j-1]*th)and(a[j]>a[j+1]*th):
            a[j] = (a[j-1]+a[j+1])/2.0
        elif(a[j]<a[j-1]/th)and(a[j]<a[j+1]/th):
            a[j] = (a[j-1]+a[j+1])/2.0
    return a
            
            

try:
    V
    R
except:  
    use_citynorm = 0    
    use_Qcitynorm_in_emo=0
    
    if(use_Qcitynorm_in_emo):
        use_citynorm=1
    print('Loading emo=0..')
    do_emo = 0
    if(do_emo):
        hxlsfname=xlsfname.replace('.xls','emo.xls')
    else:
        hxlsfname=xlsfname.replace('.xls','noemo.xls')
    wb = xlrd.open_workbook(hxlsfname)
    if(use_citynorm):
        s=wb.sheet_by_name('q,c_x_d__citydate_sumN')
    else:
        s=wb.sheet_by_name('q,city_x_dates__expNorm')
    
    V = {}
    V['comb_names'] = v(s.col(0)[1:])
    V['q'] = v(s.col(1)[1:])
    V['city'] = v(s.col(2)[1:])
    V['date_from'] = v(s.row(0)[3:])
    V['emo'] = ['allemo' for a in V['q']]
    R = []
    for rj in range(len(V['q'])):
        tx = v(s.row(rj+1)[3:])
        R.append(hfilter(tx,3))
    
    print('Now emo=1..')
    do_emo = 1
    if(do_emo):
        hxlsfname=xlsfname.replace('.xls','emo.xls')
    else:
        hxlsfname=xlsfname.replace('.xls','noemo.xls')
    wb = xlrd.open_workbook(hxlsfname)
    if(use_Qcitynorm_in_emo):
        s=wb.sheet_by_name('q,pn,c_x_d__Qcitydate_sN')
    else:
        if(use_citynorm):
            s=wb.sheet_by_name('q,pn,c_x_d__citydate_sN')
        else:
            s=wb.sheet_by_name('q,pn,city_x_dates__expN')
    
    V['comb_names'].extend(v(s.col(0)[1:]))
    V['q'].extend(v(s.col(1)[1:]))
    V['emo'].extend(v(s.col(2)[1:]))
    V['city'].extend(v(s.col(3)[1:]))
    #V['date_from2'] = v(s.row(0)[4:])
    
    for rj in range(len(s.col(1)[1:])):
        tx = v(s.row(rj+1)[4:])
        R.append(hfilter(tx,3))
    
"""   Data Loading    """

V['qe'] = [V['q'][j]+'_'+V['emo'][j] for j in range(len(V['q']))] 

uV = {k:unique(V[k]) for k in V}
uV['date_from'] = V['date_from'] # original sorting is better
uV['cd'] = []
for city in uV['city']:
    for d in V['date_from']:
        uV['cd'].append(city+'_'+d)

uV_iD = {k:{uV[k][j]:j for j in range(len(uV[k]))} for k in uV}

""" Generating matrix for corrs """

print('combining matrix for corrrs...')
R_qe_cd = zeros((len(uV['qe']),len(uV['cd'])))
for j in range(len(V['q'])):
    qe = V['qe'][j]
    city = V['city'][j]
    for dj in range(len(V['date_from'])):
        d = V['date_from'][dj]
        cd = city+'_'+d
        
        R_qe_cd[uV_iD['qe'][qe],uV_iD['cd'][cd]] = R[j][dj]


"""    Processing     """

Corrs = np.corrcoef(R_qe_cd)


"""      Saving       """

import xlwt

outfname = '2016_04_18c__cityStacked%s_Corrs.xls'%('_Qcitydate_sN' if use_Qcitynorm_in_emo else ('_citydate_sN' if use_citynorm else ''))

w = xlwt.Workbook()
s = w.add_sheet('cityCombined_Corrs')
s.write(0,0,'combined_name')
s.write(1,1,'q')
s.write(2,2,'emo')
for qej in range(len(uV['qe'])):
    s.write(0,qej+3,uV['qe'][qej])
    s.write(1,qej+3,uV['qe'][qej].split('_')[0])
    s.write(2,qej+3,uV['qe'][qej].split('_')[1])
    s.write(qej+3,0,uV['qe'][qej])
    s.write(qej+3,1,uV['qe'][qej].split('_')[0])
    s.write(qej+3,2,uV['qe'][qej].split('_')[1])
    for qei in range(len(uV['qe'])):
        s.write(qej+3,qei+3,Corrs[qej,qei])
w.save(outfname)
""" """
#
