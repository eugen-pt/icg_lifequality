# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 11:13:21 2015

@author: ep
"""



fname = '2015-03-18_13-05-35__v9_Regions__pagesC_percityAgregated_OKVED_cities.xls'
outfname = fname.replace('xls','tsv')

""" data preparation """
if(1):
    import xlrd
    
    
    wb = xlrd.open_workbook(fname)
    
    s=wb.sheet_by_index(0)
    
    Data = [[c.value for c in s.row(j)[3:]] for j in range(1,len(s.col(0)))]
    Cities = [c.value for c in s.row(0)[3:]]
    
#    Qs = [c.value for c in s.col(2)[1:]]
#    Data = array(Data).transpose()    
    if(0):
        with open(outfname,'w') as f:
            f.write('\t'.join(Cities)+'\n')
            for r in Data:
                f.write('\t'.join(['%s'%c for c in r])+'\n')        


Data=array(Data).transpose()
corrMx = corrcoef(Data.transpose())
cmf = corrMx.flatten()

t = sum(Data,axis=1)
nData = array([a/t for a in Data.transpose()]).transpose()
ncorrMx = corrcoef(nData.transpose())
ncmf = ncorrMx.flatten()

tix = find((~isnan(cmf))*(~isnan(ncmf)))
cmf=cmf[tix]
ncmf=ncmf[tix]
aaa
""" """


import pebl
from pebl import data
from pebl.learner import greedy
dataset = data.fromfile(outfname)
dataset.discretize()
learner = greedy.GreedyLearner(dataset, max_time=600)
ex1result = learner.run()
ex1result.tohtml("example4_Qs-result")

""" """
#
#from pebl import data, result
#from pebl.learner import greedy, simanneal
#from pebl.taskcontroller import multiprocess
#dataset = data.fromfile(outfname)
#learners = [ greedy.GreedyLearner(dataset, max_iterations=1000000) for i in range(5) ] + \
#           [ simanneal.SimulatedAnnealingLearner(dataset) for i in range(5) ]
##learners = [ greedy.GreedyLearner(dataset, max_iterations=1000000) for i in range(5) ] 
#tc = multiprocess.MultiProcessController(poolsize=6)
#results = tc.run(learners)
#merged_result = result.merge(results)
#merged_result.tohtml("example4-result")
""" """
#