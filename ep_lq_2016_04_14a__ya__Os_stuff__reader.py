# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 09:06:55 2016

@author: ep
"""

"""      Imports      """



"""      Consts       """

outfname = '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity___ALLTOGETHER_noOs.csv'

def unique(a):
    return list(set(a))

""" Local Definitions """

try:
    LS
    Rd
except:    
    with open(outfname,'r') as f:
        LS = f.readlines()
    
    LS = [s.decode('utf8') for s in LS]

    LS = [s.replace('\r\n','').split(',') for s in LS]
    header = LS[0]
    LS = LS[1:]
    
    #if(1):
    intVals = ['posneg','Res']

    Rd = []
    for L in LS:
        R = {}
        for j in range(len(header)):
            R[header[j]] = L[j]
            if(header[j] in intVals):
                R[header[j]] = int(L[j])
        Rd.append(R)
"""   Data Loading    """

uniqueVals = {}

for h in header:
    uniqueVals[h] = unique([d[h] for d in Rd])


"""    Processing     """


"""      Saving       """



""" """
#
