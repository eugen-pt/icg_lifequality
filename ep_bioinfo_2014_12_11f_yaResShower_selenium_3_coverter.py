# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 16:24:15 2014

@author: ep_work
"""

outfname = '2014-12-11_12-38-51__pagesCount.csv'

with open(outfname) as f:
  LS = f.readlines()

import xlwt

wb=xlwt.Workbook()

s = wb.add_sheet('1')

for j in range(len(LS)):
  L =LS[j].split(',')
  for i in range(len(L)):
    try:
      s.write(j,i,int(L[i]))
    except:
      s.write(j,i,unicode(str(L[i])))
wb.save('2014-12-11_12-38-51__pagesCount.xls')
""" """
#

