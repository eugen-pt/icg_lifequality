# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 17:54:15 2016

@author: ep
"""

"""      Imports      """

import xlrd

"""      Consts       """

xlsfname = '2016_04_18b__2__.xls'


def v(a):
    return [c.value for c in a]
def unique(a):
    return list(sort(list(set(a))))


"""   Data Loading    """

def hfilter(a,th=5):
    for j in range(1,len(a)-1):
        if(a[j]>a[j-1]*th)and(a[j]>a[j+1]*th):
            a[j] = (a[j-1]+a[j+1])/2.0
        elif(a[j]<a[j-1]/th)and(a[j]<a[j+1]/th):
            a[j] = (a[j-1]+a[j+1])/2.0
    return a
            
            

try:
    V
    R
except:  
    use_citynorm = 0    
    use_Qcitynorm_in_emo=0
    
    if(use_Qcitynorm_in_emo):
        use_citynorm=1
    print('Loading emo=0..')
    do_emo = 0
    if(do_emo):
        hxlsfname=xlsfname.replace('.xls','emo.xls')
    else:
        hxlsfname=xlsfname.replace('.xls','noemo.xls')
    wb = xlrd.open_workbook(hxlsfname)
    if(use_citynorm):
        s=wb.sheet_by_name('q,c_x_d__citydate_sumN')
    else:
        s=wb.sheet_by_name('q,city_x_dates__expNorm')
    
    V = {}
    V['comb_names'] = v(s.col(0)[1:])
    V['q'] = v(s.col(1)[1:])
    V['city'] = v(s.col(2)[1:])
    V['date_from'] = v(s.row(0)[3:])
    V['emo'] = ['allemo' for a in V['q']]
    R = []
    for rj in range(len(V['q'])):
        tx = v(s.row(rj+1)[3:])
        R.append(hfilter(tx,3))
    
    print('Now emo=1..')
    do_emo = 1
    if(do_emo):
        hxlsfname=xlsfname.replace('.xls','emo.xls')
    else:
        hxlsfname=xlsfname.replace('.xls','noemo.xls')
    wb = xlrd.open_workbook(hxlsfname)
    if(use_Qcitynorm_in_emo):
        s=wb.sheet_by_name('q,pn,c_x_d__Qcitydate_sN')
    else:
        if(use_citynorm):
            s=wb.sheet_by_name('q,pn,c_x_d__citydate_sN')
        else:
            s=wb.sheet_by_name('q,pn,city_x_dates__expN')
    
    V['comb_names'].extend(v(s.col(0)[1:]))
    V['q'].extend(v(s.col(1)[1:]))
    V['emo'].extend(v(s.col(2)[1:]))
    V['city'].extend(v(s.col(3)[1:]))
    #V['date_from2'] = v(s.row(0)[4:])
    
    for rj in range(len(s.col(1)[1:])):
        tx = v(s.row(rj+1)[4:])
        R.append(hfilter(tx,3))
    
"""   Data Loading    """

V['qe'] = [V['q'][j]+'_'+V['emo'][j] for j in range(len(V['q']))] 

temp = V['qe']
V['qe'] = V['city']
V['city'] = temp

uV = {k:unique(V[k]) for k in V}
uV['date_from'] = V['date_from'] # original sorting is better
uV_iD = {k:{uV[k][j]:j for j in range(len(uV[k]))} for k in uV}

R_d_qe_c = zeros((len(uV['date_from']),len(uV['qe']),len(uV['city'])))

for j in range(len(V['q'])):
    qe = V['qe'][j]
    city = V['city'][j]
    for dj in range(len(V['date_from'])):
        d = V['date_from'][dj]
        R_d_qe_c[uV_iD['date_from'][d],uV_iD['qe'][qe],uV_iD['city'][city]] = R[j][dj]

figure()
plt.imshow(np.corrcoef(R_d_qe_c[0].transpose()))
plt.title('No norm')
plt.colorbar()

R_d_qe_c_N = R_d_qe_c*1.0
R_d_qe_c_4norm = sum(R_d_qe_c,axis=2)
for dj in range(shape(R_d_qe_c)[0]):
    for qej in range(shape(R_d_qe_c)[1]):
        R_d_qe_c_N[dj,qej,:] = R_d_qe_c[dj,qej,:]/R_d_qe_c_4norm[dj][qej]
        
figure()
plt.imshow(np.corrcoef(R_d_qe_c_N[0].transpose()))
plt.title('Norm')
plt.colorbar()


"""    Processing     """

Corrs = array([np.corrcoef(A.transpose()) for A in R_d_qe_c_N])


"""      Saving       """

import xlwt

print('saving..')

outfname = '2016_04_20a__dateSep__QCorrs.xls'

w = xlwt.Workbook()

mCorrs = mean(Corrs,axis=0)
s = w.add_sheet('avgCorrs')
for rj in range(len(uV['city'])):
    s.write(0,rj+1,uV['city'][rj])
    s.write(rj+1,0,uV['city'][rj])
    for ri in range(len(uV['city'])):
        s.write(rj+1,ri+1,mCorrs[rj,ri])

mCorrs = np.max(Corrs,axis=0)
s = w.add_sheet('maxCorrs')
for rj in range(len(uV['city'])):
    s.write(0,rj+1,uV['city'][rj])
    s.write(rj+1,0,uV['city'][rj])
    for ri in range(len(uV['city'])):
        s.write(rj+1,ri+1,mCorrs[rj,ri])

mCorrs = np.min(Corrs,axis=0)
s = w.add_sheet('minCorrs')
for rj in range(len(uV['city'])):
    s.write(0,rj+1,uV['city'][rj])
    s.write(rj+1,0,uV['city'][rj])
    for ri in range(len(uV['city'])):
        s.write(rj+1,ri+1,mCorrs[rj,ri])

mCorrs = np.max(abs(Corrs),axis=0)
s = w.add_sheet('maxAbsCorrs')
for rj in range(len(uV['city'])):
    s.write(0,rj+1,uV['city'][rj])
    s.write(rj+1,0,uV['city'][rj])
    for ri in range(len(uV['city'])):
        s.write(rj+1,ri+1,mCorrs[rj,ri])
        
sCorrs = std(Corrs,axis=0)
s = w.add_sheet('stdCorrs')
for rj in range(len(uV['city'])):
    s.write(0,rj+1,uV['city'][rj])
    s.write(rj+1,0,uV['city'][rj])
    for ri in range(len(uV['city'])):
        s.write(rj+1,ri+1,sCorrs[rj,ri])


for th in [0.99,0.95,0.9,0.8,0.75,0.6,0.5]:
    tNaboveT = sum(Corrs>th,axis=0)
    s = w.add_sheet('Nd_above_%.2f'%th)
    for rj in range(len(uV['city'])):
        s.write(0,rj+1,uV['city'][rj])
        s.write(rj+1,0,uV['city'][rj])
        for ri in range(len(uV['city'])):
            s.write(rj+1,ri+1,tNaboveT[rj,ri])
    
    tNaboveT_pairs = []
    tNaboveT_pairs_N = [] 
    for rj in range(len(uV['city'])):
        for ri in range(rj+1,len(uV['city'])):
            tNaboveT_pairs.append([uV['city'][rj],uV['city'][ri]])
            tNaboveT_pairs_N.append(tNaboveT[rj,ri])
    tix = argsort(tNaboveT_pairs_N)[::-1]
    s = w.add_sheet('Nd_above_%.2f_sortedList'%th)
    s.write(0,0,'city1')
    s.write(0,1,'city2')
    s.write(0,2,'N dates corr>%.2f'%th)
    for j in range(len(tix)):
        i=tix[j]
        s.write(j+1,0,tNaboveT_pairs[i][0])
        s.write(j+1,1,tNaboveT_pairs[i][1])
        s.write(j+1,2,tNaboveT_pairs_N[i])
        
    
    

for dj in range(len(V['date_from'])):
    d = V['date_from'][dj]
    
    s = w.add_sheet(d)
    for rj in range(len(uV['city'])):
        s.write(0,rj+1,uV['city'][rj])
        s.write(rj+1,0,uV['city'][rj])
        for ri in range(len(uV['city'])):
            s.write(rj+1,ri+1,Corrs[dj][rj,ri])


w.save(outfname)

print('Finished.')
""" """
#
