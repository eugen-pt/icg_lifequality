# -*- coding: utf-8 -*-
"""
Created on Thu May 12 12:21:34 2016

@author: ep
"""

"""      Imports      """

import urllib
from BeautifulSoup import BeautifulSoup

"""      Consts       """

url = 'https://ru.wiktionary.org/wiki/%D0%9F%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5:%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%87%D0%B0%D1%81%D1%82%D0%BE%D1%82%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D0%BF%D0%BE_%D0%9D%D0%9A%D0%A0%D0%AF/%D1%81%D1%83%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5'

""" Local Definitions """

R=urllib.urlopen(url)

"""   Data Loading    """

soup = BeautifulSoup(R.read())

NCRL_1000nouns = [a.find('a').text for a in soup.find('div',attrs={'id':"mw-content-text"}).findAll('li')]

"""    Processing     """


"""      Saving       """



""" """
#
