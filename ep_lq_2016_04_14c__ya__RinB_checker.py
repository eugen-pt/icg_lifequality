# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 17:17:37 2016

@author: ep
"""

"""      Imports      """


"""      Consts       """

outfname = '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity___ALLTOGETHER_noOs.csv'

infnames = [ \
            '2016-04-13_11-30-58__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity_ep.csv', \
#            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity_o_2016_04_11.csv' , \
            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity_ep_fmri_2016_04_12e.csv', \
            '2016-04-12_14-35-29__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
#            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity_o_2016_04_12e.csv' , \
            '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity_ep_fmri_vm_2016_04_13a.csv', \
            ]


infnames = [ \
        '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep_RBt.csv', \
        '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep.csv' , \
        ]


####
###
##
#
#suffix=''
#outfname = '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep_RBt%s.csv'%suffix #3
#
#infnames = [ \
#            '2016-04-08_12-59-00__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
#            '2016-04-08_12-56-48__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
#            '2016-04-08_13-02-26__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
#            '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity.csv', \
#            '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep.csv', \
#            '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep_RBt_4syns0.csv', \
#            ]
#infnames       = infnames + [outfname]
#
#
#outfname =     '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep__ALLTOGETHER.csv'      

#
##
###
####


""" Local Definitions """

PreStr_done = {}

doubles = {}

def loadStuff(fname):
  if(1):
    """ Deals badly with commas in qs"""
    with open(fname) as f:
      LS = f.readlines()[1:]
    #print(len(LS))
    LS=LS[1:]
    try:
        LS = [s.decode('utf-8') for s in LS]
    except:
        pass
#    LS = [[s.replace('&',',').replace(u'&',',') for s in L.split(',')[0:4]] for L in LS[1:]]
    LS = [[s for s in L.replace('\r\n','').split(',')] for L in LS]
  else:
    import csv
    
    with open(fname,'rb') as f:
      r = csv.reader(f)
      #print(len(LS))
      LS= [row for row in r]
      LS = [L[0:4] for L in LS[1:]]
      
  for L in LS:
    PreStr = ','.join(L[0:-1])
#    print(PreStr)
    if(PreStr in PreStr_done):
        if(PreStr in doubles):
            doubles[PreStr][fname] = int(L[-1])
        else:
            doubles[PreStr] = {fname:int(L[-1])}
    PreStr_done[unicode(PreStr)]=int(L[-1])

with open(infnames[0],'r') as f:
    for line in f:
        header = line
        break

for fname in infnames:
    print(fname+'..'),
    loadStuff(fname)
    print('.ok')
    
#aaa
#
y = array([std(doubles[k].values())/(0.0001+mean(doubles[k].values())) for k in doubles])
tix = find(y>1)
#aaa
hqs = doubles.keys()
for j in tix:
    print(hqs[j])
    for k in doubles[hqs[j]]:
        print(' - %30s : %i'%('...'+k[55:],doubles[hqs[j]][k]))
#    
aaa    
print('writing..'),    
with open(outfname,'w') as f:
    f.write(header)
    for k in PreStr_done:
        f.write('%s,%i\r\n'%(k,PreStr_done[k]))
print('ok..')
"""   Data Loading    """



"""    Processing     """


"""      Saving       """



""" """
#
