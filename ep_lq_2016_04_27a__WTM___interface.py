# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 18:02:24 2016

@author: ep
"""

"""      Imports      """

import json
import requests
import uuid
import datetime

"""      Consts       """

WTM_root_url = 'http://icg.8ep.ru/utasks1/'

""" Local Definitions """

def raiseError(R):
    text = R.text
    #if('</html>' in R):
    if(len(R.text)>100):
        if('<div id="summary">' in R) and ('</pre>' in R):
            text = R.text[R.text.index('<div id="summary">')+len('<div id="summary">'):R.text.index('</pre>')]
        else:
            tempfname = 'WTMie_%s_%s.html'%(datetime.datetime.now().strftime('%Y%m%d_%H%M%S'),uuid.uuid4())
            with open(tempfname,'w') as f:
                f.write(R.text)
            text = tempfname
            with open('WTM_interface__errors.txt','a') as f:
                f.write("%s\n"%tempfname)
    raise ValueError('%i: %s'%(R.status_code,text))

def myjson(d):
    if(type(d)!=type({})):
        return json.dumps(d)
    else:
        ks = d.keys()
        ks.sort()
        return '{'+', '.join([myjson(k)+': '+myjson(d[k]) for k in ks])+'}'

def WTM_addTask(task='',context="test_fromInt"):
    if(len(task)==0):
        task = {'q':'test_q_from_interface'}
    if(type(task)==type({})):
        task = myjson(task)
    R=requests.post(WTM_root_url+'add/', \
                    data={"task_dict":task, \
                    "task_context":context})
    if(R.status_code not in [201,200]):
        raiseError(R)

def WTM_addTasks(tasks,context="test_fromInt",no_duplicates=0):
    tasks = [myjson(a) for a in tasks]
    R=requests.post(WTM_root_url+'add/', \
                    data={"task_dicts":json.dumps(tasks), \
                    "task_context":context,'no_duplicates':no_duplicates})
    if(R.status_code not in [200,201]):
        raiseError(R)

def WTM_getTask(context='',N=10,decode_task_dict=1,get_random=1):

    R=requests.get(WTM_root_url+'get_task/?N=%i%s&get_random=%i'%(N,('&task_context=%s'%context if context else ''),get_random))

    if(R.status_code!=200):
        raiseError(R)

    if(decode_task_dict):
        T = json.loads(R.text)
        for task in T:
            task['task_dict'] = json.loads(task['task_dict'])
        return T
    else:
        return json.loads(R.text)

def WTM_sendResult(worker,task_id,task_s,result):
    R=requests.post(WTM_root_url+'add_result/',\
                    data={'id':task_id,'task_dict':task_s,'result':myjson(result),'worker':worker})
    if(R.status_code!=201):
        raiseError(R)

# results = [{'id':1,'task_dict':'{}','result':'{}'}  ,{} , ...]
def WTM_sendResults_many(worker,results):
    for res in results:
        if type(res['result'])==type({}):
            res['result']=myjson(res['result'])
        if type(res['task_dict'])==type({}):
            res['task_dict']=myjson(res['task_dict'])

    R=requests.post(WTM_root_url+'add_result_multi/',\
                    data={'results':json.dumps(results),'worker':worker})
    if(R.status_code!=201):
        raiseError(R)

"""   Data Loading    """

def WTM_getResults(context='',N=-1):

    R=requests.get(WTM_root_url+'results/?%s&N=%i&json=1'%('' if context=='' else 'task_context=%s'%context,N))

    if(R.status_code!=200):
        raiseError(R)

    T = json.loads(R.text)

    for tid in T['tasks_by_id']:
        T['tasks_by_id'][tid]['task'] = json.loads(T['tasks_by_id'][tid]['task'])
    for res in T['results']:
        res['results'] = json.loads(res['results'])

    return T
"""    Processing     """


"""      Saving       """
#aaa
if __name__ == "__main__":
    aaa
    print('..adding task..')
    for j in range(10):
        WTM_addTask(task = {'q':'%i'%(j%3)},context="test_fromInt8")
    print('..adding tasks..')
    WTM_addTasks([{'q':'%i'%(j%30)} for j in range(100)],context="test_fromInt8_many")
    print('..getting task..')
    Tasks = WTM_getTask(context="test_fromInt8_many")
    print(Tasks)
    T = Tasks[0]
    print(T)
    #WTM_sendResult('interfaceMain',T['id'],T['task_dict'],{'interface_tested':1})

    print('..sending results..')
    WTM_sendResults_many('interfaceMain_many',[{'id':T['id'],'task_dict':T['task_dict'],'result':myjson({'interface_tested':1})} for j in range(5)])
    print('Finished.')
""" """
#
