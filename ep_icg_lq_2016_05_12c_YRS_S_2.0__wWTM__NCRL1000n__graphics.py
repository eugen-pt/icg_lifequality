# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work

""""""

Checklist:

- check worker variable (line 43 :) )
- check version (line 47)
- check experiment name (line 49)   --  !!! This is IMPORTANT







"""
from   colorama import init
init()

def printXY(s, x=0, y=0):
    print("\033["+str(int(y))+";"+str(int(x))+"H"+s)

for j in range(64):
    print('\r\n')

""" """


worker_name = 'icg12-ep-2'
#worker_name = ''
if worker_name == '':
    for t in [("socket","gethostname()"), ('os','uname()[1]'), ('platform','uname()[1]')]:
        try:
            worker_name = eval("__import__('%s').%s"%t)
        except:
            pass
# Check that everything's OK and worker is set to a reasonable string
printXY('Worker name : [%s]'%worker_name,0,0)


#aaa_checkWorkerName__commentThisIfOK


# See that thiss corresponds to version
version = '2.0.0'

experiment = '2_0_0__2016_05_12a__NCRL1000n'


email_subj_prefix = '[ICG_Ya]@%s: '%(worker_name)


"""
        Operating options
"""

# 1 => doesnt actually start parsing
stopAfterCheckDone = 0

# 0 => nothing, 1=> short, 2 => lots of stuff including URLs, etc
#   -1 => hmm=)
verbose = -1

# 0 = from start , 1= from end, 2 = randomized--undone
# not implemented in the online-based tool
do_fromTheEnd = 0






if(1):
    num_fetch_threads = 8
    num_fetch_threads_only_json = 0
    suffix = ''
else:
    num_fetch_threads = 0
    num_fetch_threads_only_json = 1
    suffix = '_j'

report_every_minute = 0
report_every_hour = 1
report_every_N = 5000

[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered,pauseBetweenRetries] = [.05,.05,.05,.3,15]
#[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered,pauseBetweenRetries] = [1,.2,.05,.5,10]

pauseBetweenWorkerStart = 10    # may be helpful to enter captcha
pauseWhenYandexError = 20

#from ep_bioinfo_2014_12_10f_src_reader1 import *
#from ep_bioinfo_2015_01_29d_src_reader3_3 import *
#from ep_icg_lq_2015_12_24c__phil__words import *
from ep_icg_lq_2016_01_11c_countries_load import *


# for cities list
from ep_icg_lq_2016_04_28b__Ya__regionslist import *


#aaa__checkIfInputLoadedOK


from scipy import misc
import subprocess

import datetime
import gzip
import json
import re
import time
import zipfile
from   numpy import array,sort
from   Queue import Queue
from   selenium import webdriver
from   selenium.webdriver.common.keys import Keys
from   threading import Thread
from   time import sleep as pause

from   ep_icg_lq_2015_12_25d__email__test import sendemail

from   ep_lq_2016_04_27a__WTM___interface import *

# System modules





""" """
""" Setting starting working parameters """
""" """


def returnCursor():
    printXY('',1,19)

minPrintX=5
maxPrintX = 60
def printI(s,i,printX=-1,minX = minPrintX,maxX = maxPrintX):
    y=i+2
    if(printX<0)or(printX+len(s)>maxPrintX):
        printXY(' '*(maxX-minX),minX,y)
        printX = minX
    printXY(s,printX,y)
    returnCursor()
    return printX+len(s)

minY = 15
maxY = 19
def eprint(s):
    if(verbose!=-1):
        print(s)
        return
    printXY(' ',1,eprint.nowY)
    eprint.nowY+=1
    if(eprint.nowY>maxY):
        eprint.nowY = minY
        printXY(' '*60,0,eprint.nowY)
    printXY('.',1,eprint.nowY)
    printXY(s,2,eprint.nowY)
    returnCursor()#printXY('0-0',0,0)
eprint.nowY = maxY
#printXY('10-0',10,0)
#printXY('0-10',0,10)
#printXY('20-15',20,15)


def savingPreStr(tq):
    return myjson(tq)

def rand():
    return ord(os.urandom(1))/255.0


#aaa
""" """
""" """
""" """


def downloadEnclosures(i, q, outq, wipq, doneq, only_json = 0):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    if(verbose==-1):
        printI('% 2i :'%i,i,minX=0)
        printX = minPrintX
        printX=printI('Starting driver..',i,printX)
    else:
        eprint('%s: Starting driver..'%i)
    driver = webdriver.Firefox()
    driver.set_page_load_timeout(30)


    if(0): # need to login ?
        print '%s: Logining..' % i
        driver.get('https://passport.yandex.ru/passport?mode=auth')
        driver.find_element_by_xpath('//input[@id="login"]').clear()
        driver.find_element_by_xpath('//input[@id="login"]').send_keys('iwan.vepreff')#ya.kolchanov2015')
        driver.find_element_by_xpath('//input[@id="passwd"]').clear()
        driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
        driver.find_element_by_xpath('//span[@class="_nb-button-content"]').click()
        print '%s: log in OK' % i
        printX=printI('Starting driver..',i,printX)
    if(verbose==-1):
        printX=printI('Started OK ',i,printX)
    else:
        eprint('%s: Started OK'%i)
    qstr = ''
    LQj = -153
    while True:
        try:
            if(verbose==2):
                eprint('%s: Reading next Q' % i)
            elif(verbose==1):
                eprint('%s-q '%i),
            elif(verbose==-1):
                pass
                #printX=printI('q',i,printX)


            task = q.get()
            hq = task['task_dict']
            qstr = hq['q']

            if(verbose==2):
                eprint('%s: q=%s' % (i,qstr))
            elif(verbose==-1):
                printX=printI('+',i,printX)

            # checking as WorkInProgress task
            wip_queue.put(task)

            # preparing for URL
            tqstr = qstr.replace(' ','%20').replace('&','%26').replace('|','%7C')
            regstr = ('&rstr=-%i'%(hq['city_num'] if 'city_num' in hq else YaRegions_NumsDict[hq['city']])) if (len(hq['city'])>0) else ''


            if(only_json):
                url='http://yandex.ru/search?text=%s&lang=ru%s&from_date_full=%s&to_date_full=%s&ajax={}'%(tqstr,regstr,hq['dates'][0],hq['dates'][1])
            else:
                url='http://yandex.ru/yandsearch?text=%s&lang=ru%s&from_date_full=%s&to_date_full=%s'%(tqstr,regstr,hq['dates'][0],hq['dates'][1])

            if(verbose==2):
                eprint(url)
            while(1):
                try:
                    driver.get(url)
                    if(only_json):
                        tD = json.loads(driver.find_element_by_tag_name('pre').text)
                        temp_title = tD['serp']['params']['found']
                    break
                except:
                    if(verbose==-1):
                        printX=printI('.',i,printX)
                    else:
                        eprint('.')
                pause(pauseBetweenRetries)
                pass
            pause(pauseMin+rand()*pauseK)


            if(only_json==0):
                while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
                  eprint('%s: "Yandex" not in title! sth wrong')
                  try:
                    driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                    pause(1)
                  except:
                    pause(pauseWhenYandexError)
                    while(1):
                      try:
                        driver.get(url)
                        break
                      except:
                        pass
                    pause(pauseMin+rand()*pauseK)

            #print '%s: url opened' % i

                temp = driver.title
            else:
                pass

            if(verbose==2):
                eprint('%s: url opened' % i)
            elif(verbose==1):
                eprint('%s-u '%i),
            elif(verbose==-1):
                pass
                #printX=printI('u',i,printX)


            prevtitle = ''
            Ns = -153

            nothingInTheTitle = 1
            for j in range(15):
                if(only_json==0):
                    ttitle = driver.title
                else:
                    ttitle = temp_title

                temp=ttitle.replace(u'млн','000 000').replace(u'тыс.','000')

                try:
                    tempN = int(''.join(re.findall('[0-9]',temp)))
                except:
                    if(u'ничего не найдено' in temp):
                        tempN=0
                    else:
                        pass# raise ValueError('wtf cant find a number in the title')
                        #raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
                if(prevtitle!=ttitle):
                    nothingInTheTitle=0
                    break
                pause(pauseAfter+rand()*pauseK)

            # if nothing after 15 tries with pauses
            if(nothingInTheTitle):
                raise ValueError('wtf cant find a number in the title')
            Ns = tempN

            res = {'N':Ns}

            #print '%s: Done' % i
            outq.put((task,res))
            doneq.put(task)

            q.task_done()
        except Exception, e:
            if(verbose==-1):
                printX=printI('%s: EXCEPTION OCCURED: %s' % (i,e),i,maxPrintX)
            else:
                eprint('%s: EXCEPTION OCCURED: %s' % (i,e))
            #raise e
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1)


#aaa
# Filling the Queue
#eprint("Filling the Queue")

#aaa


NAlreadyDone = 0
enclosure_queue = Queue()



output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()


if(stopAfterCheckDone):
    raise ValueError('stopAfterCheckDone')



def CheckWIPnDone():
    threadName = 'CheckWIPnDone :'
    i = 14
    #printXY(threadName,1,i+1)
    minX = len(threadName)+2
    #printI('starting',i,minX=minX)
    wips = {}
    wipN=0
    while(not wip_queue.empty()):
        hq = wip_queue.get()
        PreStr =  savingPreStr(hq)
        wips[PreStr]=hq
        wipN=wipN+1

    eprint('CheckWIPnDone:' +' found %i wipStuff'%wipN)
    while(not doneq_queue.empty()):
        hq = doneq_queue.get()
        PreStr =  savingPreStr(hq)
        try:
            wips[PreStr]=0
            wipN=wipN-1
        except:
            eprint('somehow done but not wip : %s'%PreStr)
#      raise ValueError('somehow wip but not done : %s'%PreStr)
    eprint('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)

    for s in  wips.keys():
        t = wips[s]
        if(t==0):
            pass
        else:
            hq = t
            print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(t)
            enclosure_queue.put(hq)


    #print 'CheckWIPnDone:done'


# This version does not send any kind of file
def reportEmail(subj='Hello!',body='Hello!'):
    #eprint('Reporting E-mail subj=%s  body=%s'%(subj,body))
    #sendemail(subj=email_subj_prefix+subj,body=body)
    return #


Workers = []

def start_all_workers(num_fetch_threads=num_fetch_threads):
#  for worker in Workers:
#    del worker
  #eprint('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
      Workers.append(worker)
      pause(pauseBetweenWorkerStart)
  for i in range(num_fetch_threads_only_json):
      worker = Thread(target=downloadEnclosures, args=(num_fetch_threads+i, enclosure_queue,output_queue,wip_queue,doneq_queue,1))
      worker.setDaemon(True)
      worker.start()
      Workers.append(worker)
      pause(pauseBetweenWorkerStart)
  #eprint('Workers started.')

if(1):
  import psutil

  def fillerThread(q_in,get_random=0):
      threadName = ('R' if get_random else '') +'fillerThread :'
      if(verbose==-1):
          i = 11 + get_random
          printI(threadName,i,minX=0)
          minX = len(threadName)+2
          printI('starting',i,minX=minX)
      else:
          eprint('\n%s %s'%(threadName,'starting..'))

      while(1):
          while(q_in.qsize()>num_fetch_threads*50):
              printI('%i tasks.. '%q_in.qsize(),i,minX=minX)
              pause(1)
          if(verbose==-1):
              printI('getting tasks..',i,minX=minX)
          else:
              eprint('\n%s %s'%(threadName,'getting tasks..'))
          tasks = []
          for WTMtryJ in range(10):
              try:
                  tasks = WTM_getTask(context = experiment,get_random=get_random,N=20)#*num_fetch_threads+10)
                  WTMtryJ=-1
                  break
              except Exception, e:
                  if(verbose==-1):
                      printI('exception: %s'%e,i,minX=minX)
                  else:
                      eprint('fillerThread: .. %s'%e)

          if(WTMtryJ>=0):
              if(verbose==-1):
                  printI('.. COULDN''T SEND',i,minX=minX)
              else:
                  eprint(threadName+': .. COULDN''T SEND')
              continue
          for task in tasks:
              q_in.put(task)
          if(verbose==-1):
              printI('got tasks, now:%i'%q_in.qsize(),i,minX=minX)
          else:
              eprint(threadName+': got tasks, now:%i'%q_in.qsize())


  def savingThread(q, q_in, sNleft = 0):
    threadName = 'savingThread :'
    if(verbose==-1):
        i = 10
        printI(threadName,i,minX=0)
        minX = len(threadName)+2
        printI('STarted.',i,minX=minX)
    else:
        eprint('savingThread: Started.')
    NDone=0

    last_sent_datetime = datetime.datetime.now()
    last_sent = NDone#q_in.qsize()
    last_sent_hour = datetime.datetime.now().hour
    last_sent_minute = datetime.datetime.now().minute
    last_q = ''

    reportEmail(subj='[S] Running, NLeft report',body='S %i  last:%s'%(last_sent,last_q))

    while(True):
      try:
          in_empty_n = 0

          ntdn = 0
          while(q.empty()):
            if(q_in.empty()):
              in_empty_n=in_empty_n+1
              if(verbose==-1):
                  printI('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n,i,minX=minX)
              else:
                  eprint('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
            else:
              #eprint('savingThread: nothing to do..')
              ntdn=ntdn+1

            #q_in.qsize()
    #        eprint('~ %i left undone, ntd=%i'%(q_in_size,ntdn))


            if(ntdn>40):
              if(verbose==-1):
                  printI(' !! Restarting',i,minX=minX)
              else:
                  eprint('='*10)
                  eprint('Restarting')
              sendemail(subj=email_subj_prefix+'Restarting',body='Started')
              #eprint('='*10)

              if(verbose==-1):
                  printI('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n,i,minX=minX)
              else:
                  eprint('Restarting : Killing firefoxes..')
              # Restarting because sth went wrong!

              for proc in psutil.process_iter():
                if (proc.name == 'firefox')or(proc.name == 'iceweasel'):
                  proc.kill()

              if(verbose==-1):
                  printI('Restarting : Killing firefoxes done.',i,minX=minX)
              else:
                  eprint('done.')

              CheckWIPnDone()

              pause(1800)

              start_all_workers()
              ntdn=0
              #eprint('='*10)
              sendemail(subj=email_subj_prefix+'Restarting',body='Done')


            if(in_empty_n>100):
              eprint('savingThread: nothing to DO! returning.')
              return
            pause(1)
          if(verbose==-1):
              printI('saving !)',i,minX=minX)
          else:
              eprint('savingThread: saving !)')
          temp_saved=0

          try:
              Results
          except:
              Results = []
          if(verbose==-1):
              printI('saving !)',i,minX=minX)
          else:
              eprint('savingThread: N results = %i'%len(Results))
          while not q.empty():
              task,res=q.get()
              last_q = task['task_dict']['q']
              Results.append({'id':task['id'],'task_dict':task['task_dict'],'result':res})
              NDone=NDone+1
              temp_saved=temp_saved+1
          if(verbose==-1):
              printI('saving !)',i,minX=minX)
          else:
              eprint('savingThread: added results , now %i'%len(Results))
          json.dump(Results,open('2016_05_04a__temp_Results.json','w'))
          for jjjj in range(10):
              try:
                  WTM_sendResults_many(worker_name,Results)
                  Results=[]
                  eprint('savingThread: SENT results , now %i'%len(Results))
                  jjjj=-1
                  break
              except Exception,e:
                  eprint('savingThread: .. exception:%s'%e)
          if(jjjj>=0):
              eprint('savingThread: .. COULDN''T SEND')
          timespan = (datetime.datetime.now()-last_sent_datetime).total_seconds()
          speed = (NDone - last_sent)*1.0/(timespan+0.0001)
          if(verbose==-1):
              printI('   saved %i, %i done total   Average speed: %.2f q/s'%(temp_saved,NDone,speed),i,minX=minX)
          else:
              eprint('   saved %i, %i done total   Average speed: %.2f q/s'%(temp_saved,NDone,speed))
              eprint('')
          last_sent = NDone
          last_datetime = datetime.datetime.now()

          try:
              #subprocess.Popen(['sudo','find','/tmp','-amin','+30','-delete'])
              #subprocess.Popen(['sudo','find','/tmp','-amin','+30','-exec','rm','-f','{}','\;'])
              eprint('/tmp cleared')
          except:
              eprint('****************\nError in clearing temp')
              reportEmail(subj='Error in clearing temp',body='N %i  last:%s'%(NDone,str(last_q)))

          if(1):
            if(report_every_N>0) and (abs(NDone-last_sent)>=report_every_N):
                reportEmail(subj='[N] Running, NLeft report',body='N %i  last:%s'%(NDone,str(last_q)))
                last_sent= NDone

            if(report_every_minute>0) and (abs(datetime.datetime.now().minute - last_sent_minute)>=report_every_minute) :
                last_sent_minute = datetime.datetime.now().minute
                reportEmail(subj='[M] Running, NLeft report',body='M %i  last:%s'%(NDone,str(last_q)))

            if(report_every_hour>0)and(abs(datetime.datetime.now().hour-last_sent_hour)>=report_every_hour):
                last_sent_hour = datetime.datetime.now().hour
                reportEmail(subj='[H] Running, NLeft report',body='H %i  last:%s'%(NDone,str(last_q)))
          pause(5)
      except Exception, e:
          printI(' EXCEPTION : %s'%e,i,minX=minX)
          pass


  #eprint('waiting for first output..')
  #while(output_queue.empty()):
  #  pause(.1)
  #eprint('starting saver..')
  #  eprint('NDone=%i'%NDone)

  # straight-up filler
#  worker = Thread(target=fillerThread, args=( enclosure_queue,))
#  worker.setDaemon(True)
#  worker.start()
  # random filler
  eprint('starting fillerThread..')
  worker = Thread(target=fillerThread, args=( enclosure_queue,1))
  worker.setDaemon(True)
  worker.start()



  eprint('waiting for tasks..')
  while(enclosure_queue.empty()):
      pause(2)



  eprint('Starting all workers..')
  start_all_workers()

  eprint('Starting savingThread')
  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,0,))
  worker.setDaemon(True)
  worker.start()

  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
#  print '*** Main thread waiting'
  eprint('..')
  enclosure_queue.join()
#  print '*** Main thread Done waiting'
#  reportEmail('*** Main thread Done waiting','')

""" """
#

