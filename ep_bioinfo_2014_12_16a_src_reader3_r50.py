# -*- coding: utf-8 -*-
"""
Created on Wed Dec 10 16:59:38 2014

@author: ep_work
"""


import xlrd

import os

import numpy as np
from numpy import array



fnames = [u'AT.xls']
path = u'src/'
fnames = ['51_cities_stats_filtered.xls']
add = [1]
numdatas=0

#path = u'src/1-redone/'
#add = [0,0,0,0]
#numdatas=2


try:
  LQs_files
  LQs_cats
  LQs_qs
  LQs_datas
  print('lready done..')
except:
  print('doing.')
#  fnames=os.walk(path).next()[-1]
  LQs_files=[]
  LQs_cats = []
  LQs_qs = []
  LQs_datas = []


#  do_not_use = [u'',u'культура часть 1.xlsx',u'культура часть 2.xlsx',u'культура часть 4.xlsx']

  #for fname in [fnames[2]]:#
  for filej in range(len(fnames)):
    fname=fnames[filej]
#    if(fname in do_not_use):
#      continue
    i=add[filej]
    print('file %s..'%fname)
    wb = xlrd.open_workbook(path+fname)
    s=wb.sheet_by_index(0)

    LQs_files = s.col_values(0,1)
    LQs_cats = s.col_values(1,1)
    LQs_qs = s.col_values(2,1)
    LQs_datas = [s.row_values(j+1,3) for j in range(len(LQs_qs))]

#    qstr = ''
#    curcat = ''
#    for j in range(len(s.col_values(0))):
#      if(s.col_values(0+i)[j]==1):
#        curcat = qstr
##        if(len(qstr)==0):
##          curcat =
#        print('%s \\ %s'%(fname,curcat))
#      qstr = s.col_values(1+i)[j]
#      if(type(s.col_values(0+i)[j])==type(1.0)):
#  #      print('.')
#        tvs = s.row_values(j)[2+i:]
#        if(type(tvs[0])!=type(1.0)):
#          continue
#        while(1):
#          try:
#            tvs[tvs.index('')]=0
#          except:
#            break
#        try:
#          if(sum(tvs[0:-1])==0):
#            continue
#        except:
#          print(fname)
#          print(curcat)
#          print(qstr)
#          print(tvs)
#        # temp solution to exclude one set of doubles..
#        try:
#          print(qstr)
#        except:
#          qstr=qstr[:-1]
#        if(len(curcat)>0):
#          LQs_files.append(fname)
#          LQs_cats.append(curcat)
#          LQs_qs.append(qstr)
#          LQs_datas.append(tvs)



LQs_UniqCats_Dixs = {}
for j in range(len(LQs_cats)):
  try:
    LQs_UniqCats_Dixs[LQs_cats[j]].append(j)
  except:
    LQs_UniqCats_Dixs[LQs_cats[j]] = [j]

LQs_UniqCats = np.unique(LQs_UniqCats_Dixs.keys())
LQs_UniqCats_ixs = [LQs_UniqCats_Dixs[s] for s in LQs_UniqCats]


# Manually written
LQs_Cities = u'НОВОСИБИРСК	ТОМСК	ОМСК	КЕМЕРОВО	ВЛАДИВОСТОК	КАЛИНИНГРАД	СОЧИ	МУРМАНСК	САНКТ-ПЕТЕРБУРГ	МОСКВА'.split('\t')
LQs_engCities= ['Nsk','Tomsk','Omsk','Kemerovo','Vlad','Kalin','Sochi','Murmansk','Piter','Moscow']
LQs_CityPopulation= np.array([1548000,557179,1166092,544006,603244,448548,399673,299148,5132000,12111000])

LQs_Cities = [u'Москва',u'Санкт-Петербург',u'Новосибирск',u'Екатеринбург',u'Нижний Новгород',u'Казань',u'Самара',u'Челябинск',u'Омск',u'Ростов-на-Дону',u'Уфа',u'Красноярск',u'Пермь',u'Волгоград',u'Воронеж',u'Саратов',u'Краснодар',u'Тольятти',u'Тюмень',u'Ижевск',u'Барнаул',u'Ульяновск',u'Иркутск',u'Владивосток',u'Ярославль',u'Хабаровск',u'Махачкала',u'Оренбург',u'Томск',u'Новокузнецк',u'Кемерово',u'Астрахань',u'Рязань',u'Набережные',u'Челны',u'Пенза',u'Липецк',u'Тула',u'Киров',u'Чебоксары',u'Калининград',u'Курск',u'Улан-Удэ',u'Ставрополь',u'Магнитогорск',u'Брянск',u'Иваново',u'Тверь',u'Сочи' ,u'Белгород',u'Симферополь']
LQs_engCities=['MSK'   , 'SP'             ,    'NSK'     ,      'EKB'    , 'NN'             , 'KAZ'   , 'SAM'   , 'CHB'      , 'OMSK', 'RnD'           , 'UFA', 'KRAS'      , 'PER'  , 'VOL'      , 'VOR'    , 'SAR'    , 'KRD'      , 'TOL'     , 'TUM'   , 'IZH'   , 'BAR'    , 'UL'       , 'IRK'    , 'VLA'        , 'YAR'      , 'HAB'      , 'MAH'      , 'ORE'     , 'TOM'  , 'NOV'        , 'KEM'     , 'ASTR'     , 'RYAZ'  , 'NaCh'      , 'Che'  , 'PENZ' , 'LIPCK' , 'TULA', 'KIR'  , 'CHEB'     , 'KALIN'      , 'KUR'  , 'UU'      , 'STAV'      , 'MAGN'        , 'BRYA'  , 'IVA'    , 'TVER' , 'SOCHI', 'BELGRD'  , 'SIMF'       ]


LQs_Regions = array([u'Московская область',u'Ленинградская область',u'Новосибирская область',u'Свердловская область',u'Нижегородская область',u'Республика Татарстан',u'Самарская область',u'Челябинская область',u'Омская область',u'Ростовская область',u'Республика Башкортостан',u'Красноярский край',u'Пермский край',u'Волгородская область',u'Воронежская область',u'Саратовская область',u'Краснодарский край',u'Самарская область',u'Тюменская область',u'Удмуртская Республика',u'Алтайский край',u'Ульяновская область',u'Иркутская область',u'Приморский край',u'Ярославская область',u'Хабаровский край',u'Республика Дагестан',u'Оренбургская область',u'Томская область',u'Кемеровская область',u'Кемеровская область',u'Астраханская область',u'Рязанская область',u'Республика Татарстан',u'',u'Пензенская область',u'Липецкая область',u'Тульская область',u'Кировская область',u'Чувашская республика',u'Калининградская область',u'Курская область',u'Республика Бурятия',u'Ставропольский край',u'Челябинская область',u'Брянская область',u'Ивановская область',u'Тверская область',u'Краснодарский край',u'Белгородская область',u'Республика Крым'])

LQs_CityPopulation = np.array([11979530,5028000,1523801,1429400,1259921,1176187,1171598,1156201,1160670,1103733,1077719,1016385,1013887,1018790,1003500,839800,784048,719198,634171,632913,691149,615306,606137,600378,597750,593636,576194,556127,547989,549182,540095,527345,527795,519025,15978,519900,509098,493813,483176,464940,441376,428741,416079,412315,411880,410837,409075,408987,368011,373528,358037])

#LQs_RegionPopulation = np.array([2731176.00 ,1070128.00 ,1973876.00 ,2734075.00, 1938516.00,963128.00 ,5404273.00 ,771058.00 ,5131942.00 ,12108257.00])

use_cities = array([1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1])
use_cities = array([j  for j in range(len(use_cities))if (use_cities[j]==1)])

if(numdatas>0):
    LQs_D = np.array([a[:-numdatas] for a in LQs_datas])
else:
    LQs_D = np.array(LQs_datas)


#LQs_D = LQs_D[:,use_cities]
LQs_Cities = np.array(LQs_Cities)[use_cities]
LQs_engCities = np.array(LQs_engCities)[use_cities]
LQs_CityPopulation = np.array(LQs_CityPopulation)[use_cities]
LQs_Regions = LQs_Regions[use_cities]


LQs_UniqRegs = np.unique(LQs_Regions)
LQs_LQs_UniqRegs_ixs = [[j for j in range(len(LQs_Regions)) if LQs_Regions[j]==r ] for r in LQs_UniqRegs]


LQs_CM_D = array([np.sum(LQs_D[a,:],axis=0) for a in LQs_UniqCats_ixs])


# sum(scores) per city will be = 1
LQs_D_CityScoreNorm = LQs_D/np.sum(LQs_D,axis=0)

# Articles per person
LQs_D_CityPopulationNorm = LQs_D/LQs_CityPopulation
#LQs_D_RegionPopulationNorm = LQs_D/LQs_RegionPopulation
""" category-based means """

from numpy import array
LQs_CM_D_CityScoreNorm = array([np.sum(LQs_D_CityScoreNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])
LQs_CM_D_CityPopulationNorm = array([np.sum(LQs_D_CityPopulationNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])
#LQs_CM_D_RegionPopulationNorm = array([np.sum(LQs_D_RegionPopulationNorm[a,:],axis=0) for a in LQs_UniqCats_ixs])
#

""" """
#

