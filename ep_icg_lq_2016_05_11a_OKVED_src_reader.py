# -*- coding: utf-8 -*-
"""
Created on Wed May 11 18:13:07 2016

@author: ep
"""

"""      Imports      """

import glob
import os
import random
import xlrd


"""      Consts       """

rpath = 'src/OKVED'

files = glob.glob(rpath+'/'+'OKVED*')

""" Local Definitions """

def v(a):
    return [c.value for c in a]

#def load(fpath):
Rs=[]
for fpath in files[:]:
    print(fpath)
    fname = os.path.basename(fpath).replace('.xlsx','').replace('.xls','').replace('OKVED_','')
    wb = xlrd.open_workbook(fpath)
    if(len(wb.sheet_names())>1):
        raise ValueError('More than 1 list in file %s'%fpath)
    s = wb.sheet_by_index(0)
    testrowj = 1 if sum([c.ctype>0 for c in s.row(1)])>0 else 2
    testline = v(s.row(testrowj))
    testline_ctype = [c.ctype for c in s.row(testrowj)]
    keys=[]
    for c in s.row(testrowj):
        if(c.ctype==1):
            keys.append('cat')
        else:
            keys.append('No')
    if(keys[-1]=='cat'):
        keys[-1]='q'
    elif(keys[-2]=='cat'):
        keys[-2]='q'

    for rj in range(testrowj,len(s.col(0))):
        line = v(s.row(rj))
        if sum([c.ctype>0 for c in s.row(rj)])<2:
            continue
        R = {'fname':fname}
        for kj in range(len(keys)):
            if(keys[kj]=='cat')and(keys[kj] in R):
                R[keys[kj]]+='__'+line[kj]
            else:
                R[keys[kj]]=line[kj]
        Rs.append(R)
    rj = random.randint(0,len(Rs)-1)
    for k in Rs[rj]:
        print('  %3s : %s'%(k,Rs[rj][k]))

"""   Data Loading    """

import xlwt
w=xlwt.Workbook()

s=w.add_sheet('1')
s.write(0,0,'cat')
s.write(0,1,'q')
for j in range(len(Rs)):
    s.write(j+1,0,Rs[j]['cat'])
    s.write(j+1,1,Rs[j]['q'])


w.save('temp__OKVED_parsed.xls')

"""    Processing     """


"""      Saving       """



""" """
#
