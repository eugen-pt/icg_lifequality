# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 11:23:58 2015

@author: ep
"""

import lxml.html as html

list_fname = 'ep_icg_lq_symptomList_eurolab_2015_03_04a.txt'

try:
  with open(list_fname,'r') as f:
    LS = f.readlines()
    ep_icg_lq_symptomList_eurolab = [a[:-1] for a in LS]
  print('loaded from file')
except:
  adresses = ['http://www.eurolab.ua/symptoms/list/','http://www.eurolab.ua/symptoms/list/page/2/','http://www.eurolab.ua/symptoms/list/page/3']
  
  ep_icg_lq_symptomList_eurolab = []
  
  for adress in adresses:
    page = html.parse(adress)
    L=page.getroot().find_class('symptom-list__list__item__link')
    ep_icg_lq_symptomList_eurolab.extend([a.text.strip().replace('(','').replace(')','') for a in L])
    L=page.getroot().find_class('symptom-list__list__item_meta')
    ep_icg_lq_symptomList_eurolab.extend([a.text.strip().replace('(','').replace(')','') for a in L])
  
  with open(list_fname,'w') as f:
    for a in ep_icg_lq_symptomList_eurolab:
      f.write(a+'\n')
  