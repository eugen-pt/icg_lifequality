# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 10:47:54 2015

@author: ep
"""

# name: это строка которую транслитим
def transliterate(name):
   """
   Автор: LarsKort
   Дата: 16/07/2011; 1:05 GMT-4;
   Не претендую на "хорошесть" словарика. В моем случае и такой пойдет,
   вы всегда сможете добавить свои символы и даже слова. Только
   это нужно делать в обоих списках, иначе будет ошибка.
   """
   # Слоаврь с заменами
   slovar = {'а':'a','б':'b','в':'v','г':'g','д':'d','е':'e','ё':'e',
      'ж':'zh','з':'z','и':'i','й':'i','к':'k','л':'l','м':'m','н':'n',
      'о':'o','п':'p','р':'r','с':'s','т':'t','у':'u','ф':'f','х':'h',
      'ц':'c','ч':'cz','ш':'sh','щ':'scz','ъ':'','ы':'y','ь':'','э':'e',
      'ю':'u','я':'ja', 'А':'a','Б':'b','В':'v','Г':'g','Д':'d','Е':'e','Ё':'e',
      'Ж':'zh','З':'z','И':'i','Й':'i','К':'k','Л':'l','М':'m','Н':'n',
      'О':'o','П':'p','Р':'r','С':'s','Т':'t','У':'u','Ф':'F','Х':'Kh','х':'h',
      'Ц':'c','Ч':'cz','Ш':'sh','Щ':'scz','Ъ':'','Ы':'y','Ь':'','Э':'e',
      'Ю':'u','Я':'ja',',':'','?':'',' ':'_','~':'','!':'','@':'','#':'',
      '$':'','%':'','^':'','&':'','*':'','(':'',')':'','-':'-','=':'','+':'',
      ':':'',';':'','<':'','>':'','\'':'','"':'','\\':'','/':'','№':'',
      '[':'',']':'','{':'','}':'','ґ':'','ї':'', 'є':'','Ґ':'g','Ї':'i',
      'Є':'e'}
        
   # Циклически заменяем все буквы в строке
   for key in slovar:
      name = name.replace(key, slovar[key])
   return name
   
   
try:
    Data_nq_qrd
    Data_nq_qs_rd
except:    
    pass

from ep_icg_lq_2015_06_01a_YRS_S_11_resultsAgregator import *
  

  
""" """

#r_TScorrs = timeLagCorrs(Data_nq_rdN_rs_qd)    
#r_TScorrs = timeLagCorrs(Data_nq_rs_qd)   
 
#r_TScorrs = timeLagCorrs(Data_nq_qs_rd)    

#r_TScorrs = timeLagCorrs(Data_nq_dN_qs_rd,maxlag=10)    
r_TScorrs = timeLagCorrs(Data_nq_dN_rs_qd,maxlag=5)    

maxCorr_lag = np.zeros((shape(r_TScorrs)[0],shape(r_TScorrs)[0]))
maxCorr = maxCorr_lag*1.0
for j in range(shape(r_TScorrs)[0]):
    for i in range(shape(r_TScorrs)[0]):
        temp = abs(r_TScorrs[j,i,:])
        maxCorr_lag[j,i] = temp.argmax()
        maxCorr[j,i] = r_TScorrs[j,i,maxCorr_lag[j,i]]
toDraw = r_TScorrs


#figure()
#plot(maxCorr.flatten(),maxCorr.transpose().flatten(),'.')


outfname = 'ep_icg_lq_2015_06_01b__/ep_icg_lq_2015_06_01b__qsEff_T%.2f_lag%i.tsv'
for th in [0.25,0.33,0.5,0.75,0.9,0.95]:
    for lag in range(5):
        with open(outfname%(th,lag),'w') as f:
            f.write('q1\tq2\tmaxCorr_lag\r\n')
            for j in range(shape(maxCorr)[0]):
                for i in range(shape(maxCorr)[0]):
                    if(j==i):
                        continue
                    if(abs(maxCorr[j,i])>=th) and (maxCorr_lag[j,i]==lag):
                        if(abs(maxCorr[j,i])>abs(maxCorr[i,j])):
                            if(maxCorr[j,i]>0):
                                stype='pos'
                            else:
                                stype='neg'
                            f.write('%s\t%s\t%s\r\n'%(usqs_nq[j].replace(' ','_'),usqs_nq[i].replace(' ','_'),stype))
                

if(0):
    figure()
    plt.imshow(r_TScorrs[:,:,0],interpolation='nearest')
    if(shape(r_TScorrs)[0] ==len(usRegion)):
        tickStuff = usRegion
    else:
        tickStuff = usqs    
    plt.yticks(range(shape(r_TScorrs)[0]),map(transliterate,tickStuff))
    plt.xticks(range(shape(r_TScorrs)[0]),map(transliterate,tickStuff),rotation=-90)
        

if(0):
    figure()
    for tj in range(shape(toDraw)[2]):
        subplot(1,shape(toDraw)[2],tj+1)
        plt.imshow(toDraw[:,:,tj],interpolation='nearest')
        plt.colorbar()
        title('dt=%i'%tj)
        

""" """
#