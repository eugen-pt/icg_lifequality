# -*- coding: utf-8 -*-
"""
Created on Wed May  4 14:59:06 2016

@author: ep
"""

"""      Consts       """


experiment = '2_0_0_test0_2016_05_04a'



"""      Imports      """
import datetime


from ep_icg_lq_2016_01_11c_countries_load import *
# for cities list
from ep_icg_lq_2016_04_28b__Ya__regionslist import *

from   ep_lq_2016_04_27a__WTM___interface import *


""" Local Definitions """
def getDates(start='01.01.2000',end='31.03.2016',each_m=3):
    if not (end):
        end = datetime.datetime.now().strftime('%d.%m.%Y')
    [start_date,end_date] = [datetime.date(tx[2],tx[1],tx[0]) for tx in  [map(int,re.findall('([0-9]+)',a)) for a in [start,end]] ]

    R_dates = []
    while(start_date<end_date):
        hstart_date = start_date
        start_date = datetime.date(hstart_date.year+1*(hstart_date.month+each_m>12),hstart_date.month+each_m if (hstart_date.month+each_m<=12) else hstart_date.month+each_m-12,1);
        hend_date = start_date-datetime.timedelta(1)
        R_dates.append((hstart_date.strftime('%d.%m.%Y'),hend_date.strftime('%d.%m.%Y')))
    return R_dates

""" """

source_dates = getDates(start='01.01.2014',end='31.12.2015',each_m=1)


"""
Негативная окраска
"""
neg_addss = [u'плохо',u'хуже',u'ужасно',u'кошмар']
"""
Позитивная окраска
"""
pos_addss = [u'хорошо',u'замечательно',u'отлично',u'лучше',u'чудесно']


posneg_adds = {1:' && '+'('+' | '.join(pos_addss)+')'+''.join([' -%s'%s for s in neg_addss]),-1:' && '+'('+' | '.join(neg_addss)+')'+''.join([' -%s'%s for s in pos_addss])}
"""   Data Loading    """

LQs = []
for cj in range(len(countries)):
    s = countries[cj]
    for tdates in source_dates:
        for posneg in posneg_adds:
            hadds = '('+' | '.join(posneg_adds[posneg])+')'
            tq = dict( \
                      dates=tdates,
                      city=u'Россия',
                      q = s+posneg_adds[posneg],
                      country = s,
                      country_num= countries_nums[cj],
                      posneg = posneg,
                      adds=posneg_adds[posneg],
            )
            LQs.append(tq)


"""    Processing     """
for LQj in range(len(LQs)):
    print('%i/%i'%(LQj,len(LQs)))
    while(1):
        try:
            WTM_addTask(LQs[LQj],experiment)
            break
        except:
            pass
#    break


#WTM_addTasks(LQs,experiment)

"""      Saving       """



""" """
#
