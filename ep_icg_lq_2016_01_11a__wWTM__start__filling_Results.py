# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 15:41:23 2016

@author: ep
"""

"""      Imports      """

import requests

from datetime import datetime


import json
#r = requests.post("http://127.0.0.1:8000/YaQs/add_task/", data=dict(q = 'test',
#                    date_from = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    date_to = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    region_int = int(123),
#                    region_string = 'Russ',
#                    url = 'http://test.com',
#                    parsed = 0,
#                    date_parsed = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    n_docs =  int(-153),
#                    experiment = 'temp',
#                    ))

#r = requests.post("http://127.0.0.1:8000/YaQs/add/", data=dict(q = 'test',
#                    date_from = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    date_to = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    region_int = int(123),
#                    region_string = 'Russ',
#                    url = 'http://test.com',
#                    parsed = 0,
#                    date_parsed = datetime.strptime('2015-04-16','%Y-%m-%d').date(),
#                    n_docs =  int(-153),
#                    experiment = 'temp',
#                    ))
#



outfname = '2015-12-21_12-35-05__v13__KNA_wtf_wtF__pagesC_percity.csv'; mode = 'Russia_Os_manyminuses'

""" Local Definitions """

def redoDate(s):
    if('/' in s):
        r = s.split('/')
        return '-'.join([r[2],r[0],r[1]])        
    elif('.' in s):    
        r = s.split('.')
        if(0):
            r.reverse()
            return '-'.join(r)
        else:    
            return '%s/%s/%s'%(r[1],r[0],r[2])

with open(outfname,'r') as f:
    LS = f.readlines()

#LS = array([unicode(L).replace('\r\n','').split(',') for L in LS[1:]])
LS = array([L.decode('utf-8').replace('\r\n','').split(',') for L in LS[1:]])
LS[:,-3] = map(redoDate,LS[:,-3])
LS[:,-2] = map(redoDate,LS[:,-2])


Regions = LS[:,3]

Qs = [L for L in LS[:,2]]


PosNegs = LS[:,4]

AddS = LS[:,5]

Dates_from = LS[:,-3]
Dates_to = LS[:,-2]

NPages = map(int,LS[:,-1])


"""   Data Loading    """

try:
    J_Done
except:
    J_Done = {}

for j in range(len(Qs)):
    if(j in J_Done):
        continue
    r = requests.post("http://127.0.0.1:80/YaQs/add/", data=dict(q = Qs[j]+' && '+AddS[j],
                        date_from = datetime.strptime(redoDate(Dates_from[j]),'%Y-%m-%d').date(),
                        date_to = datetime.strptime(redoDate(Dates_to[j]),'%Y-%m-%d').date(),
                        region_int = int(225),
                        region_string = Regions[j],
                        url = '',
                        parsed = 1,
                        date_parsed = datetime.strptime('2015-12-21','%Y-%m-%d').date(),
                        n_docs =  NPages[j],
                        experiment = mode,
                        add_info = json.dumps({'adds':AddS[j],'q':Qs[j],'posneg':PosNegs[j]}),
                        ))
    if(r.status_code!=201)       :
        break                
    J_Done[j]=1
    #break


"""    Processing     """


"""      Saving       """



""" """
#
