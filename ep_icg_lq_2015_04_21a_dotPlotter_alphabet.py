# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:18:45 2015

@author: ep
"""

import matplotlib.pyplot as plt

try:
    Data
    catCorrMx_nq
    shortCats
except:    

    from ep_icg_lq_2015_04_20c_data_loader import *


""" """

#from sklearn.metrics.cluster import normalized_mutual_info_score

#http://minepy.sourceforge.net/docs/1.0.0/python.html
from minepy import MINE


outpath = Data_fname.replace('.xls','_ab')
import os
try:
    os.mkdir(outpath)
except:
    pass    

nums = uCatNums_nq[0]
MAX = len(nums)
MICs = np.zeros((MAX,MAX))
figure()
for jj in range(MAX):
    j = nums[jj]
    for ii in range(MAX):
        if(ii==jj):
          continue
        i=nums[ii]
        a = MINE()
        a.compute_score(nData[j],nData[i])
        mic =    a.mic()     
                
        MICs[jj,ii] = mic
        
        corr = corrcoef(nData[j],nData[i])[0,1]
        
        cs = 1 - ((3.0/4)* ((1-mic)**2) + (1.0/4)*(corr**2))
        fname = '%s_%s_cs%.2f_mic%.2f_corr%.2f.png'%(Qs[j],Qs[i],cs,MICs[jj,ii],corr )
        
        plot(nData[j],nData[i],'.')
        plt.xlabel(Qs[j])
        plt.ylabel(Qs[i])
        for cj in range(len(Cities)):
            plt.text(nData[j][cj],nData[i][cj],Cities[cj])
       
        plt.xticks([])
        plt.yticks([])
        plt.savefig(outpath+os.sep+fname)
        plt.cla()

close(plt.gcf())            
#a.

aaa
""" """



tn1 = np.random.randint(0,len(uCatNums_nq[0]))
tn2 = np.random.randint(0,len(uCatNums_nq[0]))

plt.xlabel(Qs_nq[uCatNums_nq[0][tn1]])
plt.ylabel(Qs_nq[uCatNums_nq[0][tn2]])
plot(nData_nq[uCatNums_nq[0][tn1]],nData_nq[uCatNums_nq[0][tn2]],'.')
for cj in range(len(Cities)):
    plt.text(nData_nq[uCatNums_nq[0][tn1]][cj],nData_nq[uCatNums_nq[0][tn2]][cj],Cities[cj])




""" """
#