# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 10:31:30 2015

@author: root
"""

import xlrd
from numpy import *


""" """

xlspath = 'ep_icg_lq_2015_11_16c_pagescount___Data_expN_qrd.xls'

w = xlrd.open_workbook(xlspath)
s = w.sheet_by_name('N')

qs = [c.value for c in s.col(0)[1:]]
Nqs = len(qs)
dates = [c.value for c in s.row(0)[1:]]
Data = [[c.value for c in s.row(j+1)[1:]] for j in range(Nqs)]

s = w.sheet_by_name('N_corrMx_linearize')
lags = [c.value for c in s.col(0)[1:]]
temp_cells = [c.value for c in s.row(0)[1:]]
lagCorrMxs = array([ reshape([c.value for c in s.row(j+1)[1:]],(Nqs,Nqs)) for j in range(len(lags))])

""" 

outputs:

qs = what was searched
Nqs = number of different qs
dates = different dates used as datapoints (as text..)
Data = normalized data (if 'expN' in xlspath - then the data is normed by exponential trend)

lags = different lags

lagCorrMxs = correlation matrix with lag, lag is the first index, reason is the second index, consequence is the third one. 
                i.e. lagCorrMxs[2,3,4] gives correlation at lag=2 of effect of qs[3] on qs[4]   (qs[3]->qs[4])

"""

J = {'qs':qs,'dates':dates,'Data':Data,'lags':lags,'lagCorrMxs':lagCorrMxs.tolist()}
import json 
with open(xlspath.replace('.xls','.json'),'w') as f:
    json.dump(J,f)

""" """
#