# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 11:55:46 2016

Had to manually changed the noemo result file -> header was wrong

@author: ep
"""

"""      Imports      """


"""      Consts       """

xlsfname = '2016_04_18b__2__.xls'

do_emo = 0

if(do_emo):
    outfname = '2016-04-08_17-25-48__v13b__KNA_wtf_wtF__pagesC_percity___ALLTOGETHER_noOs.csv'
    xlsfname=xlsfname.replace('.xls','emo.xls')
else:
    outfname =     '2016-04-08_12-59-10__v13b__KNA_wtf_wtF__pagesC_percity_ep__ALLTOGETHER.csv'      
    xlsfname=xlsfname.replace('.xls','noemo.xls')


posneg_key = u"""0 Инициатива
3 Необходимо
4 Отказаться
5 хорошо
6 плохо"""
posneg_key = {int(a[0]):a[1] for a in  [s.split(' ') for s in posneg_key.split('\n')]}
""" Local Definitions """


def unique(a):
    return list(set(a))
def redoDate(s):
    t=s.split('.')
    return t[2]+'-'+t[1]+'-'+t[0]
 
def iD(A):
    return {A[j]:j for j in range(len(A))}
def iDs(A):
    R = {}
    for j in range(len(A)):
        try:
            R[A[j]]=R[A[j]].union([j])
        except:
            R[A[j]] = set([j])
    return R
def fit_exp_linear(t, y, C=0):
    y = y - C
    y = np.log(y)
    K, A_log = np.polyfit(t, y, 1)
    A = np.exp(A_log)
    return A, K


def RecursiveDictGen(list_of_keys):            
    if(len(list_of_keys)==1):
        return {a:0 for a in list_of_keys[0]}
    else:
        return {a:RecursiveDictGen(list_of_keys[1:]) for a in list_of_keys[0]}
"""   Data Loading    """

try:
    LS
    Rd
    loaded_do_emo
    
    #Res_iDs
    
    Res_DD
    Rdyn
except:  
    with open(outfname,'r') as f:
        LS = f.readlines()
        
    LS = [s.decode('utf8') for s in LS]

    LS = [s.replace('\r\n','').split(',') for s in LS]
    
    header = LS[0]
    if(do_emo==0):
        header = 'original file,category,q,city,date_from,date_to,Res'.split(',')
    LS = LS[1:]
    
    #if(1):
    intVals = ['posneg','Res']

    Rd = []
    for L in LS:
        R = {}
        for j in range(len(header)):
            R[header[j]] = L[j]
            if(header[j] in intVals):
                R[header[j]] = int(L[j])
        Rd.append(R)
        
    loaded_do_emo = do_emo




    #Res_iDs = {h:iDs([d[h] for d in Rd]) for h in header}

    """    Processing     """
    
    # uV = uniqueValues
    try:
        uV = {h:Res_iDs[h].keys() for h in header}
    except:
        uV = {}
        
        for h in header:
            uV[h] = unique([d[h] for d in Rd])
    
    print('Correct dates sorting..')
    uV['date_from'] = [uV['date_from'][j] for j in argsort([redoDate(s) for s in uV['date_from']])]
    
    print('filtering incomplete cities..')
    
    city_NSamples = {city:0 for city in uV['city']}
    for R in Rd:
        city_NSamples[R['city']] = city_NSamples[R['city']]+1
    tmax = max(city_NSamples.values())    
    while(sum(array(city_NSamples.values())>=tmax)<82):
        tmax=tmax-1
        #raise ValueError('Not 82 but %i cities with maximum number of samples!'%sum(array(city_NSamples.values())==7670))
    
    uV['city'] =  [k for k in city_NSamples if city_NSamples[k] >= tmax]   
    
#if(1):
    print('  deleting cities..')
    j=0
    while(j<len(Rd)):
        for h in header:
            if(Rd[j][h] not in uV[h]):
                del Rd[j]
                j=j-1
                break
        j=j+1
        
    """             """
    
    for k in ['city','q']:
        uV[k] = list(sort(uV[k]))
        
    """ """
    
    print('makign Result Dict of Dicts..')
    if(do_emo):
        Res_DD = RecursiveDictGen([uV[k] for k in ['q','posneg','city','date_from']])
        for R in Rd:
            Res_DD[R['q']][R['posneg']][R['city']][R['date_from']] = R['Res']
    else:
        Res_DD = RecursiveDictGen([uV[k] for k in ['q','city','date_from']])
        for R in Rd:
            Res_DD[R['q']][R['city']][R['date_from']] = R['Res']
    
    
    print('makign overall Dynamic..')
    
    uDates = uV['date_from']
    uDates_iD = iD(uDates)
    
    Rdyn = [0 for j in uDates]
    RN = [0 for j in uDates]
    
    for R in Rd:
        RN[uDates_iD[R['date_from']]]=    RN[uDates_iD[R['date_from']]]+1
        Rdyn[uDates_iD[R['date_from']]] = Rdyn[uDates_iD[R['date_from']]] + R['Res']
    
    Rdyn = [Rdyn[j]*1.0/RN[j] for j in range(len(RN))]

t = arange(len(uV['date_from']))
A,K = fit_exp_linear(t,array(Rdyn))
expRdyn = A*exp(K*t)

if(1):
    figure()    
    plot(t,Rdyn,'-b',t,expRdyn,'-r')
    plt.xticks(arange(len(Rdyn)),uV['date_from'],rotation=-30)
    

city_date_Rdyn = RecursiveDictGen([uV['city'],uV['date_from']])
city_date_RN = RecursiveDictGen([uV['city'],uV['date_from']])
    
for R in Rd:
   city_date_Rdyn[R['city']][R['date_from']] = city_date_Rdyn[R['city']][R['date_from']]+R['Res'] 
   city_date_RN[R['city']][R['date_from']] = city_date_RN[R['city']][R['date_from']] + 1

for city in uV['city']:
    for date_from in uV['date_from']:
        city_date_Rdyn[city][date_from] = city_date_Rdyn[city][date_from]*1.0/city_date_RN[city][date_from]
        
        
if(do_emo):
    qcd_Rdyn = RecursiveDictGen([uV['q'],uV['city'],uV['date_from']])        
    qcd_RN = RecursiveDictGen([uV['q'],uV['city'],uV['date_from']])        
    for R in Rd:
       qcd_Rdyn[R['q']][R['city']][R['date_from']] = qcd_Rdyn[R['q']][R['city']][R['date_from']]+R['Res'] 
       qcd_RN[R['q']][R['city']][R['date_from']] = qcd_RN[R['q']][R['city']][R['date_from']] + 1
    
    for q in uV['q']:     
         for city in uV['city']:
            for date_from in uV['date_from']:
                qcd_Rdyn[q][city][date_from] = qcd_Rdyn[q][city][date_from]*1.0/qcd_RN[q][city][date_from]
    
""" work0 - q,city * dates """
print('Writing q,city * date')

import xlwt
w = xlwt.Workbook()
if(do_emo==0):
    s = w.add_sheet('q,city_x_dates')
    s.write(0,0,'q,city')
    s.write(0,1,'q')
    s.write(0,2,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+3,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            rn=rn+1
            s.write(rn,0,q+','+city)
            s.write(rn,1,q)
            s.write(rn,2,city)
            for cj in range(len(uV['date_from'])):
                s.write(rn,cj+3,Res_DD[q][city][uV['date_from'][cj]])

    s = w.add_sheet('q,city_x_dates__sumNorm')
    s.write(0,0,'q,city')
    s.write(0,1,'q')
    s.write(0,2,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+3,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            rn=rn+1
            s.write(rn,0,q+','+city)
            s.write(rn,1,q)
            s.write(rn,2,city)
            for cj in range(len(uV['date_from'])):
                s.write(rn,cj+3,(Res_DD[q][city][uV['date_from'][cj]]*1.0/Rdyn[cj]))

    s = w.add_sheet('q,city_x_dates__expNorm')
    s.write(0,0,'q,city')
    s.write(0,1,'q')
    s.write(0,2,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+3,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            rn=rn+1
            s.write(rn,0,q+','+city)
            s.write(rn,1,q)
            s.write(rn,2,city)
            for cj in range(len(uV['date_from'])):
                s.write(rn,cj+3,(Res_DD[q][city][uV['date_from'][cj]]/expRdyn[cj]))

    s = w.add_sheet('q,c_x_d__citydate_sumN')
    s.write(0,0,'q,city')
    s.write(0,1,'q')
    s.write(0,2,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+3,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            rn=rn+1
            s.write(rn,0,q+','+city)
            s.write(rn,1,q)
            s.write(rn,2,city)
            for cj in range(len(uV['date_from'])):
                s.write(rn,cj+3,(Res_DD[q][city][uV['date_from'][cj]]/city_date_Rdyn[city][uV['date_from'][cj]]))

else: 
    s = w.add_sheet('q,pn,city_x_dates')
    s.write(0,0,'q,posneg,city')
    s.write(0,1,'q')
    s.write(0,2,'posneg')
    s.write(0,3,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+4,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            for pn in uV['posneg']:
                rn=rn+1
                s.write(rn,0,q+','+city)
                s.write(rn,1,q)
                s.write(rn,2,posneg_key[pn])
                s.write(rn,3,city)
                for cj in range(len(uV['date_from'])):
                    s.write(rn,cj+4,Res_DD[q][pn][city][uV['date_from'][cj]])
    
    
    s = w.add_sheet('q,pn,city_x_dates__sumN')
    s.write(0,0,'q,posneg,city')
    s.write(0,1,'q')
    s.write(0,2,'posneg')
    s.write(0,3,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+4,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            for pn in uV['posneg']:
                rn=rn+1
                s.write(rn,0,q+','+city)
                s.write(rn,1,q)
                s.write(rn,2,posneg_key[pn])
                s.write(rn,3,city)
                for cj in range(len(uV['date_from'])):
                    s.write(rn,cj+4,(Res_DD[q][pn][city][uV['date_from'][cj]]*1.0/Rdyn[cj]))


    s = w.add_sheet('q,pn,city_x_dates__expN')
    s.write(0,0,'q,posneg,city')
    s.write(0,1,'q')
    s.write(0,2,'posneg')
    s.write(0,3,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+4,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            for pn in uV['posneg']:
                rn=rn+1
                s.write(rn,0,q+','+posneg_key[pn]+','+city)
                s.write(rn,1,q)
                s.write(rn,2,posneg_key[pn])
                s.write(rn,3,city)
                for cj in range(len(uV['date_from'])):
                    s.write(rn,cj+4,(Res_DD[q][pn][city][uV['date_from'][cj]]/expRdyn[cj]))


    s = w.add_sheet('q,pn,c_x_d__citydate_sN')
    s.write(0,0,'q,posneg,city')
    s.write(0,1,'q')
    s.write(0,2,'posneg')
    s.write(0,3,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+4,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            for pn in uV['posneg']:
                rn=rn+1
                s.write(rn,0,q+','+posneg_key[pn]+','+city)
                s.write(rn,1,q)
                s.write(rn,2,posneg_key[pn])
                s.write(rn,3,city)
                for cj in range(len(uV['date_from'])):
                    s.write(rn,cj+4,(Res_DD[q][pn][city][uV['date_from'][cj]]/city_date_Rdyn[city][uV['date_from'][cj]]))

    s = w.add_sheet('q,pn,c_x_d__Qcitydate_sN')
    s.write(0,0,'q,posneg,city')
    s.write(0,1,'q')
    s.write(0,2,'posneg')
    s.write(0,3,'city')
    for cj in range(len(uV['date_from'])):
        s.write(0,cj+4,uV['date_from'][cj])
    #    s.append(['']+uV['date_from'])
    rn=0
    for city in uV['city']:
        for q in uV['q']:
            for pn in uV['posneg']:
                rn=rn+1
                s.write(rn,0,q+','+posneg_key[pn]+','+city)
                s.write(rn,1,q)
                s.write(rn,2,posneg_key[pn])
                s.write(rn,3,city)
                for cj in range(len(uV['date_from'])):
                    s.write(rn,cj+4,(Res_DD[q][pn][city][uV['date_from'][cj]]/qcd_Rdyn[q][city][uV['date_from'][cj]]))
    
print('saving to %s'%xlsfname)
w.save(xlsfname)


print('Finished.')    
""" """
aaa

# manually look for any outcasts

for q in uV['q']:
    for pn in uV['posneg']:
        for city in uV['city']:
            for d in uV['date_from']:
                if(Res_DD[q][pn][city][d]>1000000):
                    for s in [q,pn,city,d,Res_DD[q][pn][city][d]]:print(s)

aaa

# just to look at the histogram

a=[]
for q in uV['q']:
    for pn in uV['posneg']:
        for city in uV['city']:
            a.append(Res_DD[q][pn][city][uV['date_from'][0]]*1.0/city_date_Rdyn[city][uV['date_from'][0]])

aaa


xlsfname = '2016_04_18b__.xls'


aaa

""" """
#
