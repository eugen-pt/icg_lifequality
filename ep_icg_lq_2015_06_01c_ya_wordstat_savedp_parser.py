# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:40:59 2015

@author: ep
"""

def st(x):
    return (x-mean(x))/std(x)      

#aaa

from ep_icg_lq_2015_06_01a_YRS_S_11_resultsAgregator import *

#def redoDate(s):
#    r = s.split('.')
#    r.reverse()
#    return '-'.join(r)


import re

import lxml.html as html
import glob
import json 

path = './src/5_fursenko/'

files = glob.glob(path+'*.htm*')

try:
    req_dates 
    req_absCounts 
    req_relCounts 
    
    req_regions 
    req_qs 
except:    
    req_dates = []
    req_absCounts = []
    req_relCounts = []
    
    req_regions = []
    req_qs = []
    for fpath in files:
    
        P = html.parse(fpath)
        R = P.getroot()
        
        hregion = R.find_class('b-search__region-select-td')[0].find('div/span/span').text.replace('н и ','нская ').replace('нь и ','нская ').replace('о и ','ская ').replace(' и ','ая ')
        
        hq = R.find_class('b-form-input__box')[0].find('input').value.replace('нейродегенеративные заболевания','нейро-дегенеративные заболевания').replace('фундаментальные','монументальные')
        
        
        try:
            D = json.loads(R.find_class('b-chart b-history__chart i-bem b-chart_js_inited')[0].attrib['onclick'].replace('return ',''))['b-chart']['data']
            
            hdates = [d['dateFrom'] for d in D]
            habsCounts = [int(d['totalCount']) for d in D]
            hrelCounts = [float(d['relCount']) for d in D]
            print(''),
        except:
    #        raise Vl
            print('!!--'),
            hdates        
            habsCounts = [0 for a in hdates]
            hrelCounts = [0.0 for a in hdates]
        
        print(hregion),
        print(' | '+hq)
        req_dates.append(map(redoDate,hdates))
        req_absCounts.append(habsCounts)
        req_relCounts.append(hrelCounts)
        req_regions.append(hregion)
        req_qs.append(hq)
    

req_uqs = unique(req_qs)
req_usqs = sort([qs_sqs_D[s] for s in req_uqs])
req_usqs_iD = iD(req_usqs)

req_udates = req_dates[0]#[8:]
req_udates_iD = iD(req_udates)

req_uRegions = unique(req_regions)
req_uRegions_iD = iD(req_uRegions)


req_Data_qrd = np.zeros((len(req_uqs),len(req_uRegions),len(req_udates)),'int')
req_Data_rel_qrd = np.zeros((len(req_uqs),len(req_uRegions),len(req_udates)),'float')

for J in range(len(req_qs)):
    req_q = req_qs[J]
    req_r = req_regions[J]
    for dj in range(len(req_dates[J])):
        req_d = req_dates[J][dj]
        if(req_q in qs_sqs_D) and (qs_sqs_D[req_q] in req_usqs_iD) and (req_r in req_uRegions_iD) and (req_d in req_udates_iD):
            req_Data_qrd[req_usqs_iD[qs_sqs_D[req_q]],req_uRegions_iD[req_r],req_udates_iD[req_d]] = req_absCounts[J][dj]
            req_Data_rel_qrd[req_usqs_iD[qs_sqs_D[req_q]],req_uRegions_iD[req_r],req_udates_iD[req_d]] = req_relCounts[J][dj]


req_Data_qs_rs_d = 1.0*sum(sum(req_Data_qrd,axis=1),axis=0)
req_Data_rel_qs_rs_d = sum(sum(req_Data_rel_qrd,axis=1),axis=0)

req_Data_dN_qrd = np.zeros(shape(req_Data_qrd),'double')
for qj in range(shape(req_Data_qrd)[0]):
    for rj in range(shape(req_Data_qrd)[1]):
        req_Data_dN_qrd[qj,rj,:] = req_Data_qrd[qj,rj,:]*1.0/req_Data_qs_rs_d
        
req_Data_rel_dN_qrd = req_Data_rel_qrd*1.0;
for qj in range(shape(req_Data_qrd)[0]):
    for rj in range(shape(req_Data_qrd)[1]):
        req_Data_rel_dN_qrd[qj,rj,:] = req_Data_rel_qrd[qj,rj,:]/req_Data_rel_qs_rs_d
"""
J_ = joined requests and pages
"""

J_uqs    = usqs_nq.tolist() + ['req_'+s for s in req_usqs]
J_udates = req_dates[0][8:]
J_uRegions = req_uRegions

J_uqs_iD = iD(J_uqs)
J_udates_iD = iD(J_udates)
J_uRegions_iD = iD(J_uRegions)

J_Data = np.zeros((len(J_uqs),len(J_uRegions),len(J_udates)))

for qj in range(len(usqs_nq)):
    for rj in range(len(usRegion)):
        for dj in range(len(udates)):
            if(usqs_nq[qj] in J_uqs_iD)and(usRegion[rj] in J_uRegions_iD)and(udates[dj] in J_udates_iD):
                J_Data[J_uqs_iD[usqs_nq[qj]],J_uRegions_iD[usRegion[rj]],J_udates_iD[udates[dj]]] = Data_nq_dN_qrd[qj,rj,dj]
#                print('.'),

for qj in range(len(req_usqs)):
    for rj in range(len(req_uRegions)):
        for dj in range(len(req_udates)):
            if('req_'+req_usqs[qj] in J_uqs_iD)and(req_uRegions[rj] in J_uRegions_iD)and(req_udates[dj] in J_udates_iD):
                J_Data[J_uqs_iD['req_'+req_usqs[qj]],J_uRegions_iD[req_uRegions[rj]],J_udates_iD[req_udates[dj]]] = req_Data_dN_qrd[qj,rj,dj]
#                print('.'),



J_Data_rs_qd = sum(J_Data,axis=1)
J_Data_qs_rd = sum(J_Data,axis=0)

""" """

r_TScorrs = timeLagCorrs(J_Data_rs_qd,maxlag=5)    

maxCorr_lag = np.zeros((shape(r_TScorrs)[0],shape(r_TScorrs)[0]))
maxCorr = maxCorr_lag*1.0
for j in range(shape(r_TScorrs)[0]):
    for i in range(shape(r_TScorrs)[0]):
        temp = abs(r_TScorrs[j,i,:])
        maxCorr_lag[j,i] = temp.argmax()
        maxCorr[j,i] = r_TScorrs[j,i,maxCorr_lag[j,i]]
toDraw = r_TScorrs

outfname = 'ep_icg_lq_2015_06_01b__/ep_icg_lq_2015_06_01c_Joined__T%.2f_lag%s.tsv'
for th in [0.5,0.75,0.9,0.95]:
    for lags in [[j] for j in range(5)]+[[1,2],[1,2,3,4,5]]:
        with open(outfname%(th,''.join(map(str,lags))),'w') as f:
            f.write('q1\tq2\tmaxCorr_lag\r\n')
            for j in range(shape(maxCorr)[0]):
                for i in range(shape(maxCorr)[0]):
                    if(j==i):
                        continue
                    if(abs(maxCorr[j,i])>=th) and (maxCorr_lag[j,i] in lags):
                        if(abs(maxCorr[j,i])>abs(maxCorr[i,j])):
                            if(maxCorr[j,i]>0):
                                stype='pos'
                            else:
                                stype='neg'
                            f.write('%s\t%s\t%s\r\n'%(J_uqs[j].replace(' ','_'),J_uqs[i].replace(' ','_'),stype))


outfname = 'ep_icg_lq_2015_06_01b__/ep_icg_lq_2015_06_02a_Joined__corrs_lag%i.csv'
for lag in range(5):
  with open(outfname%lag,'w') as f:
    f.write(','.join(['']+J_uqs)+'\r\n')
    for qj in range(len(J_uqs)):
      f.write(','.join([J_uqs[qj]]+map(str,r_TScorrs[qj,:,lag]))+'\r\n')
      
outfname = 'ep_icg_lq_2015_06_01b__/ep_icg_lq_2015_06_02b_Joined__corrsT%.2f_lag%i.csv'
for th in [0.5,0.75,0.9,0.95]:
  for lag in range(5):
    with open(outfname%(th,lag),'w') as f:
      f.write(','.join(['']+J_uqs)+'\r\n')
      for qj in range(len(J_uqs)):
        f.write(','.join([J_uqs[qj]]+map(str,[ (x/abs(x))*(abs(x)>th) for x in r_TScorrs[qj,:,lag]]))+'\r\n')

outfname = 'ep_icg_lq_2015_06_01b__/ep_icg_lq_2015_06_02b_Joined__forCytoS_corrsT%.2f_lag%i.tsv'
for th in [0.5,0.75,0.9,0.95]:
  for lag in range(5):
    with open(outfname%(th,lag),'w') as f:
      f.write('q1\tq2\tstype\r\n')
      for qj in range(len(J_uqs)):
        for qi in range(len(J_uqs)):
          if(abs(r_TScorrs[qj,qi,lag])<th):
            continue
          if(r_TScorrs[qj,qi,lag]>0):
              stype='pos'
          else:
              stype='neg'
          
          f.write('%s\t%s\t%s\r\n'%(J_uqs[qj].replace(' ','_'),J_uqs[qi].replace(' ','_'),stype))
  


if(1):
    figure()
    
    ns = [0,70]
    
    subplot(1,2,1)
    t = arange(len(J_Data_rs_qd[ns[0]]))
    plot(t,st(J_Data_rs_qd[ns[0]]),t,st(J_Data_rs_qd[ns[1]]))
    legend([J_uqs[n] for n in ns])
    title('not shifted , corr=%f'%r_TScorrs[ns[0],ns[1],0])
    
    subplot(1,2,2)
    plot(t,st(J_Data_rs_qd[ns[0]]),t-maxCorr_lag[ns[0],ns[1]],st(J_Data_rs_qd[ns[1]]))
    legend([J_uqs[n] for n in ns])
    title('shifted %i monthes ( ;) ) , corr=%f'%(maxCorr_lag[ns[0],ns[1]],r_TScorrs[ns[0],ns[1],maxCorr_lag[ns[0],ns[1]]]))
    
    

""" """


if(0):
    import xlwt
    
    Ds = [req_Data_qrd,req_Data_rel_qrd]
    fnname_adds = ['req_Data_qrd','req_Data_rel_qrd']
    for Dj in range(len(Ds)):
        wb = xlwt.Workbook()
        D = Ds[Dj]
        
        for rj in range(len(req_uRegions)):
            s = wb.add_sheet(req_uRegions[rj])
            s.write(0,0,'')
            for qj in range(len(req_usqs)):
                s.write(qj+1,0,req_usqs[qj])
            for dj in range(len(req_udates)) :
                s.write(0,dj+1,req_udates[dj])
                for qj in range(len(req_usqs)):
                    s.write(qj+1,dj+1,D[qj,rj,dj])
        wb.save('ep_icg_lq_2015_06_01c__'+fnname_adds[Dj]+'.xls')
        
        
#
#for s in req_uqs:
#    print(s)
#    qs_sqs_D[s]


#rows = R.find_class('even')+R.find_class('odd')
#tds = [r.findall('td') for r in rows]
#
#dates = [td[0].text.split() for td in tds]
#dates = [map(redoDate,[a[0],a[2]]) for a in dates]
#
#tix = argsort([a[0] for a in dates])
#
#tds = [tds[j] for j in tix]
#dates = [dates[j] for j in tix]



#dates = [''.join(s.split()) for s in dates]
""" """
#