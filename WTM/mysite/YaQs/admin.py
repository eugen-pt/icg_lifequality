from django.contrib import admin

from .models import YaQ,YaQ_task,Worker,WorkerSession

admin.site.register(YaQ)
admin.site.register(YaQ_task)
admin.site.register(Worker)
admin.site.register(WorkerSession)

