# -*- coding: utf-8 -*-
# coding=utf8
from __future__ import unicode_literals

from django.db import models

# Create your models here.

import datetime

class YaQ_task(models.Model):

    q = models.CharField(max_length=255,default=u'запрос по-умолчанию')

    experiment = models.CharField(max_length=255,default='default_exp')

    url = models.CharField(max_length=511,default='www.ya.ru')

    date_from = models.DateField(default = datetime.date.today)
    date_to = models.DateField(default = datetime.date.today)
    region_string = models.CharField(max_length=255,default = u'Россия')
    region_int = models.IntegerField(default = 225)    

class YaQ(models.Model):
 
    q = models.CharField(max_length=255,default=u'запрос по-умолчанию')

    experiment = models.CharField(max_length=255,default='default_exp')

    url = models.CharField(max_length=511,default='www.ya.ru')

    date_from = models.DateField(default = datetime.date.today)
    date_to = models.DateField(default = datetime.date.today)
    region_string = models.CharField(max_length=255,default = u'Россия')
    region_int = models.IntegerField(default = 225)    

    parsed = models.BooleanField(default=0)
    date_parsed = models.DateField(default = datetime.date.today)
    n_docs = models.IntegerField(default = -153)
    #pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.q

class Worker(models.Model):
    name = models.CharField(max_length=255)
    secret = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

class WorkerSession(models.Model):
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
    init_date = models.DateField(default = datetime.date.today)
    last_used_date = models.DateField(default = datetime.date.today)

    def __unicode__(self):
        return self.worker.question_text

class LogEntry(models.Model):
    event = models.CharField(max_length=255,default=u'test')
    def __unicode__(self):
        return self.event
#