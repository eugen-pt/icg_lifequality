from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^task_list$', views.TaskListView.as_view(), name='task_list'),
#    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^add/',views.add, name='add'),
    url(r'^add_task/',views.add_task, name='add_task'),
    url(r'^get_task/',views.get_task, name='get_task'),
    url(r'^init_worker/',views.init_worker, name='init_worker'),
    url(r'^start_session/',views.start_session, name='start_session'),
    url(r'^end_session/',views.end_session, name='end_session'),

]