from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from .models import YaQ,YaQ_task,Worker,WorkerSession
import json
import os
from datetime import datetime

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'YaQs/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return YaQ.objects.order_by('-parsed')[:20]

# Create your views here.
class TaskListView(generic.ListView):
    template_name = 'YaQs/task_list.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return YaQ_task.objects.order_by('-q')[:20]



@csrf_exempt
def add(request):
    response = HttpResponse(json.dumps({"key": "value", "key2": "value"}))
    try:
    # if(1):
        yaq = YaQ(q=request.POST['q'],
                    experiment=request.POST['experiment'],
                    date_from=datetime.strptime(request.POST['date_from'], '%Y-%m-%d').date(),
                    date_to=datetime.strptime(request.POST['date_to'], '%Y-%m-%d').date(),
                    region_int=int(request.POST['region_int']),
                    region_string=request.POST['region_string'],
                    parsed=bool(int(request.POST['parsed'])),
                    date_parsed=datetime.strptime(request.POST['date_parsed'], '%Y-%m-%d').date(),
                    n_docs=int(request.POST['n_docs']),
                    url=request.POST['url'],
                    )
        yaq.save()
        #q = request.POST['q']
    except:
        # Redisplay the question voting form.
        return HttpResponse("sth wrong..",status=400)
        # return render(request, 'YaQs/dummy.html', {
        #     'error_message': "You didn't select a choice. <br/> request.POST = %s"%json.dumps(dict(request.POST)),
        # })
    else:
        return HttpResponse(status=201)

@csrf_exempt
def add_task(request):
    response = HttpResponse(json.dumps({"key": "value", "key2": "value"}))
    try:
        yaq = YaQ_task(q = request.POST['q'],
                    date_from = datetime.strptime(request.POST['date_from'],'%Y-%m-%d').date(),
                    date_to = datetime.strptime(request.POST['date_to'],'%Y-%m-%d').date(),
                    region_int = int(request.POST['region_int']),
                    region_string = request.POST['region_string'],
                    url = request.POST['url'],
                    )
        yaq.save()
        #q = request.POST['q']
    except:
        # Redisplay the question voting form.
        return HttpResponse("sth wrong..",status=400)
        # return render(request, 'YaQs/dummy.html', {
        #     'error_message': "You didn't select a choice. <br/> request.POST = %s"%json.dumps(dict(request.POST)),
        # })
    else:
        return HttpResponse(status=201)


def init_worker(request):
    try:
        worker_name = request.GET['name']
    except:
        return HttpResponse("No name in GET",status=400)
    try:    
        worker = Worker.objects.get(name=worker_name)
        return HttpResponse("Already exists",status=400)
    except Worker.DoesNotExist:  
        secret = os.urandom(25).encode('hex')
        new_Worker = Worker(name = worker_name,secret=secret)
        new_Worker.save()
        return HttpResponse(secret,status=201)

def start_session(request):
    try:
        worker_name = request.GET['name']
        worker_secret = request.GET['secret']
    except:
        return HttpResponse("Not enough arguments in GET",status=400)
    try:
        worker = Worker.objects.get(name=worker_name)
    except Worker.DoesNotExist:  
        return HttpResponse("No worker with such name",status=404)
    if(worker.secret != worker_secret) : 
        return HttpResponse("Wrong secret",status=400)
    session = WorkerSession(worker=worker)    
    session.save()
    return HttpResponse(session.pk)

def end_session(request):
    try:
        worker_name = request.GET['name']
        worker_secret = request.GET['secret']
        session_pk = request.GET['session']
    except:
        return HttpResponse("Not enough arguments in GET",status=400)
    try:
        worker = Worker.objects.get(name=worker_name)
    except Worker.DoesNotExist:  
        return HttpResponse("No worker with such name",status=404)
    if(worker.secret!=worker_secret):
        return HttpResponse("Wrong secret",status=400)
    session = get_object_or_404(WorkerSession,pk=session_pk)
    session.delete()
    return HttpResponse("AllOK")

def get_task(request):
    try:
        pass
    except:
        return HttpResponse("sth wrong..",status=400)
    else:
        return HttpResponse("AllOK")


@csrf_exempt
def add_result(request):
    try:
        worker_name = request.GET['name']
        worker_secret = request.GET['secret']
        session_pk = request.GET['session']
    except:
        return HttpResponse("Not enough arguments in GET",status=400)
    worker = get_object_or_404(Worker,name=worker_name)
    if(worker.secret != worker_secret):
        return HttpResponse("Wrong secret",status=400)
    session = get_object_or_404(WorkerSession,pk=session_pk)
    session.last_used_date = datetime.date.today()


    try:
    # if(1):
        yaq = YaQ(q=request.POST['q'],
                    experiment=request.POST['experiment'],
                    date_from=datetime.strptime(request.POST['date_from'], '%Y-%m-%d').date(),
                    date_to=datetime.strptime(request.POST['date_to'], '%Y-%m-%d').date(),
                    region_int=int(request.POST['region_int']),
                    region_string=request.POST['region_string'],
                    parsed=bool(int(request.POST['parsed'])),
                    date_parsed=datetime.strptime(request.POST['date_parsed'], '%Y-%m-%d').date(),
                    n_docs=int(request.POST['n_docs']),
                    url=request.POST['url'],
                    )
        yaq.save()
        #q = request.POST['q']
    except:
        # Redisplay the question voting form.
        return HttpResponse("sth wrong..",status=400)
        # return render(request, 'YaQs/dummy.html', {
        #     'error_message': "You didn't select a choice. <br/> request.POST = %s"%json.dumps(dict(request.POST)),
        # })
    else:
        return HttpResponse(status=201)
