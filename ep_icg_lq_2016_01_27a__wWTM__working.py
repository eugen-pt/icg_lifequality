# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 15:12:44 2016

@author: ep
"""

"""      Imports      """

import json
import requests
"""      Consts       """

worker_name = 'icg12'
worker_secret = u'ab987d7d49a0d452576d676ac22f7435656802ae60f4fa3690'

""" """

def GET(url,data,target_code,task=''):
    R = requests.get(url+'?'+'&'.join(['%s=%s'%(k,data[k]) for k in data]))
    if(R.status_code==target_code):
        return json.loads(R.text)
    else:
        raise ValueError('%sServer returned not %i but %i'%((task+' : ') if task else '',target_code,R.status_code))

root_url = "http://127.0.0.1:80/YaQs/"    

""" """
# Worker init
try:
    worker_name
    worker_secret
except:
    worker_name = 'icg12'
    R = requests.get("http://127.0.0.1:80/YaQs/init_worker/?name=%s"%worker_name)
    if(R.status_code==201):
        worker_secret = R.text
    else:
        raise ValueError('Server returned not 201 but %i')%R.status_code

# Session init
try:
    session_pk
except:
    R = requests.get("http://127.0.0.1:80/YaQs/start_session/?name=%s&secret=%s"%(worker_name,worker_secret))
    if(R.status_code==200):
        session_pk = R.text
    else:
        raise ValueError('Server returned not 200 but %i')%R.status_code



R = requests.get("http://127.0.0.1:80/YaQs/get_task/?name=%s&secret=%s&session=%s"%(worker_name,worker_secret,session_pk))
if(R.status_code==200):  
    yaq_task = json.loads(R.text)
else:
    raise ValueError('Server returned not 200 but %i')%R.status_code
""" """

R = requests.get("http://127.0.0.1:80/YaQs/send_result/?name=%s&secret=%s&session=%s&q=%s&id=%s&N=%i"%(worker_name,worker_secret,session_pk,yaq_task['q'],yaq_task['id'],10))

    
""" """
#
