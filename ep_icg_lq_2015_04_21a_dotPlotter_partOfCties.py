# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:18:45 2015

@author: ep
"""

import matplotlib.pyplot as plt

try:
    Data
    catCorrMx_nq
    shortCats
except:    

    from ep_icg_lq_2015_04_20c_data_loader import *


""" """

okCities = find([not((' ' in c)or('-' in c)) for c in Cities])
sfx = '_okC'
if(len(okCities)<10):
    print('Regions!')
    from ep_icg_lq_2015_04_21c_russianStuff import *
    okCities = find([c in RuRegionsText for c in Cities])
    sfx = '_RuR'

#aaa
#from sklearn.metrics.cluster import normalized_mutual_info_score

#http://minepy.sourceforge.net/docs/1.0.0/python.html
from minepy import MINE


outpath_ok_ab = Data_fname.replace('.xls',''+sfx+'_ab').replace('percityAgregated','')
outpath_ok_cs = Data_fname.replace('.xls',''+sfx+'_cs').replace('percityAgregated','')
import os
try:
    os.mkdir(outpath_ok_ab)
except:
    pass    
try:
    os.mkdir(outpath_ok_cs)
except:
    pass    

nums = uCatNums_nq[0]
MAX = len(nums)
MICs = np.zeros((MAX,MAX))
figure()
for jj in range(MAX):
    j = nums[jj]
    for ii in range(MAX):
        if(ii==jj):
          continue
        i=nums[ii]
        a = MINE()

        X = nData[j][okCities]
        Y = nData[i][okCities]
        
        a.compute_score(X,Y)
        mic =    a.mic()     
                
        MICs[jj,ii] = mic
        
        corr = corrcoef(X,Y)[0,1]
        
        cs = 1 - ((3.0/4)* ((1-mic)**2) + (1.0/4)*(corr**2))
        
        plot(X,Y,'.')
        plt.xlabel(Qs[j])
        plt.ylabel(Qs[i])
        for cji in range(len(okCities)):
            plt.text(X[cji],Y[cji],Cities[okCities[cji]])
       
        plt.xticks([])
        plt.yticks([])
        fname = '%s_%s_cs%.2f_mic%.2f_corr%.2f.png'%(Qs[j],Qs[i],cs,MICs[jj,ii],corr )
        plt.savefig(outpath_ok_ab+os.sep+fname)
        fname = 'cs%.2f_mic%.2f_corr%.2f_%s_%s.png'%(cs,MICs[jj,ii],corr,Qs[j],Qs[i] )
        plt.savefig(outpath_ok_cs+os.sep+fname)
        plt.cla()

close(plt.gcf())            
#a.

aaa
""" """



tn1 = np.random.randint(0,len(uCatNums_nq[0]))
tn2 = np.random.randint(0,len(uCatNums_nq[0]))

plt.xlabel(Qs_nq[uCatNums_nq[0][tn1]])
plt.ylabel(Qs_nq[uCatNums_nq[0][tn2]])
plot(nData_nq[uCatNums_nq[0][tn1]],nData_nq[uCatNums_nq[0][tn2]],'.')
for cj in range(len(Cities)):
    plt.text(nData_nq[uCatNums_nq[0][tn1]][cj],nData_nq[uCatNums_nq[0][tn2]][cj],Cities[cj])




""" """
#