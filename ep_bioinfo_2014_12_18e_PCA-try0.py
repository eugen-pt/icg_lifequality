# -*- coding: utf-8 -*-
"""
Created on Thu Dec 18 15:13:16 2014

@author: ep
"""

from ep_bioinfo_2014_12_16a_src_reader3_r50 import *

#D = LQs_CM_D
#D = LQs_CM_D_CityScoreNorm
D = LQs_CM_D_CityPopulationNorm
#D = LQs_CM_D_RegionPopulationNorm
#outdir='PCA_figures_r50_CityScoreNorm'
outdir='PCA_figures_r50_CityPopulationNorm'

try:
    os.mkdir(outdir)
except:
    pass

uc_Eng = ['Architecture','Arts and Crafts','Design','Diseases','Fine Art','Innovation','Cinematography','Criminal','Small Business','Car makers','Music','Education','General','Consumer basket','Mass media','Sport','Theatre','Farmer','Imagin Lit','Info staffing']
engCats = uc_Eng

import numpy as np
from sklearn.decomposition import PCA
X = D.transpose()#np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
pca = PCA(n_components=20)
R=pca.fit_transform(X).transpose()

print(pca.explained_variance_ratio_)

NPCA = 4

for pcaj in range(NPCA):
    print('Component#%i (%i%% variance)'%(pcaj+1,pca.explained_variance_ratio_[pcaj]*100))
    tix = list(argsort(abs(pca.components_[pcaj])))
    tix.reverse()
    
    for j in range(len(tix)):
        print('%s\t%.2f'%(uc_Eng[tix[j]],pca.components_[pcaj][tix[j]]))
    print('')    
    print('')    
    
figure()
for cj in range(NPCA):
  for ci in range(NPCA):
    plot(R[ci,:],R[cj,:],'.')
    for j in range(len(LQs_Cities)):
      text(R[ci,j],R[cj,j],LQs_engCities[j])
    xlabel('Component%i'%(ci+1))
    ylabel('Component%i'%(cj+1))
    
    show()
    savefig(outdir+u'/f_Component%i_Component%i'%(ci+1,cj+1)+'.png', bbox_inches='tight')
#    break
    clf()
    
