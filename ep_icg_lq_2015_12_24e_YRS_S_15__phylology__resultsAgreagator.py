# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 16:07:41 2015

@author: ep
"""

"""      Imports      """
import  json

from matplotlib_venn import venn3, venn3_circles

"""      Consts       """

fname = '2015-12-24_16-06-19__v15__PH__pagesC_percity.csv'

""" Local Definitions """


"""   Data Loading    """

LS= open(fname,'r').readlines()

RD = [json.loads(S[:-2]) for S in LS[1:]]

for d in RD:
    d['q']=d['q'].replace('%20%26%26%20',' && ')

Vals = {}
uVals = {}
for k in RD[0].keys():
    print(k)
    try:
        uVals[k] = unique([d[k] for d in RD])
        Vals[k] = array([d[k] for d in RD])
    except: 
        uVals[k] = unique([d[k][0] for d in RD]) #mostly for dates
        Vals[k] = array([d[k][0] for d in RD])

uVals_ixs = {k:{uv:[j for j in range(len(Vals[k])) if (Vals[k][j]==uv)or((type(Vals[k][j])==type([]))and(Vals[k][j][0]==uv))]  for uv in uVals[k]} for k in uVals}
     
     
Qs = [d['q'].replace('%20%26%26%20',' && ') for d in RD]
Ns = [d['N'] for d in RD]


"""    Processing     """
#for cat1 in uVals['cat1']:
#    hixs = uVals_ixs['cat1'][cat1]
#    
#    thixs = [j for j in hixs if Vals['q'][j]==Vals['q1'][j]]
#    print(cat1)
#    for j in thixs:
#        s = Vals['q'][j]
#        print('%s\t%s\t%i'%(cat1,s,Vals['N'][j]))
#    
#"""    Processing     """
#for cat2 in uVals['cat2']:
#    hixs = uVals_ixs['cat2'][cat2]
#    
#    thixs = [j for j in hixs if Vals['q'][j]==Vals['q2'][j]]
#    print(cat2)
#    for j in thixs:
#        s = Vals['q'][j]
#        print('%s\t%s\t%i'%(cat2,s,Vals['N'][j]))
    

#aaa
print('\t'.join(map(str,['cat1','uq1','N','cat2','uq2','N','','q12','N'])))
for cat1 in uVals['cat1']:
    for cat2 in uVals['cat2']:
        if(cat2=='КОРПУС'):
            continue
        hixs = list(set(uVals_ixs['cat1'][cat1]).intersection(uVals_ixs['cat2'][cat2]))
        if(len(hixs)==0):
            continue
        
        uq1s = unique(Vals['q1'][hixs])
        for uq1 in uq1s:
            hhixs = set(hixs).intersection(uVals_ixs['q1'][uq1])
            if(len(hhixs)==0):
                continue
            uq2s = unique(Vals['q2'][hixs])
            for uq2 in uq2s:
                hhhixs = set(hhixs).intersection(uVals_ixs['q2'][uq2])
                if(len(hhhixs)==0):
                    continue
                
                tix1 = [j for j in hhhixs if Vals['q'][j]==uq1]
                tix2 = [j for j in hhhixs if Vals['q'][j]==uq2]
                q12 = uq1+'%20%26%26%20'+uq2
                q12 = uq1+' && '+uq2
                
                tix12 = [j for j in hhhixs if Vals['q'][j]==q12]
                #print(map(len,[tix1,tix2,tix12]))
                
                print('\t'.join(map(str,[cat1,uq1,Vals['N'][tix1[0]],cat2,uq2,Vals['N'][tix2[0]],'',q12,Vals['N'][tix12[0]]])))
                
                    


"""      Saving       """



""" """
#
