# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 15:36:49 2015

@author: ep
"""

"""      Imports      """

import xlrd

"""    Local defs     """
def v(r):
    return [c.value for c in r]

def pd(d):
    print(' | '.join(['%s:%s'%(k,d[k]) for k in d]))

""" Full Table """
xlspath = u'Список предикатов.xlsx'

cats_replace_D = {u'Эмоциональная и волевая сфера ':u'ЭВС',u'Волевая деятельность':u'ВолДеят',u'Оценка эмоции':u'Оц.эм.',u'оложительная ':u'ол.',u'Отрицательная ':u'отр.',u'ейтральная ':u'ейтр.',u'Информирование с оценкой':u'Инф.с оц.'}
def fill_values(row):
    R = []
    cr = u''
    for r in map(str,row):
        if(len(r)>0):
            cr=r            
        R.append(cr)
    return R
    
def make_cat(cats):
   R = ''
   cn = -1
   for j in range(len(cats)):
       c = cats[j]
       if(len(c)==0):
           continue
       for k in cats_replace_D:
           c=c.replace(k,cats_replace_D[k])
       R = c.strip()
       cn=j
       break
   for j in range(cn+1,len(cats)):
       c=cats[j]
       if(len(c)==0):
           continue
       for k in cats_replace_D:
           c=c.replace(k,cats_replace_D[k])
       R=R+'/'+c.strip()
   return R
    
try:
    PLF_Qs
    PLF_ucats_ordered
except:    
    PLF_ucats_ordered = []
    PLF_cats = []
    PLF_words = []
    PLF_sheetNames = []
    
    PLF_Qs = []#{'cat2':PLF_cats[j],u'q2':PLF_words[j]'}]
    
    wb = xlrd.open_workbook(xlspath)
    
    sn = u'ТАБ1'
    s = wb.sheet_by_name(sn)
    
    hcats = v(s.row(0))
    for j in range(len(hcats)):
        hcat = hcats[j]
        if(hcat not in PLF_ucats_ordered):
            PLF_ucats_ordered.append(hcat)
        words = v(s.col(j,1))
        for w in words:
            if(len(w)>0):
                PLF_cats.append(hcat)
                PLF_words.append(w)
                PLF_Qs.append({'q2':w,u'cat2':hcat,u'sheetName':sn})
    
    """ """
    sn = u'ТАБ2'
    s = wb.sheet_by_name(sn)
    
    
    hcats = [fill_values(v(s.row(j))) for j in [0,1,2]]
    
    for j in range(len(hcats[0])):
        hcat = make_cat([a[j] for a in hcats])
        if(hcat not in PLF_ucats_ordered):
            PLF_ucats_ordered.append(hcat)
        words = v(s.col(j,3))
        for w in words:
            if(len(w)>0):
                PLF_cats.append(hcat)
                PLF_words.append(w)
                PLF_Qs.append({'q2':w,u'cat2':hcat,u'sheetName':sn})
    
    """ """
    sn = u'ТАБ3'
    s = wb.sheet_by_name(sn)
    
    hcats = [fill_values(v(s.row(j))) for j in [0,1]]
    
    for j in range(len(hcats[0])):
        hcat = make_cat([a[j] for a in hcats])
        if(hcat not in PLF_ucats_ordered):
            PLF_ucats_ordered.append(hcat)
        words = v(s.col(j,2))
        for w in words:
            if(len(w)>0):
                PLF_cats.append(hcat)
                PLF_words.append(w)
                PLF_Qs.append({'q2':w,u'cat2':hcat,u'sheetName':sn})
    
    """ """
    sn = u'ТАБ4'
    s = wb.sheet_by_name(sn)
    
    hcats = [fill_values(v(s.row(j))) for j in [0,1]]
    
    for j in range(len(hcats[0])):
        hcat = make_cat([a[j] for a in hcats])
        if(hcat not in PLF_ucats_ordered):
            PLF_ucats_ordered.append(hcat)
        words = v(s.col(j,2))
        for w in words:
            if(len(w)>0):
                PLF_cats.append(hcat)
                PLF_words.append(w)
                PLF_Qs.append({'q2':w,u'cat2':hcat,u'sheetName':sn})
    
    """ """
    sn = u'ТАБ5'
    s = wb.sheet_by_name(sn)
    
    hcats = [fill_values(v(s.row(j))) for j in [0,1]]
    
    for j in range(len(hcats[0])):
        hcat = make_cat([a[j] for a in hcats])
        if(hcat not in PLF_ucats_ordered):
            PLF_ucats_ordered.append(hcat)
        words = v(s.col(j,2))
        for w in words:
            if(len(w)>0):
                PLF_cats.append(hcat)
                PLF_words.append(w)
                PLF_Qs.append({'q2':w,u'cat2':hcat,u'sheetName':sn})
    
#    for D in PLF_Qs:
#        print(' | '.join(['%s:%s'%(k,str(D[k])) for k in D]))
#aaa
""" Short Table """
xlspath = u'для филологов.xlsx'

try:
    PL_Qs
except:
    
    w = xlrd.open_workbook(xlspath)
    s = w.sheet_by_index(0)
    
    PL_cats1 = v(s.col(1,1))
    PL_words1 = v(s.col(2,1))
    PL_cats2 = v(s.row(0)[2:])
    
    PL_keys = ['cat1',u'q1',u'cat2',u'q2']
    PL_Qs = []
    
    for j2 in range(len(PL_cats2)):
        tcol = v(s.col(j2+2,1))
        for j1 in range(len(PL_words1)):
            if(len(tcol[j1])==0):
                continue
            tq = {'cat2':PL_cats2[j2]}
            tq['cat1'] = PL_cats1[j1]
            tq['q1'] = PL_words1[j1]
            tq['q2'] = tcol[j1]
            PL_Qs.append(tq)
        
    
#    for D in PL_Qs:
#        print(' | '.join(['%s:%s'%(k,str(D[k])) for k in D]))

""" """
#
