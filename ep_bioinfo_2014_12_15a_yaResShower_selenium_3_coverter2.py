# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 16:24:15 2014

@author: ep_work
"""

outfname = '2014-12-11_12-38-51__pagesCount.csv'

with open(outfname) as f:
  LS = f.readlines()

LS = [s.split(',') for s in LS]



import xlwt

outpath = 'src/1-redone/'

wbAT = xlwt.Workbook()
sAT = wbAT.add_sheet('1')

wb=xlwt.Workbook()

s = wb.add_sheet('1')

def writeL(L,j,add_date=1):
  if(add_date):
    L.append('12.12.2014')
  for i in range(len(L)):
    try:
      s.write(j,i,int(L[i]))
    except:
      s.write(j,i,unicode(str(L[i])))

def writeLat(L,j,add_date=1):
  if(add_date):
    L.append('12.12.2014')
  for i in range(len(L)):
    try:
      sAT.write(j,i,int(L[i]))
    except:
      sAT.write(j,i,unicode(str(L[i])))

curfile = ''
curcat = ''
headerL=LS[0]

temp = headerL
temp.append('date')
writeLat(temp,0,0)

hn=0

filej=0
for j in range(1,len(LS)):
  L =LS[j]
  writeLat(L,j)
  hcat = L[1]
  hfile=L[0]
  if(hfile==curfile):
    pass
  else:
    if(len(curfile)>0):
      print('saving')
      wb.save(outpath+curfile[:-1])

    wb=xlwt.Workbook()
    s = wb.add_sheet('1')
    curfile=hfile
    filej=0
#    temp = L[0:2]
#    temp.append(temp[1])
    temp=[headerL[1]]
    temp.extend(headerL[2:])
    temp.append('date')
    writeL(temp,0,0)

  if(hcat==curcat):
    pass
  else:
    filej=filej+1
    writeL([hcat,hcat],filej,0)
    curcat = hcat
    hn=0
  filej=filej+1
  hn=hn+1
#  temp = [hfile,hcat]
#  temp.append(hn)
  temp = [hn]
  temp.extend(L[2:])
  writeL(temp,filej)
wb.save(outpath+curfile[:-1])

wbAT.save(outpath+'AT.xls')

""" """
#

