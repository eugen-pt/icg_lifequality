# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 15:58:37 2014

@author: ep_work
"""


# MAKE SURE there's the data you need that's loaded !!

from ep_bioinfo_2014_12_10f_src_reader1 import *

import xlwt

wb=xlwt.Workbook()
s=wb.add_sheet('1')

def writeL(L,j,add_date=1):
  for i in range(len(L)):
    try:
      s.write(j,i,int(L[i]))
    except:
      s.write(j,i,unicode(L[i]))

temp=['file','cat','q']
temp.extend(LQs_Cities)
temp.append('date')
writeL(temp,0)

for j in range(len(LQs_qs)):
  temp = [LQs_files[j],LQs_cats[j],LQs_qs[j]]
  temp.extend(LQs_datas[j])
  writeL(temp,j+1)

wb.save('src_1_converted_AT.xls')

""" """
#

