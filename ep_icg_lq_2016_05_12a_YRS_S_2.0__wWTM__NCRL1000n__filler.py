# -*- coding: utf-8 -*-
"""
Created on Wed May  4 14:59:06 2016

@author: ep
"""

"""      Consts       """


experiment = '2_0_0__2016_05_12a__NCRL1000n'



"""      Imports      """
import datetime


#from ep_icg_lq_2016_01_11c_countries_load import *
from ep_icg_lq_2016_05_11a__NCRL_1000nouns__reader import *
# for cities list
from ep_icg_lq_2016_04_28b__Ya__regionslist import *

from   ep_lq_2016_04_27a__WTM___interface import *


""" Local Definitions """
def getDates(start='01.01.2000',end='',each_m=3):
    if not (end):
        # get start of the current month
        tempd = datetime.datetime.now().replace(day=1)
        # get end of the previous month
        tempd= tempd - datetime.timedelta(days=1)
        end = tempd.strftime('%d.%m.%Y')
    [start_date,end_date] = [datetime.date(tx[2],tx[1],tx[0]) for tx in  [map(int,re.findall('([0-9]+)',a)) for a in [start,end]] ]

    R_dates = []
    while(start_date<end_date):
        hstart_date = start_date
        start_date = datetime.date(hstart_date.year+1*(hstart_date.month+each_m>12),hstart_date.month+each_m if (hstart_date.month+each_m<=12) else hstart_date.month+each_m-12,1);
        hend_date = start_date-datetime.timedelta(1)
        R_dates.append((hstart_date.strftime('%d.%m.%Y'),hend_date.strftime('%d.%m.%Y')))
    return R_dates

""" """

source_dates = getDates(start='01.01.2014',end='31.12.2015',each_m=1)


#""" Негативная окраска """
#neg_addss = [u'плохо',u'хуже',u'ужасно',u'кошмар']
#""" Позитивная окраска """
#pos_addss = [u'хорошо',u'замечательно',u'отлично',u'лучше',u'чудесно']
#
#posneg_adds = {1:' && '+'('+' | '.join(pos_addss)+')'+''.join([' -%s'%s for s in neg_addss]),-1:' && '+'('+' | '.join(neg_addss)+')'+''.join([' -%s'%s for s in pos_addss])}

"""   Data Loading    """

LQs = []
for q in NCRL_1000nouns:
    for city in YaRuRegions:
        for tdates in source_dates:
            tq = dict( \
                      dates=tdates,
                      city=city,
                      city_num=YaRegions_NumsDict[city],
                      q = q,
                      posneg = 0,
                      adds='',
            )
            LQs.append(tq)


#aaa

add_group_N = 100


add_group_N = 10000#len(LQs)
no_duplicates = 1
"""    Processing     """
LQj=0
while(LQj<len(LQs)):
    print('%i/%i'%(LQj,len(LQs))),
    LQn = LQj+add_group_N
    if(LQn>len(LQs)):
        LQn = len(LQs)
    while(1):
        try:
            WTM_addTasks(LQs[LQj:LQn],experiment,no_duplicates=no_duplicates)
            LQj=LQn
            print('')
            break
        except:
            print('.'),
            pass


#    break


#WTM_addTasks(LQs,experiment)

"""      Saving       """



""" """
#
