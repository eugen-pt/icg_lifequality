# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work


6 = src2, with all the words !

9 = new Yandex interface

10 - timeout for loading page, yandex cities

"""

experiment = 'countries_att_dyn'



outfname = '2016-01-11_16-28-16__v17__countries_att_dyn__pagesC_percity.csv'
#outfname = ''

# Operating options

do_fromTheEnd = 0

num_fetch_threads = 8

report_every_minute = 0
report_every_hour = 1
report_every_N = 5000

# Parsing options

pauseK = .05;
pauseMin = .05;
pauseAfter = .05;
pauseAfterCityEntered = .3;
pauseBetweenWorkerStart = 10
pauseWhenYandexError = 20

#pauseK = 2;
#pauseMin = 1
#pauseAfter = 1
#pauseAfterCityEntered = 1

#from ep_bioinfo_2014_12_10f_src_reader1 import *
#from ep_bioinfo_2015_01_29d_src_reader3_3 import *
#from ep_icg_lq_2015_12_24c__phil__words import *
from ep_icg_lq_2016_01_11c_countries_load import *


# for cities list
from ep_icg_lq_2015_01_29a_gradoteka_citieslist import *


#aaa


from scipy import misc
import subprocess

import datetime
import gzip
import json
from   Queue import Queue
import re
from   selenium import webdriver
from   selenium.webdriver.common.keys import Keys
from   threading import Thread
import time
from   time import sleep as pause
import zipfile

# System modules



from ep_icg_lq_2015_12_25d__email__test import sendemail
""" """
""" Setting starting working parameters """
""" """


def myjson(d):
    if(type(d)!=type({})):
        raise ValueError('Only for dicts, not for %s'%str(type(d)))
    tkeys = sort(d.keys())
    return json.dumps([[k,d[k]] for k in tkeys])

def myjson_load(s):
    A =json.loads(s)
    return {a[0]:a[1] for a in A}



def savingPreStr(tq):
    return myjson(tq)

#aaa
#cities = YaRuRegions
#
#
#print('len(YaRuRegions)=%i'%len(YaRuRegions))
#if(len(cities)!=86):
#    raise ValueError('Length of YaRuRegions should be 86')

#delete_this_if_len_of_cities_is_86


#TODO
#outfname = '2015-12-16_10-58-44__v13__KNA_wtf_wtF__pagesC_percity.csv'
#outfname = '2015-12-23_15-57-19__v14r__NY__pagesC_percity.csv'
#outfname = '2015-12-25_13-39-05__v16__PHF__pagesC_percity.csv'
#NSO_subregions = ['Татарский','Карасукский','Барабинский','Тогучинский','Ордынский','Коченевский','Кыштовский']
#Novosibirsk_subregions = ['Дзержинский','Железнодорожный','Заельцовский','Калининский','Кировский','Ленинский','Октябрьский','Первомайский','Советский','Центральный']
#
#NSO_allSubregions = """► Баганский район‎ (6: 3 кат., 3 с.)► Барабинский район‎ (9: 4 кат., 5 с.)► Болотнинский район‎ (8: 4 кат., 4 с.)► Венгеровский район‎ (9: 6 кат., 3 с.)► Доволенский район‎ (8: 4 кат., 4 с.)З► Здвинский район‎ (9: 3 кат., 6 с.)И► Искитимский район‎ (11: 6 кат., 5 с.)К► Карасукский район‎ (7: 4 кат., 3 с.)► Каргатский район‎ (8: 3 кат., 5 с.)► Колыванский район‎ (9: 4 кат., 5 с.)► Коченёвский район‎ (9: 6 кат., 3 с.)► Кочковский район‎ (8: 5 кат., 3 с.)► Краснозёрский район‎ (6: 3 кат., 3 с.)► Куйбышевский район Новосибирской области‎ (8: 4 кат., 4 с.)► Купинский район‎ (7: 3 кат., 4 с.)► Кыштовский район‎ (26: 5 кат., 21 с.)М► Маслянинский район‎ (9: 5 кат., 4 с.)► Мошковский район‎ (9: 6 кат., 3 с.)Н► Новосибирский район‎ (9: 5 кат., 4 с.)О► Ордынский район‎ (11: 4 кат., 7 с.)С► Северный район Новосибирской области‎ (10: 5 кат., 5 с.)► Сузунский район‎ (7: 4 кат., 3 с.)Т► Татарский район‎ (6: 3 кат., 3 с.)► Тогучинский район‎ (11: 6 кат., 5 с.)У► Убинский район‎ (9: 4 кат., 5 с.)[×] Упразднённые районы Новосибирской области‎ (7: 7 с.)► Усть-Таркский район‎ (9: 4 кат., 5 с.)Ч► Чановский район‎ (8: 4 кат., 4 с.)► Черепановский район‎ (11: 5 кат., 6 с.)► Чистоозёрный район‎ (6: 3 кат., 3 с.)► Чулымский район‎ (8: 5 кат., 3 с.) """
#NSO_allSubregions=re.findall('([^ ]+) район',NSO_allSubregions)
#NSO_allSubregions.remove('Новосибирский')
#
#NSO_subregions = NSO_allSubregions
""" """


""" """

#01.11.2013 - 30.11.2013	2385	0,000000408568

#01.12.2013 - 31.12.2013	1787	0,000000297656


source_dates = """
01.01.2014 - 31.01.2014	2171	0,000000314953
01.02.2014 - 28.02.2014	2612	0,000000429862
01.03.2014 - 31.03.2014	4698	0,000000667481
01.04.2014 - 30.04.2014	4793	0,000000751422
01.05.2014 - 31.05.2014	4138	0,000000682031
01.06.2014 - 30.06.2014	2758	0,000000499594
01.07.2014 - 31.07.2014	2563	0,000000511113
01.08.2014 - 31.08.2014	2240	0,000000401344
01.09.2014 - 30.09.2014	3156	0,000000511335
01.10.2014 - 31.10.2014	2886	0,000000421512
01.11.2014 - 30.11.2014	1987	0,000000305978
01.12.2014 - 31.12.2014	1876	0,000000269841
01.01.2015 - 31.01.2015	2227	0,000000324848
01.02.2015 - 28.02.2015	2841	0,000000469259
01.03.2015 - 31.03.2015	5800	0,000000831993
01.04.2015 - 30.04.2015	6510	0,000000942154
01.05.2015 - 31.05.2015	4138	0,000000682031
01.06.2015 - 30.06.2015	2758	0,000000499594
01.07.2015 - 31.07.2015	2563	0,000000511113
01.08.2015 - 31.08.2015	2240	0,000000401344
01.09.2015 - 30.09.2015	3156	0,000000511335
01.10.2015 - 31.10.2015	2886	0,000000421512
01.11.2015 - 30.11.2015	2886	0,000000421512
01.12.2015 - 31.12.2015	2886	0,000000421512
"""
source_dates = re.findall('([0-9\.]+) \- ([0-9\.]+)',source_dates)
#source_dates = [source_dates[0][0],source_dates[-1][1]]
#source_dates=source_dates[8:]
"""
Негативная окраска
"""
neg_addss = [u'плохо',u'хуже',u'ужасно',u'кошмар']
"""
Позитивная окраска
"""
pos_addss = [u'хорошо',u'замечательно',u'отлично',u'лучше',u'чудесно']


posneg_adds = {1:' && '+'('+' | '.join(pos_addss)+')'+''.join([' -%s'%s for s in neg_addss]),-1:' && '+'('+' | '.join(neg_addss)+')'+''.join([' -%s'%s for s in pos_addss])}

#aaa

""" """
""" Preparing arrays """
""" """
#aaa
LQs = []

import copy


for cj in range(len(countries)):
    s = countries[cj]
    for tdates in source_dates:
        for posneg in posneg_adds:
            hadds = '('+' | '.join(posneg_adds[posneg])+')'
            tq = dict(experiment=experiment,
                      dates=tdates,
                      city=u'Россия',
                      q = s+posneg_adds[posneg],
                      country = s,
                      country_num= countries_nums[cj],
                      posneg = posneg,
                      adds=posneg_adds[posneg],
            )
            LQs.append(tq)

#aaa

#aaa
sendemail(subj='LQs created')
#aaa


#            for adds in posneg_adds[posneg]:
#                LQs_files.append('Novosibirsk')
#                LQs_cats.append(cat)
#                LQs_qs.append(a)
#                LQs_cities.append([u'Россия'])
#                LQs_dates.append(dates)
#                LQs_posneg.append(posneg)
#                LQs_adds.append(adds)
""" """

#LQj_done={}





PreStr_done = {}

try:
#if(1):
    with open(outfname) as f:
        LS = f.readlines()
    sDs = [myjson_load(S[:-2]) for S in LS[1:]]
    for d in sDs:
        del d['N']
        PreStr_done[myjson(d)]=1


#  aaa
#  print('Checking if file already exists..')
#  if(1):
#    """ Deals badly with commas in qs"""
#    with open(outfname) as f:
#      LS = f.readlines()
#    print(len(LS))
##    LS = [[s.replace('&',',').replace(u'&',',') for s in L.split(',')[0:4]] for L in LS[1:]]
#    LS = [[s for s in L.replace('\r\n','').split(',')[0:-1]] for L in LS[1:]]
#  else:
#    import csv
#
#    with open(outfname,'rb') as f:
#      r = csv.reader(f)
#      print(len(LS))
#      LS= [row for row in r]
#      LS = [L[0:4] for L in LS[1:]]
#
#  for L in LS:
#    PreStr = ','.join(L)
##    print(PreStr)
#    PreStr_done[unicode(PreStr)]=1


#  pass
except:
    print('Error in file loading')
    outfname = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__v17__%s__'%experiment+'pagesC_percity.csv'

    with open(outfname,'w') as f:
        f.write('json stuff per line:\r\n')

#
#aaa





def downloadEnclosures(i, q,outq,wipq,doneq):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print '%s: Logining..' % i
    driver = webdriver.Firefox()
    driver.set_page_load_timeout(30)
    #driver.get("http://www.python.org")
#    driver.get('https://passport.yandex.ru/passport?mode=auth')
#
#    driver.find_element_by_xpath('//input[@id="login"]').clear()
#    driver.find_element_by_xpath('//input[@id="login"]').send_keys('iwan.vepreff')#ya.kolchanov2015')
#
#
#    driver.find_element_by_xpath('//input[@id="passwd"]').clear()
#    driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
#
#    driver.find_element_by_xpath('//span[@class="_nb-button-content"]').click()
#
    print '%s: log in OK' % i
    qstr = ''
    LQj = -153
    while True:
        try:
            #print '%s: Reading next Q' % i
            hq = q.get()
            qstr = hq['q']
            #print '%s: q=%s' % (i,qstr)



            wip_queue.put(hq)

            tqstr = qstr.replace(' ','%20').replace('&','%26').replace('|','%7C')
            regstr = '&rstr=-%i'%YaRegions_NumsDict[hq['city']] if (len(hq['city'])>0) else ''
            url='http://yandex.ru/yandsearch?text=%s&lang=ru%s&from_date_full=%s&to_date_full=%s'%(tqstr,regstr,hq['dates'][0],hq['dates'][1])

            #print(url)
            while(1):
              try:
                driver.get(url)
                break
              except:
                pass
            pause(pauseMin+rand()*pauseK)


            while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
              print('%s: "Yandex" not in title! sth wrong')
              try:
                driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                pause(1)
              except:
                pause(pauseWhenYandexError)
                while(1):
                  try:
                    driver.get(url)
                    break
                  except:
                    pass
                pause(pauseMin+rand()*pauseK)

            #print '%s: url opened' % i

            temp = driver.title



            prevtitle = ''


            Ns = -153

            prevtitle = driver.title



            for j in range(15):
              ttitle = driver.title
              temp=ttitle.replace(u'млн','000 000')
              temp=temp.replace(u'тыс.','000')

              try:
                tempN = int(''.join(re.findall('[0-9]',temp)))
              except:
                if(u'ничего не найдено' in temp):
                  tempN=0
                else:
                  raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
              if(prevtitle!=ttitle):
                break
              pause(pauseAfter+rand()*pauseK)
            Ns = tempN

            #print '%s: Done' % i
            outq.put((hq,Ns))
            doneq.put(hq)

            q.task_done()
        except Exception, e:
            #raise e
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1);


#aaa
# Filling the Queue
#print("Filling the Queue")

#aaa


NAlreadyDone = 0;
PreStr_done
enclosure_queue = Queue()

NLeft = 0

for LQjt in range(len(LQs)):
  if(do_fromTheEnd):
    LQj = len(LQs_qs)-1-LQjt
  else:
    LQj = LQjt

  hq = LQs[LQj]
  qstr = hq['q']
#  try:
#    print(qstr)
#  except:
#    qstr=qstr[0:-1]
#    print(qstr)
  hq['q']=qstr
  """ adding unquoted stuff """
  PreStr =  savingPreStr(hq)

#    raise ValueError()

  if((unicode(PreStr) in PreStr_done)or(PreStr in PreStr_done)):
#      print('done already')
      NAlreadyDone=NAlreadyDone+1
      continue

  enclosure_queue.put(hq)
  NLeft=NLeft+1


print('Queue filled.')
#aaa
#aaa
sendemail(subj='Queue filled.')

print("**************************************************")

print('NAlreadyDone = %i'%NAlreadyDone)
print('Left to do       = %i'%enclosure_queue.qsize())
print('Left to do exact = %i'%NLeft)

#aaa


output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()

def CheckWIPnDone():
  print 'CheckWIPnDone:starting'
  wips = {}
  wipN=0
  while(not wip_queue.empty()):
    hq = wip_queue.get()
    PreStr =  savingPreStr(hq)
    wips[PreStr]=hq
    wipN=wipN+1

  print('CheckWIPnDone:' +' found %i wipStuff'%wipN)
  while(not doneq_queue.empty()):
    hq = doneq_queue.get()
    PreStr =  savingPreStr(hq)
    try:
      wips[PreStr]=0
      wipN=wipN-1
    except:
      print('somehow done but not wip : %s'%PreStr)
#      raise ValueError('somehow wip but not done : %s'%PreStr)
  print('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)

  for s in  wips.keys():
    t = wips[s]
    if(t==0):
      pass
    else:
      hq = t
      print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(t)
      enclosure_queue.put(hq)


  print 'CheckWIPnDone:done'


def saveStuff(in_fname):
    fname = in_fname+'.zip'
    f=zipfile.ZipFile(fname,'a', zipfile.ZIP_DEFLATED)
    f.write(in_fname)
    f.close()
    return fname


def reportEmail(subj='Hello!',body='Hello!'):
    print('Reporting E-mail subj=%s  body=%s'%(subj,body))
    try:
        sendemail(subj=subj,body=body,logfile=saveStuff(outfname))
    except:
        print('Reporting E-mail with attachment failed, trying without attachment..'),
        try:
            sendemail(subj=subj,body=body)
            print('   OK')
        except:
            print('***********************************\nreportEmail ERROR')


Workers = []

def start_all_workers(num_fetch_threads=num_fetch_threads):
#  for worker in Workers:
#    del worker
  print('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
      Workers.append(worker)
      pause(pauseBetweenWorkerStart)
  print('Workers started.')

if(1):

  print('NLeft=%i'%NLeft)
  start_all_workers()


  import psutil

  def savingThread(q,q_in,sNleft = 0):
    print('savingThread: Sarted.')
    NLeft=sNleft
    last_sent = NLeft#q_in.qsize()
    last_sent_hour = datetime.datetime.now().hour
    last_sent_minute = datetime.datetime.now().minute
    last_q = ''

    reportEmail(subj='[S] Running, NLeft report',body='S %i  last:%s'%(last_sent,last_q))

    while(True):

      in_empty_n = 0

      ntdn = 0
      while(q.empty()):
        if(q_in.empty()):
          in_empty_n=in_empty_n+1
          print('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
        else:
          #print('savingThread: nothing to do..')
          ntdn=ntdn+1

        #q_in.qsize()
#        print('~ %i left undone, ntd=%i'%(q_in_size,ntdn))


        if(ntdn>40):
          print('='*10)
          print('Restarting')
          sendemail(subj='Restarting',body='Started')
          print('='*10)

          print('Killing firefoxes..')
          # Restarting because sth went wrong!

          for proc in psutil.process_iter():
            if (proc.name == 'firefox')or(proc.name == 'iceweasel'):
              proc.kill()

          print('done.')

          CheckWIPnDone()

          pause(1800)

          start_all_workers()
          ntdn=0
          print('='*10)
          sendemail(subj='Restarting',body='Done')


        if(in_empty_n>100):
          print('savingThread: nothing to DO! returning.')
          return
        pause(1)
      print('savingThread: saving !)')
      temp_saved=0
      with open(outfname,'a') as f:
        while(q.empty()==0):
          hq,Ns=q.get()
          last_q = hq['q']
          hq['N'] = Ns
          f.write((savingPreStr(hq)+'\r\n').encode('utf8'))
          NLeft=NLeft-1
          temp_saved=temp_saved+1
          #print(NLeft)
      print('   saved %i, %i left'%(temp_saved,NLeft))
      try:
          subprocess.Popen(['sudo','find','/tmp','-amin','+30','-delete'])
          print('/tmp cleared')
      except:
          print('****************\nError in clearing temp')
          reportEmail(subj='Error in clearing temp',body='N %i  last:%s'%(q_in_size,str(last_q)))

      if(1):
        q_in_size = NLeft
#        print('q_in_size=%i'%q_in_size)
#        print('last_sent=%i'%last_sent)
#        print('abs(q_in_size-last_sent)=%i'%abs(q_in_size-last_sent))
#        print('report_every_N=%i'%report_every_N)
        if(report_every_N>0) and (abs(q_in_size-last_sent)>=report_every_N):
            reportEmail(subj='[N] Running, NLeft report',body='N %i  last:%s'%(q_in_size,str(last_q)))
            last_sent= q_in_size

        if(report_every_minute>0) and (abs(datetime.datetime.now().minute - last_sent_minute)>=report_every_minute) :
            last_sent_minute = datetime.datetime.now().minute
            reportEmail(subj='[M] Running, NLeft report',body='M %i  last:%s'%(q_in_size,str(last_q)))

        if(report_every_hour>0)and(abs(datetime.datetime.now().hour-last_sent_hour)>=report_every_hour):
            last_sent_hour = datetime.datetime.now().hour
            reportEmail(subj='[H] Running, NLeft report',body='H %i  last:%s'%(q_in_size,str(last_q)))
      pause(5)


  #print('waiting for first output..')
  #while(output_queue.empty()):
  #  pause(.1)
  #print('starting saver..')
  print('NLeft=%i'%NLeft)
  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,NLeft,))
  worker.setDaemon(True)
  worker.start()

  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
  print '*** Main thread waiting'
  enclosure_queue.join()
  print '*** Main thread Done waiting'
  reportEmail('*** Main thread Done waiting','')

""" """
#

