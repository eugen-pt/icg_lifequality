# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:38:59 2014

@author: ep_work

""""""

Checklist:

- check worker variable (line 43 :) )
- check version (line 47)
- check experiment name (line 49)   --  !!! This is IMPORTANT







""""""
6 = src2, with all the words !

9 = new Yandex interface

10 - timeout for loading page, yandex cities

18 - +mods for O's 13
"""

worker_name = 'icg12-ep-2'
#worker_name = ''
if(worker_name==''):
    for t in [("socket","gethostname()"),('os','uname()[1]'),('platform','uname()[1]')]:
        try:
            worker_name = eval("__import__('%s').%s"%t)
        except:
            pass
# Check that everything's OK and worker is set to a reasonable string
print('Worker name : [%s]'%worker_name)


#aaa_checkWorkerName__commentThisIfOK


# See that thiss corresponds to version
version = '2.0.0'

experiment = '2_0_0_test0_2016_05_04a'


email_subj_prefix = '[ICG_Ya]@%s: '%(worker_name)


"""
        Operating options
"""

# 1 => doesnt actually start parsing
stopAfterCheckDone = 0

# 0 => nothing, 1=> short, 2 => lots of stuff including URLs, etc
verbose = 1

# 0 = from start , 1= from end, 2 = randomized--undone
# not implemented in the online-based tool
do_fromTheEnd = 0






if(1):
    num_fetch_threads = 3
    num_fetch_threads_only_json = 0
    suffix = ''
else:
    num_fetch_threads = 0
    num_fetch_threads_only_json = 1
    suffix = '_j'

report_every_minute = 0
report_every_hour = 1
report_every_N = 5000

[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered,pauseBetweenRetries] = [.05,.05,.05,.3,15]
#[pauseK,pauseMin,pauseAfter,pauseAfterCityEntered,pauseBetweenRetries] = [1,.2,.05,.5,10]

pauseBetweenWorkerStart = 10    # may be helpful to enter captcha
pauseWhenYandexError = 20

#from ep_bioinfo_2014_12_10f_src_reader1 import *
#from ep_bioinfo_2015_01_29d_src_reader3_3 import *
#from ep_icg_lq_2015_12_24c__phil__words import *
from ep_icg_lq_2016_01_11c_countries_load import *


# for cities list
from ep_icg_lq_2016_04_28b__Ya__regionslist import *


#aaa__checkIfInputLoadedOK


from scipy import misc
import subprocess

import datetime
import gzip
import json
import re
import time
import zipfile
from   numpy import array,sort
from   Queue import Queue
from   selenium import webdriver
from   selenium.webdriver.common.keys import Keys
from   threading import Thread
from   time import sleep as pause

from   ep_icg_lq_2015_12_25d__email__test import sendemail

from   ep_lq_2016_04_27a__WTM___interface import *

# System modules





""" """
""" Setting starting working parameters """
""" """


def myjson(d):
    if(type(d)!=type({})):
        raise ValueError('Only for dicts, not for %s'%str(type(d)))
    tkeys = sort(d.keys())

    return '{'+', '.join([json.dumps(k)+': '+json.dumps(d[k]) for k in tkeys])+'}'


def savingPreStr(tq):
    return myjson(tq)

def rand():
    return ord(os.urandom(1))/255.0


#aaa
""" """
""" """
""" """


def downloadEnclosures(i, q,outq,wipq,doneq,only_json=0):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print '%s: Starting driver..' % i
    driver = webdriver.Firefox()
    driver.set_page_load_timeout(30)

    if(0): # need to login ?
        print '%s: Logining..' % i
        driver.get('https://passport.yandex.ru/passport?mode=auth')
        driver.find_element_by_xpath('//input[@id="login"]').clear()
        driver.find_element_by_xpath('//input[@id="login"]').send_keys('iwan.vepreff')#ya.kolchanov2015')
        driver.find_element_by_xpath('//input[@id="passwd"]').clear()
        driver.find_element_by_xpath('//input[@id="passwd"]').send_keys('icigrulezzz')
        driver.find_element_by_xpath('//span[@class="_nb-button-content"]').click()
        print '%s: log in OK' % i

    print '%s: Started OK' % i
    qstr = ''
    LQj = -153
    while True:
        try:
            if(verbose==2):
                print('%s: Reading next Q' % i)
            elif(verbose==1):
                print('%s-q '%i),

            task = q.get()
            hq = task['task_dict']
            qstr = hq['q']

            if(verbose==2):
                print('%s: q=%s' % (i,qstr))

            # checking as WorkInProgress task
            wip_queue.put(task)

            # preparing for URL
            tqstr = qstr.replace(' ','%20').replace('&','%26').replace('|','%7C')
            regstr = ('&rstr=-%i'%(hq['city_num'] if 'city_num' in hq else YaRegions_NumsDict[hq['city']]) if (len(hq['city'])>0) else ''


            if(only_json):
                url='http://yandex.ru/search?text=%s&lang=ru%s&from_date_full=%s&to_date_full=%s&ajax={}'%(tqstr,regstr,hq['dates'][0],hq['dates'][1])
            else:
                url='http://yandex.ru/yandsearch?text=%s&lang=ru%s&from_date_full=%s&to_date_full=%s'%(tqstr,regstr,hq['dates'][0],hq['dates'][1])

            if(verbose==2):
                print(url)
            while(1):
              try:
                driver.get(url)
                if(only_json):
                    tD = json.loads(driver.find_element_by_tag_name('pre').text)
                    temp_title = tD['serp']['params']['found']
                break
              except:
                print('.')
                pause(pauseBetweenRetries)
                pass
            pause(pauseMin+rand()*pauseK)


            if(only_json==0):
                while(u'\u042f\u043d\u0434\u0435\u043a\u0441' not in driver.title):
                  print('%s: "Yandex" not in title! sth wrong')
                  try:
                    driver.find_elements_by_xpath('//button[@class="button button_size_m button_theme_normal i-bem button_js_inited"]').click()
                    pause(1)
                  except:
                    pause(pauseWhenYandexError)
                    while(1):
                      try:
                        driver.get(url)
                        break
                      except:
                        pass
                    pause(pauseMin+rand()*pauseK)

            #print '%s: url opened' % i

                temp = driver.title
            else:
                pass

            if(verbose==2):
                print('%s: url opened' % i)
            elif(verbose==1):
                print('%s-u '%i),


            prevtitle = ''
            Ns = -153

            for j in range(15):
                if(only_json==0):
                    ttitle = driver.title
                else:
                    ttitle = temp_title

                temp=ttitle.replace(u'млн','000 000').replace(u'тыс.','000')

                try:
                    tempN = int(''.join(re.findall('[0-9]',temp)))
                except:
                    if(u'ничего не найдено' in temp):
                        tempN=0
                    else:
                        raise ValueError('wtf cant find a number in the title: [%s]'%ttitle)
                if(prevtitle!=ttitle):
                    break
                pause(pauseAfter+rand()*pauseK)
            Ns = tempN

            res = {'N':Ns}

            #print '%s: Done' % i
            outq.put((task,res))
            doneq.put(task)

            q.task_done()
        except Exception, e:
            print('%s: EXCEPTION OCCURED:' % i)
            #raise e
            try:
                driver.close()
            except:
                pass
            driver = webdriver.Firefox()
            driver.set_page_load_timeout(15)
            pause(1);


#aaa
# Filling the Queue
#print("Filling the Queue")

#aaa


NAlreadyDone = 0;
enclosure_queue = Queue()



output_queue = Queue()

# Work in progress queue
wip_queue = Queue()

# done - for  restart checking with wip_queue
doneq_queue = Queue()


if(stopAfterCheckDone):
    aaa_stopAfterCheckDone


def CheckWIPnDone():
  print 'CheckWIPnDone:starting'
  wips = {}
  wipN=0
  while(not wip_queue.empty()):
    hq = wip_queue.get()
    PreStr =  savingPreStr(hq)
    wips[PreStr]=hq
    wipN=wipN+1

  print('CheckWIPnDone:' +' found %i wipStuff'%wipN)
  while(not doneq_queue.empty()):
    hq = doneq_queue.get()
    PreStr =  savingPreStr(hq)
    try:
      wips[PreStr]=0
      wipN=wipN-1
    except:
      print('somehow done but not wip : %s'%PreStr)
#      raise ValueError('somehow wip but not done : %s'%PreStr)
  print('CheckWIPnDone:' +' found %i non-done wipStuff'%wipN)

  for s in  wips.keys():
    t = wips[s]
    if(t==0):
      pass
    else:
      hq = t
      print 'CheckWIPnDone: Adding %s to enclosure_queue'%str(t)
      enclosure_queue.put(hq)


  print 'CheckWIPnDone:done'


# This version does not send any kind of file
def reportEmail(subj='Hello!',body='Hello!'):
    print('Reporting E-mail subj=%s  body=%s'%(subj,body))
    sendemail(subj=email_subj_prefix+subj,body=body)
    return #


Workers = []

def start_all_workers(num_fetch_threads=num_fetch_threads):
#  for worker in Workers:
#    del worker
  print('Starting workers..')
  # Set up some threads to fetch the enclosures
  for i in range(num_fetch_threads):
      worker = Thread(target=downloadEnclosures, args=(i, enclosure_queue,output_queue,wip_queue,doneq_queue,))
      worker.setDaemon(True)
      worker.start()
      Workers.append(worker)
      pause(pauseBetweenWorkerStart)
  for i in range(num_fetch_threads_only_json):
      worker = Thread(target=downloadEnclosures, args=(num_fetch_threads+i, enclosure_queue,output_queue,wip_queue,doneq_queue,1))
      worker.setDaemon(True)
      worker.start()
      Workers.append(worker)
      pause(pauseBetweenWorkerStart)
  print('Workers started.')

if(1):
  import psutil

  def fillerThread(q_in):
      while(1):
          while(q_in.qsize()>30):
              pause(1)
          print('fillerThread: getting tasks..')
          tasks = []
          for WTMtryJ in range(10):
              try:
                  tasks = WTM_getTask(context = experiment,N=10*num_fetch_threads+10)
                  WTMtryJ=-1
                  break
              except Exception, e:
                  print('fillerThread: .. %s'%e)
          if(WTMtryJ>=0):
              print('fillerThread: .. COULDN''T SEND')
              continue
          for task in tasks:
              q_in.put(task)
          print('fillerThread: got tasks, now:%i'%q_in.qsize())


  def savingThread(q,q_in,sNleft = 0):
    print('savingThread: Sarted.')
    NDone=0
    last_sent = NDone#q_in.qsize()
    last_sent_hour = datetime.datetime.now().hour
    last_sent_minute = datetime.datetime.now().minute
    last_q = ''

    reportEmail(subj='[S] Running, NLeft report',body='S %i  last:%s'%(last_sent,last_q))

    while(True):

      in_empty_n = 0

      ntdn = 0
      while(q.empty()):
        if(q_in.empty()):
          in_empty_n=in_empty_n+1
          print('savingThread: nothing to do.. and they should''ve finished by now! (waiting %i already)'%in_empty_n)
        else:
          #print('savingThread: nothing to do..')
          ntdn=ntdn+1

        #q_in.qsize()
#        print('~ %i left undone, ntd=%i'%(q_in_size,ntdn))


        if(ntdn>40):
          print('='*10)
          print('Restarting')
          sendemail(subj=email_subj_prefix+'Restarting',body='Started')
          print('='*10)

          print('Killing firefoxes..')
          # Restarting because sth went wrong!

          for proc in psutil.process_iter():
            if (proc.name == 'firefox')or(proc.name == 'iceweasel'):
              proc.kill()

          print('done.')

          CheckWIPnDone()

          pause(1800)

          start_all_workers()
          ntdn=0
          print('='*10)
          sendemail(subj=email_subj_prefix+'Restarting',body='Done')


        if(in_empty_n>100):
          print('savingThread: nothing to DO! returning.')
          return
        pause(1)
      print('savingThread: saving !)')
      temp_saved=0

      try:
          Results
      except:
          Results = []
      print('savingThread: N results = %i'%len(Results))
      while not q.empty():
          task,res=q.get()
          last_q = task['task_dict']['q']
          Results.append({'id':task['id'],'task_dict':task['task_dict'],'result':res})
          NDone=NDone+1
          temp_saved=temp_saved+1
      print('savingThread: added results , now %i'%len(Results))
      json.dump(Results,open('2016_05_04a__temp_Results.json','w'))
      for jjjj in range(10):
          try:
              WTM_sendResults_many(worker_name,Results)
              Results=[]
              print('savingThread: SENT results , now %i'%len(Results))
              jjjj=-1
              break
          except Exception,e:
              print('savingThread: .. exception:%s'%e)
      if(jjjj>=0):
          print('savingThread: .. COULDN''T SEND')
      print('   saved %i, %i done total'%(temp_saved,NDone))
      try:
          subprocess.Popen(['sudo','find','/tmp','-amin','+30','-delete'])
          print('/tmp cleared')
      except:
          print('****************\nError in clearing temp')
          reportEmail(subj='Error in clearing temp',body='N %i  last:%s'%(q_in_size,str(last_q)))

      if(1):
        if(report_every_N>0) and (abs(NDone-last_sent)>=report_every_N):
            reportEmail(subj='[N] Running, NLeft report',body='N %i  last:%s'%(NDone,str(last_q)))
            last_sent= NDone

        if(report_every_minute>0) and (abs(datetime.datetime.now().minute - last_sent_minute)>=report_every_minute) :
            last_sent_minute = datetime.datetime.now().minute
            reportEmail(subj='[M] Running, NLeft report',body='M %i  last:%s'%(NDone,str(last_q)))

        if(report_every_hour>0)and(abs(datetime.datetime.now().hour-last_sent_hour)>=report_every_hour):
            last_sent_hour = datetime.datetime.now().hour
            reportEmail(subj='[H] Running, NLeft report',body='H %i  last:%s'%(NDone,str(last_q)))
      pause(5)


  #print('waiting for first output..')
  #while(output_queue.empty()):
  #  pause(.1)
  #print('starting saver..')
  #  print('NDone=%i'%NDone)

  worker = Thread(target=fillerThread, args=( enclosure_queue,))
  worker.setDaemon(True)
  worker.start()



  while(enclosure_queue.empty()):
      pause(2)



  start_all_workers()

  worker = Thread(target=savingThread, args=( output_queue,enclosure_queue,0,))
  worker.setDaemon(True)
  worker.start()

  # Now wait for the queue to be empty, indicating that we have
  # processed all of the downloads.
#  print '*** Main thread waiting'
  enclosure_queue.join()
#  print '*** Main thread Done waiting'
#  reportEmail('*** Main thread Done waiting','')

""" """
#

